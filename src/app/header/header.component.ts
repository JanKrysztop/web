import { Component, OnDestroy, OnInit } from '@angular/core';
import { Params, Router } from '@angular/router';
import Web3Service from '../modules/web3/services/web3.service';
import { BehaviorSubject, debounceTime, distinctUntilChanged, fromEvent, Subject, Subscription } from 'rxjs';
import { ICollectionDetails } from '../modules/web3/interfaces/collection-details.interface';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import CollectionsDataService from '../modules/chain-data/services/collections-data.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
	public get collections$(): BehaviorSubject<ICollectionDetails[]> {
		return this.collectionsDataService.searchedCollections$;
	}

	public get isSearchingByName(): boolean {
		return !!this.searchForm.get('name').value || !!this.mobileSearchForm.get('name').value;
	}

	public get isHelpCenter(): boolean {
		return this.router.url.includes('/help-center');
	}

	public isConnectedToWallet: boolean;
	public web3ConnectionSubscription: Subscription;
	public isMobileNavOpened: boolean;
	public isSearchOpen: boolean;
	public collections: ICollectionDetails[];
	public isLoadedCollections: boolean;
	public searchForm: FormGroup;
	public mobileSearchForm: FormGroup;
	private subscriptions: Subscription;
	private searchCloseSubscription: Subscription;
	private readonly searchByName$: Subject<string> = new Subject<string>();

	constructor(
		private readonly router: Router,
		private readonly web3Service: Web3Service,
		private readonly collectionsDataService: CollectionsDataService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly formBuilder: FormBuilder,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();
		this.collections = [];
		this.web3ConnectionSubscription = this.web3Service.provider$.subscribe(async () => {
			setTimeout(() => {
				this.isConnectedToWallet = this.web3Service.isConnected();
			}, 500);
		});
		this.searchForm = this.formBuilder.group({
			name: ['', [Validators.maxLength(255)]],
		});
		this.mobileSearchForm = this.formBuilder.group({
			name: ['', [Validators.maxLength(255)]],
		});

		this.subscriptions.add(this.collections$.subscribe((collections: ICollectionDetails[]) => {
			this.collections = collections?.length ? [...collections] : [];
			this.isLoadedCollections = true;
			this.toggleCollectionsLoader();
		}));

		this.subscriptions.add(
			this.searchByName$
				.pipe(
					debounceTime(500),
					distinctUntilChanged(),
				)
				.subscribe((name: string) => {
					this.collectionsDataService.getCollectionsByName(name);
				}),
			);

		this.searchCloseSubscription = fromEvent(document, 'click').subscribe((e) => {
			const clickedNode: Element = e.target as Element;

			if (clickedNode.classList.contains('search-icon')) {
				return;
			}

			if (this.isMobileNavOpened) {
				const mobileSearchDropdown: Element = document.querySelector('#mobileSearchContainer');

				if (this.isSearchOpen && !mobileSearchDropdown.contains(e.target as Node)) {
					this.isSearchOpen = false;
				}

				return;
			}
			const searchDropdown: Element = document.querySelector('#searchContainer');

			if (this.isSearchOpen && !searchDropdown.contains(e.target as Node)) {
				this.isSearchOpen = false;
			}
		});
	}

	public ngOnDestroy(): void {
		if (this.web3ConnectionSubscription) {
			this.web3ConnectionSubscription.unsubscribe();
		}
		if (this.searchCloseSubscription) {
			this.searchCloseSubscription.unsubscribe();
		}
		this.subscriptions.unsubscribe();
	}

	public scrollTo(targetId: string): void {
		document.getElementById(targetId).scrollIntoView({behavior: 'smooth', block: 'end'});
	}

	public goToCreate(): void {
		this.router.navigate(['/create']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToHelpCenter(): void {
		this.router.navigate(['/help-center']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToDocumentation(): void {
		window.open('https://docs.omnisea.org', '_blank');

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public async goToUserCollections(): Promise<void> {
		this.router.navigate(['/collections/user'], {queryParams: {userAddress: await this.web3Service.getUserAddress()}});

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToHome(): void {
		this.router.navigate(['/']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToExplore(): void {
		this.router.navigate(['/collections']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToRankings(): void {
		this.router.navigate(['/collections/rankings']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public goToONFTGateway(): void {
		this.router.navigate(['/onft/gateway']);
	}

	public goToToken(): void {
		this.router.navigate(['/token']);

		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	public async connectWalletWithModal(): Promise<void> {
		await this.web3Service.connectWithModal();
	}

	public toggleMobileNav(): void {
		this.isMobileNavOpened = !this.isMobileNavOpened;
	}

	public toggleSearch(): void {
		this.isSearchOpen = !this.isSearchOpen;
	}

	public onSearch(): void {
		const name: string = this.searchForm.get('name').value;

		if (!name) {
			return;
		}
		this.searchByName$.next(name);
	}

	public onMobileSearch(): void {
		const name: string = this.mobileSearchForm.get('name').value;

		this.searchByName$.next(name);
	}

	public goToCollection(collection: ICollectionDetails): void {
		const queryParams: Params = {
			id: collection.id,
			chainSymbol: collection.chainSymbol,
			collectionAddress: collection.collectionAddress,
			metadataURI: collection.metadataURI,
			creatorAddress: collection.creator,
		};

		this.router.navigate(
			['/collections/details'],
			{queryParams},
		);
		this.isSearchOpen = false;
		if (this.isMobileNavOpened) {
			this.toggleMobileNav();
		}
	}

	private toggleCollectionsLoader(): void {
		if (this.isMobileNavOpened) {
			this.isLoadedCollections
				? this.uiLoaderService.stopBackgroundLoader('mobileSearchLoader')
				: this.uiLoaderService.startBackgroundLoader('mobileSearchLoader');

			return;
		}

		this.isLoadedCollections
			? this.uiLoaderService.stopBackgroundLoader('searchLoader')
			: this.uiLoaderService.startBackgroundLoader('searchLoader');
	}

	private stopCollectionsLoader(): void {
		if (this.isMobileNavOpened) {
			this.uiLoaderService.stopBackgroundLoader('mobileSearchLoader');

			return;
		}

		this.uiLoaderService.stopBackgroundLoader('searchLoader');
	}
}
