import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoTwoComponent } from './demos/demo-two/demo-two.component';
import { createRoutes } from './modules/create/create-routes';
import { collectionsRoutes } from './modules/collections/collections-routes';
import { testnetRoutes } from './modules/testnet/testnet-routes';
import { SpecificModulePreloadStrategy } from './modules/shared/classes/specific-module-preload-strategy';
import { tokenRoutes } from './modules/token/token-routes';
import { communityRoutes } from './modules/community/community-routes';
import { helpCenterRoutes } from './modules/help-center/help-center-routes';
import { oNFTGatewayRoutes } from './modules/onft-gateway/onft-gateway-routes';

const routes: Routes = [
	{path: '', component: DemoTwoComponent},
	{
		path: 'create',
		children: createRoutes,
	},
	{
		path: 'collections',
		children: collectionsRoutes,
	},
	{
		path: 'testnet',
		children: testnetRoutes,
	},
	{
		path: 'token',
		children: tokenRoutes,
	},
	{
		path: 'community',
		children: communityRoutes,
	},
	{
		path: 'help-center',
		children: helpCenterRoutes,
	},
	{
		path: 'onft',
		children: oNFTGatewayRoutes,
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		scrollPositionRestoration: 'enabled',
		initialNavigation: 'enabled',
		preloadingStrategy: SpecificModulePreloadStrategy,
	})],
	exports: [RouterModule],
	providers: [SpecificModulePreloadStrategy],
})
export class AppRoutingModule {
}
