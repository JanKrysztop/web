import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-demo-two',
	templateUrl: './demo-two.component.html',
	styleUrls: ['./demo-two.component.scss']
})
export class DemoTwoComponent {
	constructor(
		private readonly router: Router,
	) {
	}

	public goToCreate(): void {
		this.router.navigate(['/create']);
	}


	public goToCollectionsList(): void {
		this.router.navigate(['/collections']);
	}
}
