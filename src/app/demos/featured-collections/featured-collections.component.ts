import { Component, OnInit } from '@angular/core';
import { Params, Router } from '@angular/router';
import { ICollectionDetails } from '../../modules/web3/interfaces/collection-details.interface';
import FirebaseStorageService from '../../modules/firebase/services/firebase-storage.service';
import { CollectionStatusEnum } from '../../modules/web3/enum/collection-status.enum';
import { ChainSymbolEnum } from '../../modules/web3/enum/chain-symbol.enum';

@Component({
	selector: 'app-featured-collections',
	templateUrl: './featured-collections.component.html',
	styleUrls: ['./featured-collections.component.scss'],
})
export class FeaturedCollectionsComponent implements OnInit {
	public featuredCollections: ICollectionDetails[];
	public isLoaded: boolean;
	public visibleIndex: number;

	constructor(
		private readonly router: Router,
		private readonly firebaseStorageService: FirebaseStorageService,
	) {
	}

	public ngOnInit(): void {
		this.visibleIndex = 0;

		this.featuredCollections = [
			{
				id: 1653941901312,
				status: CollectionStatusEnum.CREATED,
				collectionName: 'Fuji',
				collectionAddress: '0x3b36d3E54A0Cb98af45C3957221fB71DdCaA797F',
				chainSymbol: ChainSymbolEnum.avax,
				createdAt: 1653941901312,
				creator: '0x2fAAAa87963fdE26B42FB5CedB35a502d3ee09B3',
				creatorProfile: {
					name: 'Devv',
					lowercaseName: 'devv',
				},
				totalSupply: 3,
				tokenIds: 0,
				mintPrice: 7.99,
				fileURI: 'QmXfpiUwwcXNchj1ZDcPWY3wkBxsyLDqVreuKTxhLfN2Mf_1653941901312',
			},

			{
				id: 1653941991062,
				status: CollectionStatusEnum.CREATED,
				collectionName: 'Omni Art',
				collectionAddress: '0x29A5d74C29B2A86de70357bC5cd1AEba12DedE0A',
				chainSymbol: ChainSymbolEnum.bsc,
				createdAt: 1653941991062,
				creator: '0x46523F4E792E30A266d9b29469076a1695f17081',
				totalSupply: 4,
				tokenIds: 0,
				mintPrice: 0,
				fileURI: 'QmTM1xTQNvWLZNQd6qEzpMwpgJEt8nPew1VwyFfsV5NGJf_1653941991062',
			},

			{
				id: 1653892570389,
				status: CollectionStatusEnum.CREATED,
				collectionName: 'Monke',
				collectionAddress: '0xc83c73eC5993894b454d325a19Ed14d7A94c7B05',
				chainSymbol: ChainSymbolEnum.bsc,
				createdAt: 1653892570389,
				creator: '0x2fAAAa87963fdE26B42FB5CedB35a502d3ee09B3',
				creatorProfile: {
					name: 'Devv',
					lowercaseName: 'devv',
				},
				totalSupply: 2,
				tokenIds: 2,
				mintPrice: 5,
				fileURI: 'QmdwWoMV8D8YXqDYpRqBgxrt5txuBtVLjahoPQspbCLBRm_1653892570389',
			},
		];

		for (const collection of this.featuredCollections) {
			this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
				collection.imageURL = url;
			});
		}
	}

	public goToCollection(collection: ICollectionDetails): void {
		const queryParams: Params = {
			id: collection.id,
			chainSymbol: collection.chainSymbol,
			collectionAddress: collection.collectionAddress,
			metadataURI: collection.metadataURI,
			creatorAddress: collection.creator,
		};

		this.router.navigate(
			['/collections/details'],
			{queryParams},
		);
	}

	public async goToUserCollections(userAddress: string): Promise<void> {
		this.router.navigate(['/collections/user'], {queryParams: {userAddress}});
	}

	public getCreatorAlias(collection: ICollectionDetails): string {
		if (collection.creatorProfile?.name) {
			return collection.creatorProfile.name;
		}

		return `${collection.creator.substring(0, 5)}...${collection.creator.slice(-3)}`;
	}

	public onImageLoad(collection: ICollectionDetails): void {
		collection.isImageLoaded = true;
		const areImagesLoaded: boolean = this.featuredCollections.every((coll) => coll.isImageLoaded);

		if (areImagesLoaded) {
			this.isLoaded = true;

			setInterval(() => {
				if (this.visibleIndex <= 1) {
					this.visibleIndex++;

					return;
				}

				this.visibleIndex = 0;
			}, 5000);
		}
	}
}
