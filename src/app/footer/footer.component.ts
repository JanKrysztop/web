import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
	public get isHome(): boolean {
		return window.location.href === '';
	}

	constructor(private readonly router: Router) {
	}

	public goToHome(): void {
		this.router.navigate(['/']);
	}

	public goToFaucet(): void {
		this.router.navigate(['/testnet']);
	}

	public goToAmbassadors(): void {
		this.router.navigate(['/community/ambassadors']);
	}

	public goToAirdropForm(): void {
		this.router.navigate(['/community/airdrop-verification']);
	}

	public goToToken(): void {
		this.router.navigate(['/token']);
	}

	public goToTokenBridge(): void {
		this.router.navigate(['/token/bridge']);
	}

	public goToONFTGateway(): void {
		this.router.navigate(['/onft/gateway']);
	}

	public goToHelpCenter(): void {
		this.router.navigate(['/help-center']);
	}

	public goToDocumentation(): void {
		window.open('https://docs.omnisea.org', '_blank');
	}
}
