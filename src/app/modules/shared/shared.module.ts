import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
	NgxUiLoaderModule,
} from 'ngx-ui-loader';
import { HeaderComponent } from '../../header/header.component';
import { FooterComponent } from '../../footer/footer.component';
import { Web3Module } from '../web3/web3.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from '@danielmoncada/angular-datetime-picker';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxTippyModule } from 'ngx-tippy-wrapper';
import { ToastrComponent } from './components/toastr/toastr.component';
import { ToastrErrorComponent } from './components/toastr/toastr-error';
import { ToastrModule } from 'ngx-toastr';
import NotificationsService from './services/notifications.service';
import { NgProgressModule } from 'ngx-progressbar';
import { NgProgressRouterModule } from 'ngx-progressbar/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import StaticFilesService from './services/static-files.service';
import { VimeModule } from '@vime/angular';
import { AxelarModule } from '../axelar/axelar.module';
import { OmnichainModule } from '../omnichain/omnichain.module';

@NgModule({
	declarations: [
		HeaderComponent,
		FooterComponent,
		ToastrComponent,
		ToastrErrorComponent,
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		FlexLayoutModule,
		Web3Module,
		// @ts-ignore
		NgxUiLoaderModule.forRoot({
			bgsColor: '#ec255a',
			bgsOpacity: 0.5,
			bgsPosition: 'center-center',
			bgsSize: 60,
			bgsType: 'ball-scale-multiple',
			blur: 11,
			delay: 0,
			fastFadeOut: true,
			fgsColor: '#ec255a',
			fgsPosition: 'center-center',
			fgsSize: 60,
			fgsType: 'ball-scale-multiple',
			gap: 24,
			logoPosition: 'center-center',
			logoSize: 120,
			logoUrl: '',
			masterLoaderId: 'master',
			overlayBorderRadius: '0',
			overlayColor: 'rgba(40, 40, 40, 0.8)',
			pbColor: 'ec255a',
			pbDirection: 'ltr',
			pbThickness: 3,
			hasProgressBar: true,
			textColor: '#000000',
			textPosition: 'bottom-center',
			maxTime: -1,
			minTime: 300
		}),
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		NgxSkeletonLoaderModule,
		NgxTippyModule,
		ToastrModule.forRoot({
			toastComponent: ToastrComponent,
			toastClass: 'notyf confirm',
			positionClass: 'toast-bottom-center',
			preventDuplicates: true,
			includeTitleDuplicates: true,
			tapToDismiss: true,
			progressBar: true,
			maxOpened: 3,
		}),
		NgProgressModule.withConfig({
			color: '#EC255A',
		}),
		NgProgressRouterModule,
		InfiniteScrollModule,
		VimeModule,
		AxelarModule,
		OmnichainModule,
	],
	providers: [
		NotificationsService,
		StaticFilesService,
	],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		HeaderComponent,
		FooterComponent,
		FlexLayoutModule,
		NgxUiLoaderModule,
		Web3Module,
		OwlDateTimeModule,
		OwlNativeDateTimeModule,
		NgxSkeletonLoaderModule,
		NgxTippyModule,
		ToastrModule,
		ToastrComponent,
		ToastrErrorComponent,
		NgProgressModule,
		NgProgressRouterModule,
		InfiniteScrollModule,
		VimeModule,
		AxelarModule,
		OmnichainModule,
	],
})
export class SharedModule {
}
