export interface ICompressImageOptions {
	maxSizeMB?: number;
	maxWidthOrHeight?: number;
	useWebWorker?: boolean;
	maxIteration?: number;
	exifOrientation?: number;
	onProgress?: (progress: number) => void;
	fileType?: string;
	initialQuality?: number;
	alwaysKeepResolution?: boolean;
}
