import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import UserService from './services/user.service';
import ProfileService from './services/profile.service';

@NgModule({
	providers: [
		UserService,
		ProfileService,
	],
	imports: [
		SharedModule,
	],
})
export class UserModule {
}
