import { Injectable } from '@angular/core';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import Web3Service from '../../web3/services/web3.service';
import { IUserTransactionsList } from '../interfaces/user-transactions-list.interface';
import { BehaviorSubject } from 'rxjs';
import firebase from 'firebase/compat';
import Unsubscribe = firebase.Unsubscribe;
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';

@Injectable()
export default class UserService {
	public readonly userTransactions$: BehaviorSubject<IUserTransactionsList> = new BehaviorSubject<IUserTransactionsList>(undefined);
	private userAddress: string;
	private userTransactionsListener: Unsubscribe;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
	) {
	}

	public async run(): Promise<void> {
		setTimeout(() => {
			this.web3Service.provider$.subscribe(async (provider) => {
				if (!provider) {
					return;
				}

				const walletInterval: NodeJS.Timer = setInterval(async () => {
					if (this.web3Service.isConnected()) {
						const userAddress: string = await provider.getSigner()?.getAddress();

						if (userAddress) {
							clearInterval(walletInterval);
							this.listenToAllUserTransactions();
						}
					}
				}, 250);
			});
		}, 250);
	}

	public async listenToAllUserTransactions(): Promise<void> {
		if (this.userTransactionsListener) {
			return;
		}

		if (!this.userAddress) {
			this.userAddress = await this.web3Service.getUserAddress();

			if (!this.userAddress) {
				return;
			}
		}
		this.userTransactionsListener = await this.firebaseDatabaseService.get<IUserTransactionsList>(
			`/transactions/${this.userAddress}`,
			{},
			async (transactionsList: IUserTransactionsList) => {
				this.userTransactions$.next(transactionsList);
			},
		);
	}
}
