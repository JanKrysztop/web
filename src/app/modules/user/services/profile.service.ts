import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IProfile } from '../interfaces/profile.interface';
import FirestoreService from '../../firebase/services/firestore.service';
import { doc, getDoc, documentId } from 'firebase/firestore';
import { collection as colRef, Firestore, getDocs, limit, orderBy, query, where } from '@firebase/firestore';
import NotificationsService from '../../shared/services/notifications.service';
import Web3Service from '../../web3/services/web3.service';
import moment from 'moment';
import { IFollow } from '../interfaces/follow.interface';
import FirebaseStorageService from '../../firebase/services/firebase-storage.service';

@Injectable()
export default class ProfileService {
	public readonly profile: BehaviorSubject<IProfile> = new BehaviorSubject<IProfile>(undefined);

	constructor(
		private readonly firestoreService: FirestoreService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly notificationsService: NotificationsService,
		private readonly web3Service: Web3Service,
	) {
	}

	public async getProfile(userAddress: string): Promise<IProfile> {
		const db: Firestore = this.firestoreService.firestore$.getValue();
		const docRef = doc(db, 'profiles', userAddress);
		// @ts-ignore
		const docSnap = await getDoc<IProfile>(docRef);

		return docSnap.exists() ? docSnap.data() : undefined;
	}

	// TODO: Check if can load more than 10 in one batch
	public async getProfiles(userAddresses: string[]): Promise<IProfile[]> {
		if (!userAddresses?.length) {
			return [];
		}

		const db: Firestore = this.firestoreService.firestore$.getValue();
		const qb = query(
			colRef(db, 'profiles'),
			where(documentId(), 'in', userAddresses),
		);
		// @ts-ignore
		const snapshots = await getDocs<IProfile>(qb);
		const profiles: IProfile[] = [];

		snapshots.forEach((profileDoc) => {
			const profile: IProfile = profileDoc.data();

			if (profile?.avatar) {
				this.firebaseStorageService.getFileURL(profile.avatar).then((url) => {
					profile.avatar = url;
					profile.avatarLoaded = true;
				});
			}
			profiles.push(profile);
		});

		return profiles;
	}

	public async isFollower(followedAddress: string): Promise<boolean> {
		const userAddress: string = await this.web3Service.getUserAddress();

		if (!userAddress) {
			console.error('Cannot get isFollower() due to no wallet connection');

			return;
		}
		const db: Firestore = this.firestoreService.firestore$.getValue();
		const ref = doc(db, `follows/${userAddress}/followed/${followedAddress}`);
		// @ts-ignore
		const docSnap = await getDoc<IFollow>(ref);

		return docSnap.exists() && docSnap.data()?.isFollowing;
	}

	public async getFollowing(userAddress?: string): Promise<IFollow[]> {
		userAddress = userAddress || await this.web3Service.getUserAddress();

		if (!userAddress) {
			return [];
		}
		const db: Firestore = this.firestoreService.firestore$.getValue();
		const qb = query(
			colRef(db, `follows/${userAddress}/followed`),
			where('isFollowing', '==', true),
			orderBy('updatedAt', 'desc'),
		);
		// @ts-ignore
		const querySnapshot = await getDocs<IFollow>(qb);
		const following: IFollow[] = [];

		querySnapshot.forEach((followDoc) => {
			following.push(followDoc.data());
		});

		return following;
	}

	public async getFollowers(userAddress?: string): Promise<IFollow[]> {
		userAddress = userAddress || await this.web3Service.getUserAddress();

		if (!userAddress) {
			return [];
		}
		const db: Firestore = this.firestoreService.firestore$.getValue();
		const qb = query(
			colRef(db, `follows/${userAddress}/followers`),
			where('isFollowing', '==', true),
			orderBy('updatedAt', 'desc'),
		);
		// @ts-ignore
		const querySnapshot = await getDocs<IFollow>(qb);
		const followers: IFollow[] = [];

		querySnapshot.forEach((followDoc) => {
			followers.push(followDoc.data());
		});

		return followers;
	}

	public async follow(followedAddress: string): Promise<void> {
		const userAddress: string = await this.web3Service.getUserAddress();

		if (!userAddress) {
			this.notificationsService.error('Could not follow', 'Reconnect and try again.');

			return;
		}
		const followData: Pick<IFollow, 'isFollowing' | 'updatedAt'> = {
			isFollowing: true,
			updatedAt: moment().unix(),
		};

		await this.firestoreService.set<IFollow>(
			`follows/${userAddress}/followed`,
			followedAddress,
			{
				...followData,
				userAddress: followedAddress,
			},
		);
		await this.firestoreService.set<IFollow>(
			`follows/${followedAddress}/followers`,
			userAddress,
			{
				...followData,
				userAddress,
			},
		);
		this.notificationsService.success('Followed');
	}

	public async unfollow(followedAddress: string): Promise<void> {
		const userAddress: string = await this.web3Service.getUserAddress();

		if (!userAddress) {
			this.notificationsService.error('Could not unfollow', 'Reconnect and try again.');

			return;
		}
		const unfollowData: Pick<IFollow, 'isFollowing' | 'updatedAt'> = {
			isFollowing: false,
			updatedAt: moment().unix(),
		};

		await this.firestoreService.set(
			`follows/${userAddress}/followed`,
			followedAddress,
			unfollowData,
		);
		await this.firestoreService.set(
			`follows/${followedAddress}/followers`,
			userAddress,
			unfollowData,
		);
		this.notificationsService.success('Unfollowed');
	}

	public async isProfileNameUnique(name: string): Promise<boolean> {
		const db: Firestore = this.firestoreService.firestore$.getValue();
		const qb = query(
			colRef(db, 'profiles'),
			where('lowercaseName', '==', name.toLowerCase()),
			limit(1),
		);
		const querySnapshot = await getDocs(qb);

		return querySnapshot.empty;
	}
}
