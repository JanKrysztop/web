export interface IProfile {
	name: string;
	lowercaseName: string;
	userAddress?: string;
	avatar?: string;
	avatarLoaded?: boolean;
	backgroundImage?: string;
	twitter?: string;
	instagram?: string;
	discord?: string;
	externalLink?: string;
	ens?: string;
	isVerified?: string;
	isBlocked?: string;
	isFollowed?: boolean;
}
