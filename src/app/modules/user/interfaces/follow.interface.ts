export interface IFollow {
	isFollowing: boolean;
	updatedAt: number;
	userAddress: string;
}
