import { Injectable } from '@angular/core';
import { create } from 'ipfs-http-client';
import { BehaviorSubject } from 'rxjs';
import { CollectionMetadataDto } from '../../create/dto/collection-metadata.dto';
import { IPFSHTTPClient } from 'ipfs-http-client/types/src/types';
import { AddResult } from 'ipfs-core-types/types/src/root';
import { environment } from '../../../../environments/environment';

@Injectable({
	providedIn: 'root',
})
export default class IpfsService {
	public ipfsClient$: BehaviorSubject<IPFSHTTPClient> = new BehaviorSubject<IPFSHTTPClient>(undefined);
	private readonly ipfsRpcAuthHeader: string;

	public constructor() {
		this.ipfsRpcAuthHeader = this.composeIpfsAuthHeader();
	}

	public connect(): void {
		this.ipfsClient$.next(create({
			host: environment.ipfsInfuraApiHost,
			port: environment.ipfsInfuraApiPort,
			protocol: environment.ipfsInfuraApiProtocol,
			headers: {
				authorization: this.ipfsRpcAuthHeader,
			},
		}));
	}

	public getClient(): IPFSHTTPClient {
		return this.ipfsClient$.getValue();
	}

	public sendFile(file: File): Promise<AddResult> {
		return this.getClient().add(file);
	}

	public saveMetadata<T>(metadataDto: T): Promise<AddResult> {
		return this.getClient().add(JSON.stringify(metadataDto));
	}

	public get(path: string): AsyncIterable<Uint8Array> {
		return this.getClient().get(path);
	}

	public read(path: string): AsyncIterable<Uint8Array> {
		return this.getClient().cat(path);
	}

	private composeIpfsAuthHeader(): string {
		return 'Basic '
		+ Buffer.from(environment.ipfsInfuraProjectId + ':' + environment.ipfsInfuraSecret)
			.toString('base64');
	}
}
