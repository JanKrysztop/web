import { Injectable } from '@angular/core';
import { INFTDetails } from '../../web3/interfaces/nft-details.interface';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { Contract } from '@ethersproject/contracts';
import { BigNumber, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import FirebaseStorageService from '../../firebase/services/firebase-storage.service';
import { ContentTypeEnum } from '../../collections/enums/content-type.enum';

@Injectable()
export default class NftsService {
	public readonly omniNftAbi: string[] = [
		'function createdAt() external view returns (uint256)',
		'function creator() external view returns (address)',
		'function getMintedBy(address addr) public view returns (uint256[] memory)',
		'function collectionName() external view returns (string memory)',
		'function hidden() external view returns (bool)',
		'function dropFrom() external view returns (uint256)',
		'function dropTo() external view returns (uint256)',
		'function collectionURI() external view returns (string memory)',
		'function fileURI() external view returns (string memory)',
		'function mintPrice() external view returns (uint256)',
		'function totalSupply() external view returns (uint256)',
		'function tokenIds() external view returns (uint256)',
		'function tokenURI(uint256 tokenId) external view returns (string memory)'
	];

	constructor(
		private readonly web3Service: Web3Service,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly firebaseStorageService: FirebaseStorageService,
	) {
	}

	public async getByCollectionId(
		collectionId: number,
		onSuccess: (result: Record<string, INFTDetails>) => Promise<void>,
		onError: () => Promise<void>,
	): Promise<void> {
		await this.firebaseDatabaseService.get<Record<string, INFTDetails>>(
			`/tokens/${collectionId}`,
			{},
			onSuccess,
			onError,
		);
	}

	public async setDataFromDatabaseByTokenId(
		collectionId: number,
		nftRef: INFTDetails,
	): Promise<void> {
		await this.firebaseDatabaseService.getById<INFTDetails>(
			`/tokens/${collectionId}/${nftRef.tokenId}`,
			async (nft: INFTDetails) => {
				if (!nft) {
					console.error(`Not found tokenId: ${nftRef.tokenId} of collection with ID: ${collectionId}`);

					return;
				}

				nftRef.smFileIpfsHash = nft.smFileIpfsHash;
				nftRef.fileIpfsHash = nft.fileIpfsHash;
				nftRef.tokenURI = nft.tokenURI;
				nftRef.attributes = nft.attributes;

				this.firebaseStorageService.getIpfsFileURL(nftRef.smFileIpfsHash).then((url) => {
					nftRef.fileURL = url;
				});

				this.firebaseStorageService.getFileMetadata(nftRef.smFileIpfsHash).then((fileMetadata) => {
					nftRef.contentType = fileMetadata?.contentType.includes('audio') ? ContentTypeEnum.MUSIC : ContentTypeEnum.ART;
				});
			},
			async () => {
				console.error(`Unable to get NFT tokenId: ${nftRef.tokenId} of collection with ID: ${collectionId}`);
			}
		);
	}

	public async getMintedByUser(
		collectionAddress: string,
		collectionId: number,
		chainSymbol: ChainSymbolEnum,
		userAddress: string,
	): Promise<INFTDetails[]> {
		try {
			const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol);
			const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
			const mintedBy: BigNumber[] = await contract.getMintedBy(userAddress);
			const tokensIds: number[] = mintedBy?.length ? mintedBy.map((tokenId) => tokenId.toNumber()) : [];
			const nfts: INFTDetails[] = [];
			const metadataURI: string = await contract.collectionURI();
			const collectionName: string = await contract.collectionName();
			const totalSupply: number = (await contract.totalSupply()).toNumber();

			if (!tokensIds) {
				return;
			}

			for (const tokenId of tokensIds) {
				const nft: INFTDetails = {
					tokenId,
					collectionAddress,
					collectionName,
					chainSymbol,
					collectionId,
					collectionMetadataURI: metadataURI,
				};

				if (totalSupply > 0) {
					this.setDataFromDatabaseByTokenId(collectionId, nft);
				} else {
					const collectionFileURI: string = await contract.fileURI();

					this.firebaseStorageService.getIpfsFileURL(collectionFileURI).then((url) => {
						nft.fileURL = url;
					});
				}

				nfts.push(nft);
			}

			return nfts;
		} catch (e) {
			console.error(e);
		}
	}
}
