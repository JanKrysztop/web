import { Routes } from '@angular/router';

export const tokenRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./token.module').then(m => m.TokenModule),
	},
];
