import { Component, OnDestroy, OnInit } from '@angular/core';
import Web3Service from '../../../web3/services/web3.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import NotificationsService from '../../../shared/services/notifications.service';
import { environment } from '../../../../../environments/environment';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import { ChainNameToSymbolMap } from '../../../web3/types/chain-name-to-symbol-map.type';
import TokenBridgeService from '../../services/token-bridge.service';
import { ContractReceipt, ContractTransaction, ethers } from 'ethers';

@Component({
	selector: 'app-token-bridge',
	templateUrl: './token-bridge.component.html',
	styleUrls: ['./token-bridge.component.scss']
})
export class TokenBridgeComponent implements OnInit, OnDestroy {
	public get isBridging(): boolean {
		return this.state?.status && (this.state.status === 'approving' || this.state.status === 'pending' || this.state.status === 'completed');
	}

	public isConnectedToWallet: boolean;
	public connectedChain: ChainSymbolEnum;
	public srcChainBalanceFormatted: string;
	public dstChainBalanceFormatted: string;
	public state: { status: 'pending' | 'approving' | 'approval_error' | 'error' | 'completed' };
	public form: FormGroup;
	public selectedChainName: SupportedChainNameEnum;
	public chainNameToSymbolMap: Partial<ChainNameToSymbolMap>;
	private subscriptions: Subscription;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly formBuilder: FormBuilder,
		private readonly tokenBridgeService: TokenBridgeService,
		private readonly notificationService: NotificationsService,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();
		this.chainNameToSymbolMap = {};

		for (const chainName in environment.lzChainNameToSymbolMap) {
			if (
				!environment.lzChainNameToSymbolMap[chainName]
				|| !this.isSupportedNetwork(environment.lzChainNameToSymbolMap[chainName])
			) {
				continue;
			}
			this.chainNameToSymbolMap[chainName] = environment.lzChainNameToSymbolMap[chainName];
		}

		this.form = this.formBuilder.group({
			amount: [0, [Validators.required, Validators.min(0.0001)]],
			to: [undefined, Validators.maxLength(150)],
			dstChainId: [undefined],
		});

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();
					const userAddress: string = await this.web3Service.getUserAddress();

					if (!this.isConnectedToWallet || !userAddress) {
						return;
					}
					clearInterval(walletInterval);
					this.connectedChain = this.web3Service.getConnectedChainSymbol();
					this.chainNameToSymbolMap[environment.chainSymbolToNameMap[this.connectedChain]] = undefined;
					this.onTargetChainChange(
						this.connectedChain !== ChainSymbolEnum.eth
							? SupportedChainNameEnum.Ethereum
							: SupportedChainNameEnum.Polygon
					);
					const destinationAddressControl: AbstractControl = this.form.get('to');

					if (!destinationAddressControl.value) {
						destinationAddressControl.setValue(await this.web3Service.getUserAddress());
					}

					try {
						this.srcChainBalanceFormatted = await this.tokenBridgeService.getOSEABalanceFormatted(
							userAddress,
							this.connectedChain,
						);

						if (!this.form.get('amount').value && Number(this.srcChainBalanceFormatted) > 0) {
							this.form.get('amount').setValue(Number(this.srcChainBalanceFormatted));
						}
					} catch (e) {
						console.error(e);
					}
				}, 250);
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public async onSubmit(): Promise<void> {
		if (!environment.isTokenBridgeActive) {
			this.notificationService.error('Bridge is now inactive');

			return;
		}
		const dstChainId: number = this.form.get('dstChainId').value;
		const amount: number = this.form.get('amount').value;
		const to: string = this.form.get('to').value;

		if (!amount) {
			this.notificationService.error('Set the amount');

			return;
		}

		try {
			if (this.srcChainBalanceFormatted && Number(this.srcChainBalanceFormatted) < amount) {
				this.notificationService.error('Not enough OSEA');

				return;
			}
		} catch (e) {
			console.error(e);
		}

		this.state = {
			status: null,
		};

		try {
			const tx: ContractTransaction = await this.tokenBridgeService.bridge(
				amount,
				dstChainId,
				to,
				this.state,
				environment.chainNameToSymbolMap[this.selectedChainName],
			);

			const txResult: ContractReceipt = await tx.wait();
			this.notificationService.success('Bridging OSEA', 'OSEA will be transferred to the destination chain in a few minutes');

			if (txResult && txResult.status !== 1) {
				this.notificationService.error('Unknown status', 'Couldn\'t confirm the tx status. Please, check it in the blockchain explorer.');
			}
		} catch (e) {
			console.error(e);
			this.notificationService.error('Error', 'Couldn\'t initialize bridging. Please, try again in a moment.');
		}
	}

	public connectWallet(): void {
		this.web3Service.connectWithModal();
	}

	public onTargetChainChange(chainName: SupportedChainNameEnum): void {
		const chainSymbol: ChainSymbolEnum = this.chainNameToSymbolMap[chainName];
		const dstChainId: number = environment.chainSymbolToLzChainIdMAINNET[chainSymbol];

		if (!chainSymbol || !dstChainId) {
			this.notificationService.error('Invalid chain');
			return;
		}

		this.selectedChainName = chainName;
		this.form.get('dstChainId').setValue(dstChainId);
	}

	public mapChainNameToEnum(chainName: string): SupportedChainNameEnum {
		return chainName as SupportedChainNameEnum;
	}

	// private async setSrcChainBalance(userAddress: string): Promise<void> {
	// 	this.srcChainBalanceFormatted = await this.tokenSaleService.getUSDCBalanceFormatted(userAddress, this.connectedChain);
	// }

	private isSupportedNetwork(chainSymbol: ChainSymbolEnum): boolean {
		return environment.supportedChains.includes(chainSymbol);
	}
}
