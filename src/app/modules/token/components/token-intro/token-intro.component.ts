import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import Web3Service from '../../../web3/services/web3.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import TokenSaleService from '../../services/token-sale.service';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { BigNumber, ContractReceipt, ContractTransaction } from 'ethers';
import NotificationsService from '../../../shared/services/notifications.service';
import { environment } from '../../../../../environments/environment';
import * as moment from 'moment';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';

@Component({
	selector: 'app-token-intro',
	templateUrl: './token-intro.component.html',
	styleUrls: ['./token-intro.component.scss']
})
export class TokenIntroComponent implements OnInit, OnDestroy {
	public get isTokenSaleOngoing(): boolean {
		const now: number = moment().unix();

		return this.fromUnix && this.toUnix && now >= this.fromUnix.toNumber() && now <= this.toUnix.toNumber();
	}

	public get isBuying(): boolean {
		return ['pending', 'approving', 'depositing', 'success'].includes(this.state?.status);
	}

	public get stateInfoTitle(): string {
		switch (this.state?.status) {
			case 'pending':
				return 'In Progress...';
			case 'approving':
				return 'Approving...';
			case 'depositing':
				return 'Buying ⏳';
			case 'approval_error':
				return 'Error on Approval 😔';
			case 'deposit_error':
				return 'Error on Deposit 😔';
			case 'success':
				return 'Success ✅ Thank you for your trust! 🥳';
			default:
				return 'Buy OSEA';
		}
	}

	public get stateInfoSubtitle(): string {
		switch (this.state?.status) {
			case 'pending':
				return 'Initiating the transaction.';
			case 'approving':
				return 'Please, approve the transaction.';
			case 'depositing':
				return 'Confirm the transaction and wait for confirmation.';
			case 'approval_error':
				return 'Please, refresh the page, and try approving again.';
			case 'deposit_error':
				return 'Please, refresh the page, and try again.';
			case 'success':
				return 'Done! Waiting for the transaction confirmation.';
			default:
				return '';
		}
	}

	public get isEnded(): boolean {
		return this.toUnix?.toNumber() > 0 && moment().unix() > this.toUnix.toNumber();
	}

	public get timeToStartLabel(): string {
		const now: number = moment().unix();
		const secondsToStart: number = moment(this.fromUnix.toNumber()).diff(now);

		return this.isWaitingForStart
			? `Starts in ${moment.duration(secondsToStart, 'seconds').humanize()}`
			: 'Starts 4th of July at 6 AM UTC';
	}

	public get isTokenSaleActive(): boolean {
		return environment.isTokenSaleActive;
	}

	public get isLoaded(): boolean {
		return this.fromUnix?.toNumber() > 0 && this.toUnix?.toNumber() > 0;
	}

	public get nativeAsset(): string {
		return environment.chainSymbolToNativeAsset[this.connectedChain];
	}

	private get amountFormControl(): AbstractControl {
		return this.tokenSaleForm.get('amount');
	}

	@ViewChild('closeBtn') public closeBtn: ElementRef<HTMLButtonElement>;
	public isConnectedToWallet: boolean;
	public connectedChain: ChainSymbolEnum;
	public allocatedFormatted: string;
	public tokenSaleForm: FormGroup;
	public estimatedAllocation: number;
	public fromUnix: BigNumber;
	public toUnix: BigNumber;
	public isAllocated: boolean;
	public state: {
		status: 'pending' | 'approving' | 'depositing' | 'success' | 'approval_error' | 'deposit_error',
	};
	public usdcBalanceFormatted: string;
	public isWaitingForStart: boolean;
	public isAirdropClaimable: boolean;
	public isClaimingAirdrop: boolean;
	private rate: number;
	private subscriptions: Subscription;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly formBuilder: FormBuilder,
		private readonly tokenSaleService: TokenSaleService,
		private readonly notificationService: NotificationsService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly router: Router,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();

		if (!this.isTokenSaleActive) {
			return;
		}

		this.tokenSaleForm = this.formBuilder.group({
			amount: [0, [Validators.required, Validators.min(0.0001)]],
			terms: [undefined, Validators.requiredTrue],
		});

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();
					const userAddress: string = await this.web3Service.getUserAddress();

					if (!this.isConnectedToWallet || !userAddress) {
						return;
					}
					clearInterval(walletInterval);
					this.connectedChain = this.web3Service.getConnectedChainSymbol();
					this.setIsAirdropClaimable();

					try {
						this.getStartDate();
						this.getEndDate();
						this.getRate();
						this.getAllocated();
						this.getUSDCBalance(userAddress);
					} catch (e) {
						console.error(e);
					}
				}, 250);
			}),
		);

		this.subscriptions.add(
			this.amountFormControl.valueChanges.subscribe(() => {
				if (!this.rate) {
					return;
				}

				if (!this.amountFormControl.value) {
					this.estimatedAllocation = 0;

					return;
				}
				const estimatedAllocation: number = this.connectedChain === ChainSymbolEnum.bsc
					? (this.rate * this.amountFormControl.value)
					: (this.rate * this.amountFormControl.value) / Math.pow(10, 12);

				this.estimatedAllocation = Math.round((estimatedAllocation + Number.EPSILON) * 100) / 100;
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public async onDeposit(): Promise<void> {
		if (!this.isTokenSaleActive) {
			return;
		}
		const amount: number = this.tokenSaleForm.get('amount').value;

		if (!amount) {
			return;
		}

		try {
			if (this.usdcBalanceFormatted && Number(this.usdcBalanceFormatted) < amount) {
				this.notificationService.error('Not enough USDC');
			}
		} catch (e) {
			console.error(e);
		}

		this.state = {
			status: 'pending',
		};
		this.uiLoaderService.startBackgroundLoader('tokenBuyLoader');

		try {
			const tx: ContractTransaction = await this.tokenSaleService.deposit(amount, this.state);

			this.state.status = 'success';
			this.notificationService.success('Purchased 🥳', 'Waiting for the transaction confirmation. Thank you!');
			const txResult: ContractReceipt = await tx.wait();

			if (txResult.status === 1) {
				this.notificationService.success('Success 🥳', 'Tokens will be claimable after sale\'s end');
				this.closeModal();
				this.getAllocated();
				this.uiLoaderService.stopBackgroundLoader('tokenBuyLoader');

				return;
			}

			this.notificationService.error('Wait a minute...', 'Couldn\'t confirm the tx status. Please, refresh and check your allocation.');
			this.closeModal();
			this.getAllocated();
			this.uiLoaderService.stopBackgroundLoader('tokenBuyLoader');
		} catch (e) {
			this.uiLoaderService.stopBackgroundLoader('tokenBuyLoader');
			console.error(e);
			this.notificationService.error('Error', 'Couldn\'t complete purchase. Please, try again in a moment.');
		}
	}

	public async onWithdraw(): Promise<void> {
		try {
			const tx: ContractTransaction = await this.tokenSaleService.withdrawAllocation();

			this.notificationService.success('Transaction sent', 'Waiting for the confirmation.');
			const txResult: ContractReceipt = await tx.wait();

			if (txResult.status === 1) {
				this.notificationService.success('Success 🥳', 'Withdrawn allocated tokens');

				return;
			}

			this.notificationService.error('Error', 'Couldn\'t confirm the tx status. Please check if it succeeded.');
		} catch (e) {
			console.error(e);
			this.notificationService.error('Error', 'Couldn\'t withdraw allocation. Please, try again in a moment.');
		}
	}

	public connectWallet(): void {
		this.web3Service.connectWithModal();
	}

	public goToHelpCenter(): void {
		window.open('https://www.omnisea.org/help-center', '_blank');
	}

	public goToWhitepaper(): void {
		window.open('https://bafybeih56o2owd2elwipokmjofddw4gu6psocdrftfyd7ykbi5r6trfw7q.ipfs.infura-ipfs.io/', '_blank');
	}

	public goToTokenBridge(): void {
		this.router.navigate(['/token/bridge']);
	}

	public claimAirdrop(): void {
		this.isClaimingAirdrop = true;
		try {
			this.tokenSaleService.claimAirdrop();
		} catch (e) {
			this.isClaimingAirdrop = false;
		}
	}

	private getStartDate(): void {
		this.tokenSaleService.getStartDate().then((fromUnix) => {
			this.fromUnix = BigNumber.from(fromUnix);

			if (this.isWaitingForStart) {
				return;
			}
			this.isWaitingForStart = this.fromUnix.toNumber() > moment().unix();

			if (this.isWaitingForStart) {
				const startCounterInterval = setInterval(() => {
					if (moment().unix() >= this.fromUnix.toNumber()) {
						clearInterval(startCounterInterval);
					}
				}, 1000);
			}
		});
	}

	private getEndDate(): void {
		this.tokenSaleService.getEndDate().then((toUnix) => {
			this.toUnix = toUnix;
		});
	}

	private getRate(): void {
		this.tokenSaleService.getRate().then((rate) => {
			this.rate = rate.toNumber();
		});
	}

	private getAllocated(): void {
		this.tokenSaleService.getAllocatedByUser().then((allocated) => {
			this.isAllocated = allocated.gt(0);
			this.allocatedFormatted = this.tokenSaleService.getFormattedAllocation(allocated);
		});
	}

	private closeModal(timeout?: number): void {
		setTimeout(() => {
			const closeBtn: HTMLButtonElement = this.closeBtn.nativeElement;

			if (closeBtn) {
				closeBtn.click();
			}
			document.querySelector('#buyTokensModal').classList.add('hidden');
		}, timeout || 0);
	}

	private async getUSDCBalance(userAddress: string): Promise<void> {
		this.usdcBalanceFormatted = await this.tokenSaleService.getUSDCBalanceFormatted(userAddress, this.connectedChain);
	}

	private setIsAirdropClaimable(): void {
		this.tokenSaleService.isAirdropClaimable().then((isClaimable) => {
			this.isAirdropClaimable = isClaimable;
		});
	}
}
