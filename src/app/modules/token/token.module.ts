import { NgModule } from '@angular/core';
import { TokenRoutingModule } from './token-routing.module';
import { SharedModule } from '../shared/shared.module';
import { Web3Module } from '../web3/web3.module';
import { TokenIntroComponent } from './components/token-intro/token-intro.component';
import TokenSaleService from './services/token-sale.service';
import { TokenBridgeComponent } from './components/token-bridge/token-bridge.component';
import TokenBridgeService from './services/token-bridge.service';

@NgModule({
	providers: [
		TokenSaleService,
		TokenBridgeService,
	],
	declarations: [
		TokenIntroComponent,
		TokenBridgeComponent,
	],
	imports: [
		TokenRoutingModule,
		SharedModule,
		Web3Module,
	],
	exports: [
		TokenIntroComponent,
		TokenBridgeComponent,
	],
})
export class TokenModule {
}
