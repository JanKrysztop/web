import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AmbassadorsFormComponent } from './components/ambassadors-form/ambassadors-form.component';
import { AirdropFormComponent } from './components/airdrop-form/airdrop-form.component';

const routes: Routes = [
	{
		path: 'ambassadors',
		component: AmbassadorsFormComponent,
	},
	{
		path: 'airdrop-verification',
		component: AirdropFormComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CommunityRoutingModule {
}
