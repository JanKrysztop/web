import { NgModule } from '@angular/core';
import { CommunityRoutingModule } from './community-routing.module';
import { SharedModule } from '../shared/shared.module';
import { Web3Module } from '../web3/web3.module';
import { AmbassadorsFormComponent } from './components/ambassadors-form/ambassadors-form.component';
import { AirdropFormComponent } from './components/airdrop-form/airdrop-form.component';

@NgModule({
	declarations: [
		AmbassadorsFormComponent,
		AirdropFormComponent,
	],
	imports: [
		CommunityRoutingModule,
		SharedModule,
		Web3Module,
	],
	exports: [
		AmbassadorsFormComponent,
		AirdropFormComponent,
	]
})
export class CommunityModule {
}
