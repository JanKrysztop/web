import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import FirebaseDatabaseService from '../../../firebase/services/firebase-database.service';
import FirebaseService from '../../../firebase/services/firebase.service';
import { Subscription } from 'rxjs';
import NotificationsService from '../../../shared/services/notifications.service';
import { AirdropVerificationDto } from '../../dto/airdrop-verification.dto';
import * as moment from 'moment';
import { Router } from '@angular/router';
import Web3Service from '../../../web3/services/web3.service';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';

@Component({
	selector: 'app-airdrop-form',
	templateUrl: './airdrop-form.component.html',
	styleUrls: ['./airdrop-form.component.scss']
})
export class AirdropFormComponent implements OnInit, OnDestroy {
	public form: FormGroup;
	public code: string;
	private payload: AirdropVerificationDto;
	private subscriptions: Subscription;
	private id: string;
	private isConnectedToWallet: boolean;
	private userAddress: string;
	private isSubmitting: boolean;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly firebaseService: FirebaseService,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly notificationsService: NotificationsService,
		private readonly router: Router,
		private readonly web3Service: Web3Service,
	) {
	}

	public ngOnInit(): void {
		try {
			fetch('https://api.ipify.org?format=json')
				.then(response => response.json())
				.then(data => this.id = data.ip);
		} catch (e) {
			//
		}

		this.subscriptions = new Subscription();
		this.form = this.formBuilder.group({
			discordName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(250)]],
			code: ['', [Validators.maxLength(250)]],
			retweet1: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(500)]],
			retweet3: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(500)]],
			terms: [false, Validators.requiredTrue],
			policy: [false],
		});

		this.subscriptions.add(this.firebaseService.firebaseCommunityApp$.subscribe((app) => {
			if (!app || this.firebaseDatabaseService.communityDatabase$.getValue()) {
				return;
			}

			this.firebaseDatabaseService.setCommunityDatabase(app);
		}));
		this.firebaseService.createFirebaseCommunityApp();

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();

					if (this.isConnectedToWallet) {
						clearInterval(walletInterval);
						this.userAddress = await this.web3Service.getUserAddress();
						const connectedChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

						if (!connectedChainSymbol) {
							await this.connectWallet();
						}
					}
				}, 250);
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public onSubmit(): void {
		if (!this.firebaseDatabaseService.communityDatabase$.getValue()) {
			this.notificationsService.error('Error', 'No connection with data layer');
			return;
		}

		if (this.form.invalid) {
			this.notificationsService.error('Error', 'Incorrect form data');

			return;
		}

		if (!this.userAddress) {
			this.notificationsService.error('Connect Wallet', 'Connect your eligible wallet before submitting the form');

			return;
		}

		if (this.isSubmitting) {
			return;
		}
		this.isSubmitting = true;

		try {
			this.payload = this.getPayload();
			this.firebaseDatabaseService.add(
				'airdrop',
				this.payload,
				async () => {
					localStorage?.setItem('lastAddress', this.userAddress);
					this.notificationsService.success('Success', 'Airdrop will be claimable from 1st of August');
					this.form.reset();
					this.goToHome();
					this.isSubmitting = false;
				},
				async () => {
					this.notificationsService.error('Sorry', 'We couldn\'t send your verification. Please, try again (we recommend trying other device/browser)');
					this.isSubmitting = false;
				},
				this.firebaseDatabaseService.communityDatabase$.getValue(),
			);
		} catch (e) {
			console.error(e);
			this.isSubmitting = false;
		}
	}

	public generateNewCode(): void {
		this.code = Math.random().toString(36).replace(/[^a-z]+/g, '').substring(0, 5);
	}

	public isWalletConnected(): boolean {
		return this.web3Service.isConnected();
	}

	public connectWallet(): void {
		this.web3Service.connectWithModal();
	}

	private getPayload(): AirdropVerificationDto {
		return {
			address: this.userAddress,
			lastAddress: localStorage?.getItem('lastAddress') || null,
			discordName: this.form.get('discordName').value,
			at: moment().unix() || null,
			retweet1: this.form.get('retweet1').value,
			retweet3: this.form.get('retweet3').value,
			terms: this.form.get('terms').value,
			policy: this.form.get('policy').value || null,
			id: this.id || null,
			code: this.code || null,
			userCode: this.form.get('code').value || null,
		};
	}

	private goToHome(): void {
		this.router.navigate(['/']);
	}
}
