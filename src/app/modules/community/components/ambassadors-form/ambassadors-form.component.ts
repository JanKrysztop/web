import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import FirebaseDatabaseService from '../../../firebase/services/firebase-database.service';
import FirebaseService from '../../../firebase/services/firebase.service';
import { Subscription } from 'rxjs';
import { AmbassadorApplicationDto } from '../../dto/ambassador-application.dto';
import NotificationsService from '../../../shared/services/notifications.service';

@Component({
	selector: 'app-ambassadors-form',
	templateUrl: './ambassadors-form.component.html',
	styleUrls: ['./ambassadors-form.component.scss']
})
export class AmbassadorsFormComponent implements OnInit, OnDestroy {
	public form: FormGroup;
	private ambassadorApplication: AmbassadorApplicationDto;
	private subscriptions: Subscription;
	private id: string;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly firebaseService: FirebaseService,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly notificationsService: NotificationsService,
	) {
	}

	public ngOnInit(): void {
		try {
			fetch('https://api.ipify.org?format=json')
				.then(response => response.json())
				.then(data => this.id = data.ip);
		} catch (e) {
			//
		}

		this.subscriptions = new Subscription();
		this.form = this.formBuilder.group({
			twitterName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(250)]],
			discordName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(250)]],
			testnetAddress: ['', [Validators.maxLength(250)]],
			region: ['', [Validators.required, Validators.maxLength(250)]],
			experience: ['', [Validators.maxLength(2000)]],
			sharedTestnetURL: ['', [Validators.maxLength(500)]],
			availability: ['', [Validators.required, Validators.maxLength(500)]],
			retweets: ['', [Validators.required, Validators.maxLength(2000)]],
		});

		this.subscriptions.add(this.firebaseService.firebaseCommunityApp$.subscribe((app) => {
			if (!app || this.firebaseDatabaseService.communityDatabase$.getValue()) {
				return;
			}

			this.firebaseDatabaseService.setCommunityDatabase(app);
		}));

		if (!this.firebaseService.firebaseCommunityApp$.getValue()) {
			this.firebaseService.createFirebaseCommunityApp();
		}
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public onSubmit(): void {
		if (!this.firebaseDatabaseService.communityDatabase$.getValue()) {
			return;
		}

		if (this.form.invalid) {
			return;
		}

		this.ambassadorApplication = this.getAmbassadorApplicationData();
		this.firebaseDatabaseService.add(
			'ambassadors',
			this.ambassadorApplication,
			async () => {
				this.notificationsService.success('Thank you 😊', 'Your application has been sent');
			},
			async () => {
				this.notificationsService.error('Sorry', 'We couldn\'t send your application. Please, try again');
			},
			this.firebaseDatabaseService.communityDatabase$.getValue(),
		);
	}

	public scrollToForm() {
		document.getElementById('ambassadorsForm').scrollIntoView({behavior: 'smooth'});
	}

	private getAmbassadorApplicationData(): AmbassadorApplicationDto {
		return {
			twitterName: this.form.get('twitterName').value,
			discordName: this.form.get('discordName').value,
			region: this.form.get('region').value,
			availability: this.form.get('availability').value,
			retweets: this.form.get('retweets').value,
			sharedTestnetURL: this.form.get('experience').value || '',
			testnetAddress: this.form.get('experience').value || '',
			experience: this.form.get('experience').value || '',
			id: this.id || null,
		};
	}
}
