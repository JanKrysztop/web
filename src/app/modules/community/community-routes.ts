import { Routes } from '@angular/router';

export const communityRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./community.module').then(m => m.CommunityModule),
	},
];
