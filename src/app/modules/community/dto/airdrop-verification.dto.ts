export interface AirdropVerificationDto {
	address: string;
	lastAddress: string;
	discordName: string;
	at: number;
	code: string;
	userCode: string;
	retweet1: string;
	retweet3: string;
	terms: boolean;
	policy?: boolean;
	id?: string;
}
