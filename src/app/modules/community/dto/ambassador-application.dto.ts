export interface AmbassadorApplicationDto {
	twitterName: string;
	discordName: string;
	testnetAddress?: string;
	region: string;
	experience?: string;
	sharedTestnetURL?: string;
	availability: string;
	retweets: string;
	id?: string;
}
