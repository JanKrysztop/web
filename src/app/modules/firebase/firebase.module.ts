import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import FirebaseService from './services/firebase.service';
import FirebaseStorageService from './services/firebase-storage.service';
import FirebaseDatabaseService from './services/firebase-database.service';
import FirebaseAnalyticsService from './services/firebase-analytics.service';
import FirestoreService from './services/firestore.service';

@NgModule({
	providers: [
		FirebaseService,
		FirestoreService,
		FirebaseStorageService,
		FirebaseDatabaseService,
		FirebaseAnalyticsService,
	],
	imports: [
		CommonModule,
	],
})
export class FirebaseModule {
}
