import { Injectable } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { FirebaseApp, FirebaseOptions } from '@firebase/app';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment';
const { initializeAppCheck, ReCaptchaV3Provider } = require('firebase/app-check');

@Injectable({
	providedIn: 'root',
})
export default class FirebaseService {
	public readonly firebaseApp$: BehaviorSubject<FirebaseApp> = new BehaviorSubject<FirebaseApp>(undefined);
	public readonly firebaseCommunityApp$: BehaviorSubject<FirebaseApp> = new BehaviorSubject<FirebaseApp>(undefined);

	public createFirebaseApp(): void {
		const firebaseConfig: FirebaseOptions = this.getFirebaseConfig();
		const firebaseApp: FirebaseApp = initializeApp(firebaseConfig);

		if (!environment.production) {
			// @ts-ignore
			self.FIREBASE_APPCHECK_DEBUG_TOKEN = environment.firebase.useDebugToken;
		}
		const appCheck = initializeAppCheck(firebaseApp, {
			provider: new ReCaptchaV3Provider(environment.firebase.appCheckToken),
			isTokenAutoRefreshEnabled: true,
		});

		this.firebaseApp$.next(appCheck.app);
	}

	public createFirebaseCommunityApp(): void {
		const firebaseConfig: FirebaseOptions = this.getFirebaseConfig();
		firebaseConfig.databaseURL = environment.firebase.communityDatabaseURL;
		firebaseConfig.storageBucket = undefined;
		const firebaseApp: FirebaseApp = initializeApp(firebaseConfig,  'community');

		if (!environment.production) {
			// @ts-ignore
			self.FIREBASE_APPCHECK_DEBUG_TOKEN = environment.firebase.useDebugToken;
		}
		const appCheck = initializeAppCheck(firebaseApp, {
			provider: new ReCaptchaV3Provider(environment.firebase.appCheckToken),
			isTokenAutoRefreshEnabled: true,
		});

		this.firebaseCommunityApp$.next(appCheck.app);
	}

	private getFirebaseConfig(): FirebaseOptions {
		return {
			apiKey: environment.firebase.apiKey,
			authDomain: environment.firebase.authDomain,
			projectId: environment.firebase.projectId,
			storageBucket: environment.firebase.storageBucket,
			databaseURL: environment.firebase.databaseURL,
			messagingSenderId: environment.firebase.messagingSenderId,
			appId: environment.firebase.appId,
			measurementId: environment.firebase.measurementId,
		};
	}
}
