import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FirebaseApp } from '@firebase/app';
import { getDatabase } from 'firebase/database';
import {
	Database,
	ref,
	set,
	push,
	update,
	remove,
	query,
	orderByChild,
	orderByKey,
	limitToLast,
	limitToFirst,
	equalTo,
	onValue,
	increment,
	Query,
	DatabaseReference,
	QueryConstraint,
} from '@firebase/database';
import firebase from 'firebase/compat';
import Unsubscribe = firebase.Unsubscribe;

@Injectable({
	providedIn: 'root',
})
export default class FirebaseDatabaseService {
	public readonly database$: BehaviorSubject<Database> = new BehaviorSubject<Database>(undefined);
	public readonly communityDatabase$: BehaviorSubject<Database> = new BehaviorSubject<Database>(undefined);

	public setDatabase(firebaseApp: FirebaseApp): void {
		this.database$.next(getDatabase(firebaseApp));
	}

	public setCommunityDatabase(firebaseApp: FirebaseApp): void {
		this.communityDatabase$.next(getDatabase(firebaseApp));
	}

	public async save<T>(
		path: string,
		data: T,
		onSuccess?: (result: T) => Promise<void>,
		onError?: () => Promise<void>,
		selectedDatabase?: Database,
	): Promise<void> {
		const db: Database = selectedDatabase || this.database$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firebase Database on: ${path}: save`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}

		try {
			await set(ref(db, path), data);

			if (!onSuccess || typeof onSuccess !== 'function') {
				return;
			}

			onSuccess(data);
		} catch (e) {
			console.error(e);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}
	}

	public async add<T>(
		path: string,
		data: T,
		onSuccess?: (result: T) => Promise<void>,
		onError?: () => Promise<void>,
		selectedDatabase?: Database,
	): Promise<void> {
		const db: Database = selectedDatabase || this.database$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firebase Database on: ${path}: add()`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}

		try {
			await push(ref(db, path), data);

			if (!onSuccess || typeof onSuccess !== 'function') {
				return;
			}

			onSuccess(data);
		} catch (e) {
			console.error(e);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}
	}

	public async update<T = object>(
		path: string,
		data: T,
		onSuccess?: (result: T) => Promise<void>,
		onError?: () => Promise<void>,
		selectedDatabase?: Database,
	): Promise<void> {
		const db: Database = selectedDatabase || this.database$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firebase Database on: ${path}: update`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}

		try {
			await update(ref(db, path), data || {});

			if (!onSuccess || typeof onSuccess !== 'function') {
				return;
			}

			onSuccess(data);
		} catch (e) {
			console.error(e);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}
	}

	public async exists(
		path: string,
		onExist: () => Promise<void>,
		onNotExist: () => Promise<void>,
	): Promise<void> {
		const db: Database = this.database$.getValue();
		const entityRef: DatabaseReference = ref(db, path);
		onValue(entityRef, (snapshot) => {
			const isExisting: boolean = snapshot.exists();

			if (isExisting) {
				onExist();

				return;
			}

			onNotExist();
		});
	}

	public async getById<T>(
		path: string,
		onSuccess: (result: T) => Promise<void>,
		onError?: () => Promise<void>,
		selectedDatabase?: Database,
	): Promise<void> {
		const db: Database = selectedDatabase || this.database$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firebase Database on: ${path}: getById()`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}

		const entityRef: DatabaseReference = ref(db, path);
		onValue(entityRef, (snapshot) => {
			const isExisting: boolean = snapshot.exists();

			if (!isExisting) {
				console.debug(`${path} not exists`);
			}

			if (!onSuccess || typeof onSuccess !== 'function') {
				return;
			}
			onSuccess(snapshot.val());
		}, (e) => {
			if (!isErrorCallback) {
				return;
			}
			onError();
		});
	}

	public async get<T>(
		path: string,
		queryParams?: {
			limit?: number;
			orderBy?: string;
			equalTo?: string | number | boolean;
			sortOrder?: 'ASC' | 'DESC';
		},
		onSuccess?: (result: T) => Promise<void>,
		onError?: () => Promise<void>,
		selectedDatabase?: Database,
	): Promise<Unsubscribe> {
		const db: Database = selectedDatabase || this.database$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firebase Database on: ${path}: get()`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}
		const conditions: QueryConstraint[] = [];

		if (queryParams.limit !== undefined) {
			queryParams.sortOrder === 'ASC'
				? conditions.push(limitToFirst(queryParams.limit + 1))
				: conditions.push(limitToLast(queryParams.limit + 1));
		}

		if (queryParams.orderBy !== undefined) {
			conditions.push(orderByChild(queryParams.orderBy));
		} else {
			conditions.push(orderByKey());
		}

		if (queryParams.equalTo !== undefined) {
			conditions.push(equalTo(queryParams.equalTo));
		}

		const dbQuery: Query = query(ref(db, path), ...conditions);

		return onValue(dbQuery, (snapshot) => {
			const isExisting: boolean = snapshot.exists();

			if (!isExisting) {
				console.debug(`${path} with filters: ${JSON.stringify(queryParams)} not exists`);
			}

			if (!onSuccess || typeof onSuccess !== 'function') {
				return;
			}
			onSuccess(snapshot.val());
		}, (e) => {
			console.error(e);

			if (!isErrorCallback) {
				return;
			}
			onError();
		});
	}

	public async remove(
		path: string,
		selectedDatabase?: Database,
	): Promise<boolean> {
		const db: Database = selectedDatabase || this.database$.getValue();

		if (!db) {
			console.error(`No Firebase Database on: ${path}: remove`);

			return false;
		}

		try {
			await remove(ref(db, path));

			return true;
		} catch (e) {
			console.error(e);

			return false;
		}
	}

	public async increment(
		path: string,
		selectedDatabase?: Database,
	): Promise<void> {
		const db: Database = selectedDatabase || this.database$.getValue();

		if (!db) {
			console.error(`No Firebase Database on: ${path}: increment`);
		}

		try {
			await set(ref(db, path), increment(1));
		} catch (e) {
			console.error(e);
		}
	}

	public async unsubscribe(unsubscribeFn: Promise<Unsubscribe>): Promise<void> {
		(await unsubscribeFn)();
	}
}
