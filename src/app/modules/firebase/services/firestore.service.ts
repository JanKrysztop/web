import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FirebaseApp } from '@firebase/app';
import { getFirestore } from 'firebase/firestore';
import firebase from 'firebase/compat';
import Unsubscribe = firebase.Unsubscribe;
import { Firestore, collection, doc, addDoc, setDoc, deleteDoc, DocumentReference } from '@firebase/firestore';

@Injectable({
	providedIn: 'root',
})
export default class FirestoreService {
	public readonly firestore$: BehaviorSubject<Firestore> = new BehaviorSubject<Firestore>(undefined);

	public setFirestore(firebaseApp: FirebaseApp): void {
		this.firestore$.next(getFirestore(firebaseApp));
	}

	public async add<T>(
		collectionName: string,
		subCollectionPath: string[],
		data: T,
		onSuccess?: (result: T) => Promise<void>,
		onError?: () => Promise<void>,
		selectedStore?: Firestore,
	): Promise<DocumentReference<T>> {
		const db: Firestore = selectedStore || this.firestore$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firestore on: ${collectionName}: add()`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}

		try {
			const colRef = collection(db, collectionName, ...subCollectionPath);
			const result: DocumentReference = await addDoc(colRef, data);

			if (onSuccess && typeof onSuccess === 'function') {
				onSuccess(data);
			}
			// @ts-ignore
			return result;
		} catch (e) {
			console.error(e);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}
	}

	public async set<T = object>(
		collectionPath: string,
		docName: string,
		data: T,
		onSuccess?: (result: T) => Promise<void>,
		onError?: () => Promise<void>,
		selectedStore?: Firestore,
	): Promise<void> {
		const db: Firestore = selectedStore || this.firestore$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firestore on: ${collectionPath}/${docName}: set()`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}

		try {
			const colRef = collection(db, collectionPath);
			const docRef = doc(colRef, docName);
			await setDoc(docRef, (data || {}), { merge: true });

			if (!onSuccess || typeof onSuccess !== 'function') {
				return;
			}

			onSuccess(data);
		} catch (e) {
			console.error(e);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}
	}

	public async delete<T = object>(
		collectionPath: string,
		docName: string,
		onSuccess?: () => Promise<void>,
		onError?: () => Promise<void>,
		selectedStore?: Firestore,
	): Promise<void> {
		const db: Firestore = selectedStore || this.firestore$.getValue();
		const isErrorCallback: boolean = onError && typeof onError === 'function';

		if (!db) {
			console.error(`No Firestore on: ${collectionPath}/${docName}: set()`);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}

		try {
			const colRef = collection(db, collectionPath);
			const docRef = doc(colRef, docName);
			await deleteDoc(docRef);

			if (!onSuccess || typeof onSuccess !== 'function') {
				return;
			}

			onSuccess();
		} catch (e) {
			console.error(e);

			if (!isErrorCallback) {
				return;
			}
			onError();
		}
	}

	public async unsubscribe(unsubscribeFn: Promise<Unsubscribe>): Promise<void> {
		(await unsubscribeFn)();
	}

	private isNumberEven(value: number): boolean{
		return value % 2 === 0;
	}
}
