import { Injectable } from '@angular/core';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { BigNumber, ContractReceipt, ContractTransaction, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { Contract } from '@ethersproject/contracts';
import { environment } from '../../../../environments/environment';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import * as moment from 'moment';
import { CollectionEarningListType } from '../../collections/types/collection-earning-list.type';
import { CollectionRefundsListType } from '../../collections/types/collection-refunds-list.type';
import { IClaimEarnedTx } from '../../collections/interfaces/claim-earned-tx.interface';
import { IMintTx } from '../../collections/interfaces/mint-tx.interface';
import { CollectionBasicChainDataType } from '../types/collection-basic-chain-data.type';
import FirestoreService from '../../firebase/services/firestore.service';
import { doc, Firestore, getDoc } from '@firebase/firestore';
import firebase from 'firebase/compat';
import { IClaimRefundTx } from '../../collections/interfaces/claim-refund-tx.interface';
import DocumentData = firebase.firestore.DocumentData;
import { IMintData } from '../../collections/interfaces/mint-data.interface';
import { OmnichainActionTypeEnum } from '../../omnichain/enums/omnichain-action-type.enum';
import OmnichainService from '../../omnichain/services/omnichain.service';

@Injectable()
export default class CollectionsService {
	public readonly collectionFactoryContractAbi: string[] = [
		'function create(string memory chSym, string memory collName, string memory collURI, string memory fileURI, uint256 price, uint256 from, uint256 to, string[] memory URIs, uint gas) public payable',
		'function estimateFees(string memory collName, string memory chSym, string memory URI, string memory fileURI, string[] memory URIs, uint gas) external view returns (uint)',
		'function repository() public view returns (address)',
		'event Created(address collectionAddress)',
	];
	public readonly omniNftAbi: string[] = [
		'function createdAt() external view returns (uint256)',
		'function creator() external view returns (address)',
		'function getMintedBy(address addr) public view returns (uint256[] memory)',
		'function collectionName() external view returns (string memory)',
		'function hidden() external view returns (bool)',
		'function dropFrom() external view returns (uint256)',
		'function dropTo() external view returns (uint256)',
		'function collectionURI() external view returns (string memory)',
		'function fileURI() external view returns (string memory)',
		'function mintPrice() external view returns (uint256)',
		'function totalSupply() external view returns (uint256)',
		'function tokenIds() external view returns (uint256)',
		'function tokenURI(uint256 tokenId) external view returns (string memory)'
	];
	public readonly collectionsRepositoryContractAbi: string[] = [
		'function getById(uint256 _tokenId) public view returns (IOmniERC721 collection)',
		'function getAllByUser(address user) external view returns (address[] memory)',
	];
	public readonly tokenFactoryContractAbi: string[] = [
		'function refunds(address coll, string memory dstChainName, address spender) public view returns (uint256)',
		'function mints(address coll, string memory dstChainName) public view returns (uint256)',
		'function getEarned(address coll, string memory _dstChainName, uint256 gas, uint256 redirectFee) external payable',
		'function refund(address coll, string memory _dstChainName, uint256 gas, uint256 redirectFee) external payable',
		'function estimateFees(string memory chSym, uint256 price, uint256 gas, uint256 nativeGas) external view returns (uint)',
	];
	private mintTxFirestoreId: string;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly firestoreService: FirestoreService,
		private readonly omnichainService: OmnichainService,
	) {
	}

	public async getCollectionMintedCount(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
	): Promise<number> {
		const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol);
		const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);

		return (await contract.tokenIds()).toNumber();
	}

	public async getCollectionDetails(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
		fallbackId?: number,
	): Promise<ICollectionDetails> {
		fallbackId = fallbackId || 0;
		try {
			const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol, fallbackId);
			const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
			const dropFrom: number = (await contract.dropFrom()).toNumber();
			const dropTo: number = (await contract.dropTo()).toNumber();
			const createdAt: number = await contract.createdAt();
			const creator: string = await contract.creator();
			const mintPrice: number = (await contract.mintPrice()).toNumber();
			const collectionName: string = await contract.collectionName();
			const fileURI: string = await contract.fileURI();
			const hidden: boolean = await contract.hidden();
			const metadataURI: string = await contract.collectionURI();
			const tokenIds: number = (await contract.tokenIds()).toNumber();
			const totalSupply: number = (await contract.totalSupply()).toNumber();

			return {
				collectionName,
				collectionAddress,
				metadataURI,
				fileURI,
				creator,
				createdAt,
				dropFrom,
				dropTo,
				mintPrice,
				tokenIds,
				totalSupply,
				hidden,
				chainSymbol,
			};
		} catch (e) {
			console.error(e);
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getCollectionDetails(
						collectionAddress,
						chainSymbol,
						fallbackId,
					);
				}
			}

			return {
				collectionName: '',
				collectionAddress,
				metadataURI: '',
				createdAt: 0,
				dropFrom: 0,
				dropTo: 0,
				mintPrice: 0,
				tokenIds: 0,
				totalSupply: 0,
				hidden: true,
				chainSymbol,
			};
		}
	}

	public async getCollectionStateFromChain(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
		fallbackId?: number,
	): Promise<CollectionBasicChainDataType> {
		fallbackId = fallbackId || 0;
		try {
			const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol, fallbackId);
			const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
			const metadataURI: string = await contract.collectionURI();
			const tokenIds: number = (await contract.tokenIds()).toNumber();
			const fileURI: string = await contract.fileURI();

			return {
				collectionAddress,
				metadataURI,
				tokenIds,
				fileURI,
			};
		} catch (e) {
			console.error(e);
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getCollectionStateFromChain(
						collectionAddress,
						chainSymbol,
						fallbackId,
					);
				}
			}

			throw new Error(e);
		}
	}

	public async getCollectionBasicDetails(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
		fallbackId?: number,
	): Promise<ICollectionDetails> {
		fallbackId = fallbackId || 0;
		const collectionBasicDetails: ICollectionDetails = {
			collectionName: '',
			collectionAddress,
			fileURI: '',
			createdAt: 0,
			mintPrice: undefined,
			tokenIds: undefined,
			totalSupply: undefined,
			hidden: false, // TODO: After implementing default is false
			chainSymbol,
		};

		try {
			const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol, fallbackId);
			const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);

			collectionBasicDetails.createdAt = await contract.createdAt();
			collectionBasicDetails.collectionName = await contract.collectionName();
			collectionBasicDetails.fileURI = await contract.fileURI();
			// const hidden: boolean = await contract.hidden();

			contract.mintPrice().then((mintPrice: BigNumber) => {
				collectionBasicDetails.mintPrice = mintPrice.toNumber();
			});
			contract.tokenIds().then((tokenIds: BigNumber) => {
				collectionBasicDetails.tokenIds = tokenIds.toNumber();
			});
			contract.totalSupply().then((totalSupply: BigNumber) => {
				collectionBasicDetails.totalSupply = totalSupply.toNumber();
			});

			return collectionBasicDetails;
		} catch (e) {
			console.error(e);
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getCollectionDetails(
						collectionAddress,
						chainSymbol,
						fallbackId,
					);
				}
			}

			return collectionBasicDetails;
		}
	}

	public async isCollectionCreated(
		collectionAddress: string,
		chainSymbol: ChainSymbolEnum,
	): Promise<boolean> {
		const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol);
		const contract: Contract = new ethers.Contract(collectionAddress, this.omniNftAbi, provider);
		const createdAt: BigNumber | undefined = await contract.createdAt();

		return createdAt && (createdAt.toNumber() > 0);
	}

	public async getCollectionContractOnSelectedNetwork(
		contractAddress: string,
		chainSymbol: ChainSymbolEnum,
	): Promise<Contract> {
		const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol);

		return new ethers.Contract(contractAddress, this.omniNftAbi, provider);
	}

	public chainSymbolToCollectionFactoryAddressMap(chainSymbol: ChainSymbolEnum): string {
		return environment.collectionFactoryAddressesMap[chainSymbol];
	}

	public async getCollectionFactoryContractOnSelectedNetwork(
		chainSymbol: ChainSymbolEnum,
		fallbackId?: number,
	): Promise<Contract> {
		try {
			const contractAddress: string = environment.collectionFactoryAddressesMap[chainSymbol];
			const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol, fallbackId);

			return new ethers.Contract(contractAddress, this.collectionFactoryContractAbi, provider);
		} catch (e) {
			console.error(e);
		}
	}

	public async getRepositoryContract(chainSymbol: ChainSymbolEnum, fallbackId?: number): Promise<Contract> {
		fallbackId = fallbackId || 0;

		try {
			const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol, fallbackId);
			const collectionFactoryAddress: string = environment.collectionFactoryAddressesMap[chainSymbol];
			if (!collectionFactoryAddress) {
				return;
			}
			const factoryContract: Contract = new ethers.Contract(collectionFactoryAddress, this.collectionFactoryContractAbi, provider);
			const repositoryAddress: string = await factoryContract.repository();

			return new ethers.Contract(repositoryAddress, this.collectionsRepositoryContractAbi, provider);
		} catch (e) {
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getRepositoryContract(chainSymbol, fallbackId);
				}
			}

			console.error(e);
			return;
		}
	}

	public async getTokenFactoryContract(chainSymbol: ChainSymbolEnum, fallbackId?: number, isWrite?: boolean): Promise<Contract> {
		fallbackId = fallbackId || 0;

		try {
			const tokenFactoryAddress: string = environment.tokenFactoryAddressesMap[chainSymbol];

			if (!tokenFactoryAddress) {
				return;
			}
			return new ethers.Contract(
				tokenFactoryAddress,
				this.tokenFactoryContractAbi,
				isWrite ? this.web3Service.getProvider().getSigner() : this.web3Service.getNetworkProvider(chainSymbol, fallbackId),
			);
		} catch (e) {
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getTokenFactoryContract(chainSymbol, fallbackId);
				}
			}

			console.error(e);
			return;
		}
	}

	public async getCollectionById(collectionId: string): Promise<ICollectionDetails> {
		const db: Firestore = this.firestoreService.firestore$.getValue();
		const ref = doc(db, `collections/${collectionId}`);
		// @ts-ignore
		const docSnap = await getDoc<ICollectionDetails>(ref);

		return docSnap.exists() ? docSnap.data() : undefined;
	}

	public async saveCollectionInDatabase(creatorAddress: string, collectionDetails: ICollectionDetails): Promise<void> {
		const timestamp: number = moment().unix();

		await this.firebaseDatabaseService.save<ICollectionDetails>(
			`/collections/${collectionDetails.id}`,
			collectionDetails,
			undefined,
			async () => {
				throw new Error(`Cannot store collection of user: ${creatorAddress} at: ${timestamp}`);
			}
		);
	}

	public async getRefundsList(
		collection: ICollectionDetails,
		spenderAddress: string,
	): Promise<Partial<CollectionRefundsListType>> {
		const refundsList = {};
		const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol);

		if (!contract) {
			return refundsList;
		}

		for (const chainSymbol of environment.supportedChains) {
			if (chainSymbol === collection.chainSymbol) {
				continue;
			}

			const chainRefund: BigNumber | undefined = await contract.refunds(
				collection.collectionAddress,
				chainSymbol,
				spenderAddress,
			);


			refundsList[chainSymbol] = chainRefund.gt(0) ? Number(ethers.utils.formatUnits(chainRefund, 6)) : 0;
		}

		return refundsList;
	}

	public async getEarningsList(
		collection: ICollectionDetails,
		mintPrice: number,
	): Promise<Partial<CollectionEarningListType>> {
		const earningsList = {};
		const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol);

		if (!contract) {
			return earningsList;
		}

		for (const chainSymbol of environment.supportedChains) {
			if (chainSymbol === collection.chainSymbol) {
				continue;
			}

			const chainMintsCount: BigNumber | undefined = await contract.mints(
				collection.collectionAddress,
				chainSymbol,
			);

			earningsList[chainSymbol] = (chainMintsCount?.toNumber() || 0) * mintPrice;
		}

		return earningsList;
	}

	public async claimEarned(collection: ICollectionDetails, dstChainSymbol: ChainSymbolEnum): Promise<boolean> {
		try {
			const userAddress: string = await this.web3Service.getProvider().getSigner().getAddress();
			const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol, 0, true);
			const encodedFnData: string = this.web3Service.encodePayload(
				[
					collection.collectionAddress,
					dstChainSymbol,
					environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: measure and set gas used for CLAIM_EARNED action
					0,
				],
				['address', 'string', 'uint', 'uint'],
			);
			const encodedPayload: string = this.web3Service.encodePayload(
				[
					dstChainSymbol,
					userAddress || '0xMock',
					encodedFnData,
					environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: measure and set gas used for CLAIM_EARNED action
					userAddress || '0xMock',
					dstChainSymbol,
					userAddress || '0xMock',
					0,
				],
				['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
			);
			const estimatedFees = await this.omnichainService.getEstimatedFees(
				dstChainSymbol,
				encodedPayload,
				OmnichainActionTypeEnum.MINT_TOKEN, // TODO: measure and set gas used for CLAIM_EARNED action
			);
			const tx: ContractTransaction = await contract.getEarned(
				collection.collectionAddress,
				dstChainSymbol,
				environment.chainSymbolToMintTokenGas[dstChainSymbol],
				estimatedFees.redirectFee,
				{value: estimatedFees.fee},
			);

			const srcChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();
			const at: number = moment().unix();
			let claimTxData: IClaimEarnedTx = {
				collectionId: collection.id,
				srcChainSymbol,
				dstChainSymbol,
				at,
			};
			let claimTxFirestoreId: string;

			try {
				const res = await this.firestoreService.add<IClaimEarnedTx>(
					'transactions',
					[userAddress, 'claimEarned'],
					claimTxData,
				);

				claimTxFirestoreId = res.id;
			} catch (e) {
				console.error(e);
			}

			try {
				this.firebaseDatabaseService.save<IClaimEarnedTx>(
					`/transactions/${userAddress}/claimEarned/${collection.collectionAddress}/${tx.hash}`,
					claimTxData,
				);
			} catch (e) {
				console.error(e);
			}
			const txResult: ContractReceipt = await tx.wait();
			claimTxData = {
				collectionId: collection.id,
				srcChainSymbol,
				dstChainSymbol,
				at,
				status: txResult?.status || null,
			};

			try {
				this.firestoreService.set<IClaimEarnedTx>(
					`transactions/${userAddress}/claimEarned`,
					claimTxFirestoreId,
					claimTxData,
				);
			} catch (e) {
				console.error(e);
			}

			try {
				this.firebaseDatabaseService.save<IClaimEarnedTx>(
					`/transactions/${userAddress}/claimEarned/${collection.collectionAddress}/${tx.hash}`,
					claimTxData,
				);
			} catch (e) {
				console.error(e);
			}

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}

	public async refund(collection: ICollectionDetails, dstChainSymbol: ChainSymbolEnum): Promise<boolean> {
		try {
			const userAddress: string = await this.web3Service.getProvider().getSigner().getAddress();
			const contract: Contract = await this.getTokenFactoryContract(collection.chainSymbol, 0, true);
			const encodedFnData: string = this.web3Service.encodePayload(
				[
					collection.collectionAddress,
					dstChainSymbol,
					environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: measure and set gas used for REFUND action
					0,
				],
				['address', 'string', 'uint', 'uint'],
			);
			const encodedPayload: string = this.web3Service.encodePayload(
				[
					dstChainSymbol,
					userAddress || '0xMock',
					encodedFnData,
					environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: measure and set gas used for REFUND action
					userAddress || '0xMock',
					dstChainSymbol,
					userAddress || '0xMock',
					0,
				],
				['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
			);
			const estimatedFees = await this.omnichainService.getEstimatedFees(
				dstChainSymbol,
				encodedPayload,
				OmnichainActionTypeEnum.MINT_TOKEN, // TODO: measure and set gas used for REFUND action
			);
			const tx: ContractTransaction = await contract.refund(
				collection.collectionAddress,
				dstChainSymbol,
				environment.chainSymbolToMintTokenGas[dstChainSymbol], // TODO: measure and set gas used for REFUND action
				estimatedFees.redirectFee,
				{value: estimatedFees.fee},
			);
			const srcChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();
			const at: number = moment().unix();
			let claimRefundTxData: IClaimRefundTx = {
				collectionId: collection.id,
				srcChainSymbol,
				dstChainSymbol,
				at,
			};
			let txFirestoreId: string;

			try {
				const res = await this.firestoreService.add<IClaimEarnedTx>(
					'transactions',
					[userAddress, 'refund'],
					claimRefundTxData,
				);

				txFirestoreId = res.id;
			} catch (e) {
				console.error(e);
			}

			try {
				this.firebaseDatabaseService.save<IClaimRefundTx>(
					`/transactions/${userAddress}/refund/${collection.collectionAddress}/${tx.hash}`,
					claimRefundTxData,
				);
			} catch (e) {
				console.error(e);
			}
			const txResult: ContractReceipt = await tx.wait();

			claimRefundTxData = {
				collectionId: collection.id,
				srcChainSymbol,
				dstChainSymbol,
				at,
				status: txResult?.status || null,
			};
			try {
				this.firestoreService.set<IClaimEarnedTx>(
					`transactions/${userAddress}/refund`,
					txFirestoreId,
					claimRefundTxData,
				);
			} catch (e) {
				console.error(e);
			}

			try {
				this.firebaseDatabaseService.save<IClaimEarnedTx>(
					`/transactions/${userAddress}/refund/${collection.collectionAddress}/${tx.hash}`,
					claimRefundTxData,
				);
			} catch (e) {
				console.error(e);
			}

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}

	public async getTransactionResult(txHash: string, chainSymbol: ChainSymbolEnum): Promise<ContractReceipt> {
		const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol);

		return await provider.getTransactionReceipt(txHash);
	}

	public getTxHashFromDatabase(path: string): Promise<string | null> {
		return new Promise((resolve, reject) => {
			this.firebaseDatabaseService.getById<Pick<ContractTransaction, 'hash'>>(
				path,
				async (tx: Pick<ContractTransaction, 'hash'>) => {
					resolve(tx?.hash || null);
					return;
				},
				async () => {
					console.error('Could not getTxHashFromDatabase() for path: ', path);
					reject();
				}
			);
		});
	}

	public async saveMintTx(
		collection: ICollectionDetails,
		userAddress: string,
		tx: ContractTransaction,
		status?: number,
	): Promise<void> {
		const mintTxData: IMintTx = {
			collectionId: collection.id,
			srcChainSymbol: this.web3Service.getConnectedChainSymbol(),
			dstChainSymbol: collection.chainSymbol,
			txHash: tx.hash,
			at: moment().unix(),
			status: status || null,
		};

		try {
			if (!this.mintTxFirestoreId) {
				const res = await this.firestoreService.add<IMintTx>(
					'transactions',
					[userAddress, 'mint'],
					mintTxData,
				);

				this.mintTxFirestoreId = res.id;
			} else {
				this.firestoreService.set<IMintTx>(
					`transactions/${userAddress}/mint`,
					this.mintTxFirestoreId,
					mintTxData,
				);
			}

		} catch (e) {
			console.error(e);
		}

		try {
			this.firebaseDatabaseService.save<IMintTx>(
				`/transactions/${userAddress}/mint/${collection.collectionAddress}/${tx.hash}`,
				mintTxData,
			);
		} catch (e) {
			console.error(e);
		}
	}

	public async incrementCollectionMintsCount(
		collection: ICollectionDetails,
		userAddress: string,
		mintPrice: number,
	): Promise<void> {
		try {
			this.firebaseDatabaseService.increment(`/stats/collections/${collection.id}/mint`);
		} catch (e) {
			console.error(e);
		}

		try {
			await this.firestoreService.add<IMintData>(
				'mints',
				[],
				{
					collectionId: collection.firestoreId,
					collectionName: collection.collectionName,
					collectionFileURI: collection.fileURI,
					userAddress,
					at: moment().unix(),
					price: mintPrice,
				},
			);
		} catch (e) {
			console.error(e);
		}
	}

	public async getCollectionMintsCountFromDatabase(
		collectionId: number,
		onSuccess: (mintCount: number) => Promise<void>,
	): Promise<void> {
		try {
			this.firebaseDatabaseService.getById<number>(
				`/stats/collections/${collectionId}/mint`,
				async (mintCount: number) => {
					onSuccess(mintCount);
				},
			);
		} catch (e) {
			console.error(e);
		}
	}
}
