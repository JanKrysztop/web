import { Injectable } from '@angular/core';
import FirebaseDatabaseService from '../../firebase/services/firebase-database.service';
import { BehaviorSubject } from 'rxjs';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { environment } from '../../../../environments/environment';
import CollectionsService from './collections.service';
import FirebaseStorageService from '../../firebase/services/firebase-storage.service';
import * as moment from 'moment';
import { ContractReceipt } from 'ethers';
import { CollectionStatusEnum } from '../../web3/enum/collection-status.enum';
import firebase from 'firebase/compat';
import Web3Service from '../../web3/services/web3.service';
import { CollectionBasicChainDataType } from '../types/collection-basic-chain-data.type';
import { Contract } from '@ethersproject/contracts';
import FirestoreService from '../../firebase/services/firestore.service';
import {
	collection as colRef,
	doc as docRef,
	Firestore,
	getDoc,
	getDocs,
	limit,
	onSnapshot,
	orderBy,
	query,
	QueryConstraint,
	where
} from '@firebase/firestore';
import { IMintData } from '../../collections/interfaces/mint-data.interface';
import { ICollectionStatistics } from '../../collections/interfaces/collection-statistics.interface';
import { NavigationEnd, Router } from '@angular/router';
import { ContentTypeEnum } from '../../collections/enums/content-type.enum';
import Unsubscribe = firebase.Unsubscribe;

@Injectable()
export default class CollectionsDataService {
	public get publicCollections(): ICollectionDetails[] {
		return this.publicCollections$.getValue();
	}

	public get hasFilters(): boolean {
		return this.filters.name.length > 0
			|| this.filters.isFree
			|| this.filters.isMintActive
			|| this.filters.contentType !== undefined;
	}

	public readonly publicCollections$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>([]);
	public readonly searchedCollections$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>([]);
	public readonly filteredPublicCollections$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>([]);
	public readonly userCollections$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>(undefined);
	public readonly selectedUserCollections$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>(undefined);
	public readonly createdCollectionsFromChain$: BehaviorSubject<ICollectionDetails[]> = new BehaviorSubject<ICollectionDetails[]>(undefined);
	public readonly areNewPublicCollectionsAvailable$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	public readonly trendingCollectionsStats$: BehaviorSubject<ICollectionStatistics[]> = new BehaviorSubject<ICollectionStatistics[]>([]);
	public readonly defaultPagination: number;
	public readonly maxPagination: number;
	public pagination: number;
	public itemsPerLoad: number;
	public filters: {
		name: string;
		isFree: boolean;
		isMintActive: boolean;
		contentType?: ContentTypeEnum;
	};
	public sortBy: 'newest' | 'oldest';
	private publicCollectionsListener: Promise<Unsubscribe>;
	private connectedUserCollectionsListener: Promise<Unsubscribe>;
	private selectedUserCollectionsListener: Promise<Unsubscribe>;
	private trendingCollectionsListener: Promise<Unsubscribe>;
	private isPublicCollectionsLoadingBlocked: boolean;
	private areNewPublicCollectionsAvailable: boolean;

	constructor(
		private readonly firestoreService: FirestoreService,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly web3Service: Web3Service,
		private readonly collectionsService: CollectionsService,
		private readonly router: Router,
	) {
		this.defaultPagination = 40;
		this.maxPagination = this.defaultPagination * 3;
		this.pagination = this.defaultPagination;
		this.itemsPerLoad = this.defaultPagination;
		this.sortBy = 'newest';
		this.filters = {
			name: '',
			isFree: undefined,
			isMintActive: undefined,
			contentType: undefined,
		};
	}

	public async run(): Promise<void> {
		let isListeningToPublicCollections: boolean = false;

		this.router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				if (isListeningToPublicCollections || event.url === '/token' || event.url === '/community/airdrop-verification') {
					return;
				}
				isListeningToPublicCollections = true;
				this.getPublicCollections();
			}
		});

		setTimeout(() => {
			this.web3Service.provider$.subscribe(async (provider) => {
				if (!provider) {
					return;
				}

				const walletInterval: NodeJS.Timer = setInterval(async () => {
					if (this.web3Service.isConnected()) {
						const userAddress: string = await provider.getSigner()?.getAddress();

						if (userAddress) {
							clearInterval(walletInterval);
							this.listenToConnectedUserCollectionsLegacy(userAddress);
						}
					}
				}, 250);
			});

			this.userCollections$.subscribe((userCollections) => {
				if (!userCollections?.length) {
					return;
				}

				this.listenToUserCollectionsState();
			});
		}, 250);
	}

	public async listenToConnectedUserCollections(userAddress: string): Promise<void> {
		const supportedChains: ChainSymbolEnum[] = environment.supportedChains;
		const firestore: Firestore = this.firestoreService.firestore$.getValue();

		if (!firestore) {
			throw new Error('No Firestore at listenToConnectedUserCollections()');
		}

		if (this.connectedUserCollectionsListener) {
			return;
		}

		const qb = query(
			colRef(firestore, 'collections'),
			where('creator', '==', userAddress),
			where('chainSymbol', 'in', supportedChains),
			orderBy('createdAt', 'desc'),
		);

		// @ts-ignore
		this.connectedUserCollectionsListener = onSnapshot<ICollectionDetails>(qb, async (querySnapshot) => {
			const userCollections: ICollectionDetails[] = [];

			querySnapshot.forEach((doc) => {
				userCollections.push(doc.data());
			});

			if (userCollections?.length) {
				this.userCollections$.next(userCollections);

				for (const collection of this.userCollections$.getValue()) {
					if (collection.imageURL || collection.isImageLoading) {
						continue;
					}
					collection.isImageLoading = true;

					this.firebaseStorageService.getIpfsFileURL(collection.fileStoragePath).then((url) => {
						collection.imageURL = url;
					});
				}

				return;
			}
			this.userCollections$.next([]);
		});
	}

	public async listenToConnectedUserCollectionsLegacy(userAddress: string): Promise<void> {
		if (!this.firebaseDatabaseService.database$.getValue()) {
			throw new Error('No DB at getUserCollections()');
		}

		if (this.connectedUserCollectionsListener) {
			return;
		}

		this.connectedUserCollectionsListener = this.firebaseDatabaseService.get<Record<string, ICollectionDetails>>(
			'/collections/',
			{
				orderBy: 'creator',
				equalTo: userAddress,
			},
			async (creatingCollections) => {
				if (creatingCollections) {
					const supportedChains: ChainSymbolEnum[] = environment.supportedChains;
					const userCollections: ICollectionDetails[] = Object.values(creatingCollections)
						.reverse()
						.filter((collection) => supportedChains.includes(collection.chainSymbol));

					this.userCollections$.next(Object.values(userCollections));

					for (const collection of this.userCollections$.getValue()) {
						if (collection.imageURL || collection.isImageLoading) {
							return;
						}
						collection.isImageLoading = true;

						this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
							collection.imageURL = url;
						});
					}

					return;
				}

				this.userCollections$.next([]);
			},
			async () => {
				console.error(`Could not getUserCollections of user: ${userAddress}`);
			},
		);
	}

	public async listenToUserCollectionsState(): Promise<void> {
		if (!this.userCollections$?.getValue()) {
			return;
		}

		this.checkUserCollectionsStateSavedInDatabase();
	}

	public async listenToSelectedUserCollections(userAddress: string): Promise<void> {
		if (!this.firebaseDatabaseService.database$.getValue()) {
			throw new Error('No DB at listenToSelectedUserCollections()');
		}

		if (this.selectedUserCollectionsListener) {
			await this.firebaseDatabaseService.unsubscribe(this.selectedUserCollectionsListener);
		}

		this.selectedUserCollectionsListener = this.firebaseDatabaseService.get<Record<string, ICollectionDetails>>(
			'/collections/',
			{
				orderBy: 'creator',
				equalTo: userAddress,
			},
			async (creatingCollections) => {
				if (creatingCollections) {
					const supportedChains: ChainSymbolEnum[] = environment.supportedChains;
					const userCollections: ICollectionDetails[] = Object.values(creatingCollections)
						.reverse()
						.filter((collection) => supportedChains.includes(collection.chainSymbol));

					this.selectedUserCollections$.next(Object.values(userCollections));

					for (const collection of this.selectedUserCollections$.getValue()) {
						if (collection.imageURL || collection.isImageLoading) {
							return;
						}
						collection.isImageLoading = true;

						this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
							collection.imageURL = url;
						});
					}

					return;
				}

				this.selectedUserCollections$.next([]);
			},
			async () => {
				console.error(`Could not listenToSelectedUserCollections of user: ${userAddress}`);
			},
		);
	}

	public resetPublicCollectionsFilters(): void {
		this.filters = {
			name: '',
			isFree: undefined,
			isMintActive: undefined,
			contentType: undefined,
		};
		this.pagination = this.defaultPagination;
		this.getPublicCollections();
	}

	public async getPublicCollections(): Promise<void> {
		const supportedChains: ChainSymbolEnum[] = environment.supportedChains;
		const firestore: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at listenToPublicCollection()');
		}

		if (this.publicCollectionsListener) {
			await this.firestoreService.unsubscribe(this.publicCollectionsListener);
		}
		const queryConstraints: QueryConstraint[] = [
			where('isPublic', '==', true),
			where('status', '==', CollectionStatusEnum.CREATED),
			where('chainSymbol', 'in', supportedChains),
			orderBy('createdAt', this.sortBy === 'newest' ? 'desc' : 'asc'),
			limit(this.pagination),
		];

		if (this.filters.isFree) {
			queryConstraints.push(
				where('mintPrice', '==', 0),
			);
		}

		if (this.filters.contentType && this.filters.contentType !== ContentTypeEnum.ART) {
			queryConstraints.push(
				where('contentType', '==', this.filters.contentType),
			);
		}

		const qb = query(
			colRef(firestore, 'collections'),
			...queryConstraints,
		);

		// @ts-ignore
		const querySnapshot = await getDocs<ICollectionDetails>(qb);
		const publicCollections: ICollectionDetails[] = [];

		querySnapshot.forEach((doc) => {
			publicCollections.push(doc.data());
		});

		if (publicCollections?.length) {
			const collections: ICollectionDetails[] = [];

			for (const publicCollection of publicCollections) {
				if (this.filters.isMintActive && !(await this.isCollectionActive(publicCollection))) {
					continue;
				}

				if (
					this.filters.contentType === ContentTypeEnum.ART
					&& (publicCollection.contentType && publicCollection.contentType !== ContentTypeEnum.ART)
				) {
					continue;
				}

				this.firebaseStorageService.getIpfsFileURL(publicCollection.fileURI).then((url) => {
					publicCollection.imageURL = url;
				});

				collections.push(publicCollection);
			}
			this.publicCollections$.next(collections);

			return;
		}
		this.publicCollections$.next([]);
	}

	public async getCollectionsByName(collectionName: string): Promise<ICollectionDetails[]> {
		if (collectionName?.length < 3) {
			return;
		}
		const supportedChains: ChainSymbolEnum[] = environment.supportedChains;
		const firestore: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at getPublicCollectionsByName()');
		}

		const queryConstraints: QueryConstraint[] = [
			where('isPublic', '==', true),
			where('status', '==', CollectionStatusEnum.CREATED),
			where('chainSymbol', 'in', supportedChains),
			where('collectionName', '>=', collectionName),
			where('collectionName', '<=', collectionName + '\uf8ff'),
			orderBy('collectionName'),
			orderBy('createdAt', this.sortBy === 'newest' ? 'desc' : 'asc'),
		];

		const qb = query(
			colRef(firestore, 'collections'),
			...queryConstraints,
		);

		// @ts-ignore
		const querySnapshot = await getDocs<ICollectionDetails>(qb);
		const searchedCollections: ICollectionDetails[] = [];

		querySnapshot.forEach((doc) => {
			searchedCollections.push(doc.data());
		});

		if (searchedCollections?.length) {
			const collections: ICollectionDetails[] = [];

			for (const searchedCollection of searchedCollections) {
				this.firebaseStorageService.getIpfsFileURL(searchedCollection.fileURI).then((url) => {
					searchedCollection.imageURL = url;
				});

				collections.push(searchedCollection);
			}
			this.searchedCollections$.next(collections);

			return;
		}
		this.searchedCollections$.next([]);
	}

	public async listenToPublicCollectionLegacy(): Promise<void> {
		const supportedChains: ChainSymbolEnum[] = environment.supportedChains;

		if (!this.firebaseDatabaseService.database$.getValue()) {
			throw new Error('No DB at getPublicCollections()');
		}

		if (this.publicCollectionsListener) {
			await this.firebaseDatabaseService.unsubscribe(this.publicCollectionsListener);
		}

		this.isPublicCollectionsLoadingBlocked = false;
		this.publicCollectionsListener = this.firebaseDatabaseService.get<Record<string, ICollectionDetails>>(
			'/collections/',
			{
				limit: this.pagination,
				sortOrder: this.sortBy === 'newest' ? 'DESC' : 'ASC',
			},
			async (createdCollections) => {
				if (this.isPublicCollectionsLoadingBlocked) {
					this.areNewPublicCollectionsAvailable = true;
					this.areNewPublicCollectionsAvailable$.next(this.areNewPublicCollectionsAvailable);

					return;
				}
				this.isPublicCollectionsLoadingBlocked = true;
				this.areNewPublicCollectionsAvailable = false;
				this.areNewPublicCollectionsAvailable$.next(this.areNewPublicCollectionsAvailable);

				if (createdCollections) {
					const publicCollections: ICollectionDetails[] = Object.values(createdCollections).filter((collection) => {
						const isPublic: boolean = collection.isPublic
							&& collection.status === CollectionStatusEnum.CREATED
							&& !(supportedChains.length !== environment.allChainsCount && !supportedChains.includes(collection.chainSymbol));

						if (isPublic) {
							this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
								collection.imageURL = url;
							});
						}

						return isPublic;
					});
					this.publicCollections$.next(this.sortCollections(publicCollections));

					if (this.hasFilters) {
						await this.onFilter();
					}

					return;
				}
				this.publicCollections$.next([]);
				this.filteredPublicCollections$.next([]);
			},
			async () => {
				console.error(`Could not get PUBLIC COLLECTIONS`);
			},
		);
	}

	public async getTrendingCollections(minutes: number): Promise<void> {
		const db: Firestore = this.firestoreService.firestore$.getValue();

		if (!this.firestoreService.firestore$.getValue()) {
			throw new Error('No Firestore at listenToTrendingCollections()');
		}

		try {
			const documentRef = docRef(db, 'collectionsRankings', Math.round(minutes).toString());
			const docSnap = await getDoc(documentRef);

			if (docSnap.exists()) {
				const docData = docSnap.data();

				if (docData.at > moment().subtract(6, 'hours').unix()) {
					const statistics: ICollectionStatistics[] = JSON.parse(docData.data);

					this.trendingCollectionsStats$.next(statistics);

					return;
				}
			}

			if (this.trendingCollectionsListener) {
				await this.firestoreService.unsubscribe(this.trendingCollectionsListener);
			}
			const fromUnix: number = moment().subtract(minutes, 'minutes').unix();
			const qb = query(
				colRef(db, 'mints'),
				where('at', '>=', fromUnix),
			);
			// @ts-ignore
			const querySnapshot = await getDocs<IMintData>(qb);
			const mints: IMintData[] = [];
			const collectionsStatistics: ICollectionStatistics[] = [];

			querySnapshot.forEach((doc) => {
				mints.push(doc.data());
			});

			if (!mints.length) {
				this.trendingCollectionsStats$.next([]);

				return;
			}

			for (const mint of mints) {
				const collectionId: string = mint.collectionId;
				const collectionStatistics: ICollectionStatistics = collectionsStatistics.find(
					(statistics) => statistics.id === collectionId,
				);

				if (!collectionStatistics) {
					const newStatistic: ICollectionStatistics = {
						id: mint.collectionId,
						name: mint.collectionName,
						minted: 1,
						volume: mint.price,
					};

					collectionsStatistics.push(newStatistic);
					this.firebaseStorageService.getIpfsFileURL(mint.collectionFileURI).then((url) => {
						newStatistic.fileURI = url;
					});
				} else {
					collectionStatistics.minted++;
					collectionStatistics.volume += mint.price;
				}
			}
			const trendingStatistics: ICollectionStatistics[] = collectionsStatistics
				.sort((a, b) => b.minted - a.minted)
				.slice(0, 10);

			this.trendingCollectionsStats$.next(trendingStatistics);
			this.firestoreService.set(
				`collectionsRankings`,
				Math.round(minutes).toString(),
				{
					at: moment().unix(),
					data: JSON.stringify(trendingStatistics),
				},
			);
		} catch (e) {
			console.error(e);
		}
	}

	public loadNewPublicCollections(): void {
		this.listenToPublicCollectionLegacy();
	}

	public onSortChange(): void {
		this.getPublicCollections();
	}

	public async onFilter(): Promise<void> {
		if (!this.hasFilters) {
			this.resetPublicCollectionsFilters();

			return;
		}
		this.getPublicCollections();
	}

	public onPaginationChange(): void {
		if (this.pagination >= this.maxPagination) {
			return;
		}
		const pagination: number = this.pagination + this.itemsPerLoad;

		this.pagination = pagination > this.maxPagination ? this.maxPagination : pagination;
		this.getPublicCollections();
	}

	public async checkIsCollectionCreated(collection: ICollectionDetails): Promise<boolean> {
		if (collection?.onChainLoaders?.isCheckingIfCreated) {
			return;
		}
		collection.onChainLoaders = {
			isCheckingIfCreated: true,
		};
		const supportedChainsSymbols: ChainSymbolEnum[] = environment.supportedChains;

		if (!supportedChainsSymbols.includes(collection.chainSymbol)) {
			collection.onChainLoaders.isCheckingIfCreated = false;

			return;
		}
		const repository: Contract = await this.collectionsService.getRepositoryContract(collection.chainSymbol);

		if (!repository) {
			console.error(`getAllUserCollectionsFromSelectedChain() - No ${collection.chainSymbol} repository`);
			collection.onChainLoaders.isCheckingIfCreated = false;

			return;
		}
		const userCollectionsAddresses: string[] = await repository.getAllByUser(collection.creator);
		let createdCollection: CollectionBasicChainDataType;

		for (const collectionAddress of [...userCollectionsAddresses].reverse()) {
			try {
				const collectionFromChain: CollectionBasicChainDataType = await this.collectionsService
					.getCollectionStateFromChain(collectionAddress, collection.chainSymbol);

				if (collectionFromChain.metadataURI !== collection.metadataURI) {
					continue;
				}
				createdCollection = collectionFromChain;
				break;
			} catch (e) {
				console.error(e);
			}
		}

		if (!createdCollection) {
			if (collection.status === CollectionStatusEnum.INITIATED) {
				const txHash: string | null = await this.collectionsService.getTxHashFromDatabase(`/transactions/${collection.creator}/createCollection/${collection.id}`);

				if (!txHash) {
					if (collection.reason !== 'incomplete') {
						this.setCollectionStatus(collection, CollectionStatusEnum.INITIATED, 'incomplete');
					}
					collection.onChainLoaders.isCheckingIfCreated = false;

					return;
				}
				const creatingCollectionTxResult: ContractReceipt = await this.collectionsService.getTransactionResult(
					txHash,
					collection.chainSymbol,
				);

				if (creatingCollectionTxResult.status === 1) {
					collection.onChainLoaders.isCheckingIfCreated = false;

					return;
				}
				console.debug('set INITIATED to ERROR because status !== 1');
				this.setCollectionStatus(collection, CollectionStatusEnum.ERROR, 'txError');
			}

			// // Handling Omnichain collection create with success tx on srcChain, but in fact not created on dstChain (e.g. error or LZ issue):
			// if (collection.status === CollectionStatusEnum.CREATING
			// 	&& moment(collection.createdAt).isBefore(moment().subtract(24, 'hours'))
			// ) {
			// 	this.setCollectionStatus(collection, CollectionStatusEnum.ERROR, 'dstChainNoCollection');
			// }
			collection.onChainLoaders.isCheckingIfCreated = false;

			return;
		}
		collection.status = CollectionStatusEnum.CREATED;
		collection.collectionAddress = createdCollection.collectionAddress;
		collection.tokenIds = createdCollection.tokenIds;

		try {
			await this.setCollectionStatus(collection, CollectionStatusEnum.CREATED);
			this.createdCollectionsFromChain$.next([
				...(this.createdCollectionsFromChain$.getValue() || []),
				collection,
			]);
			collection.onChainLoaders.isCheckingIfCreated = false;

			return true;
		} catch (e) {
			console.error(e);
		}
		collection.onChainLoaders.isCheckingIfCreated = false;

		return;
	}

	public async setCollectionPublic(collectionDetails: ICollectionDetails): Promise<void> {
		if (collectionDetails.isPublic) {
			return;
		}

		try {
			await this.firestoreService.set(
				'collections',
				collectionDetails.firestoreId,
				{
					isPublic: true,
				},
			);

			// TODO: (Must) After full migration from realtimeDB to firestore remove rtDb and add here: collectionDetails.isPublic = true;
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.update<Pick<ICollectionDetails, 'isPublic'>>(
			`/collections/${collectionDetails.id}`,
			{
				isPublic: true,
			},
			async () => {
				collectionDetails.isPublic = true;
			},
			async () => {
				console.error(`Couldn\'t set creating collection: ${collectionDetails.collectionName} | ${collectionDetails.collectionAddress} to public`);
			}
		);
	}

	public async setCollectionPrivate(collectionDetails: ICollectionDetails): Promise<void> {
		if (!collectionDetails.isPublic) {
			return;
		}

		try {
			await this.firestoreService.set(
				'collections',
				collectionDetails.firestoreId,
				{
					isPublic: false,
				},
			);

			// TODO: (Must) After full migration from realtimeDB to firestore remove rtDb and add here: collectionDetails.isPublic = false;
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.update<Pick<ICollectionDetails, 'isPublic'>>(
			`/collections/${collectionDetails.id}`,
			{isPublic: false},
			async () => {
				collectionDetails.isPublic = false;
			},
			async () => {
				console.error(`Couldn\'t set creating collection: ${collectionDetails.collectionName} | ${collectionDetails.collectionAddress} to private`);
			}
		);
	}

	public async setCollectionStatus(collection: ICollectionDetails, status: CollectionStatusEnum, reason?: string): Promise<void> {
		try {
			await this.firestoreService.set(
				'collections',
				collection.firestoreId,
				{status, collectionAddress: collection?.collectionAddress || '', reason: reason || ''}
			);

			// TODO: (Must) After full migration from realtimeDB to firestore remove rtDb
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.update<Pick<ICollectionDetails, 'status' | 'collectionAddress' | 'reason'>>(
			`/collections/${collection.id}`,
			{status, collectionAddress: collection?.collectionAddress || '', reason: reason || ''},
			undefined,
			async () => {
				console.error(`Couldn\'t set collection: ${collection.collectionName} | ${collection?.collectionAddress || null} status to ${status}`);
			}
		);
	}

	public async getCollectionDetails(
		id: number,
		onSuccess: (result: ICollectionDetails) => Promise<void>,
		onError: () => Promise<void>,
	): Promise<void> {
		await this.firebaseDatabaseService.getById(
			`/collections/${id}`,
			onSuccess,
			onError,
		);

		// mklmlkn.
	}

	private async isCollectionActive(collection: ICollectionDetails): Promise<boolean> {
		const from: number = collection.dropFrom;
		const to: number = collection.dropTo;
		const isOngoing: boolean = (!from || (moment().unix() >= from && moment().unix() <= to));

		if (!collection.totalSupply) {
			return isOngoing;
		}

		// TODO: Query via Firestore after implementing mechanism for storing current tokenIds (minted count)
		return collection.totalSupply > collection.tokenIds;
	}

	private sortCollections(collections: ICollectionDetails[]): ICollectionDetails[] {
		return this.sortBy === 'newest' ? [...collections.reverse()] : collections;
	}

	private checkUserCollectionsStateSavedInDatabase(): void {
		for (const userCollection of this.userCollections$.getValue()) {
			if (![CollectionStatusEnum.INITIATED, CollectionStatusEnum.CREATING].includes(userCollection.status)) {
				continue;
			}
			this.checkIsCollectionCreated(userCollection).then((isCreated) => {
				if (isCreated) {
					return;
				}

				const collectionStatusInterval: NodeJS.Timer = setInterval(() => {
					this.checkIsCollectionCreated(userCollection).then((isFinallyCreated) => {
						if (!isFinallyCreated) {
							return;
						}

						clearInterval(collectionStatusInterval);
					});
				}, 30000);
			});
		}
	}
}
