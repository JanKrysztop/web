import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import CollectionsService from './collections.service';
import * as moment from 'moment';
import { INetworkTaskState } from '../../web3/interfaces/network-task-state.interface';
import FirebaseStorageService from '../../firebase/services/firebase-storage.service';

@Injectable({
	providedIn: 'root',
})
export default class CollectionsChainDataService {
	public get collections(): ICollectionDetails[] {
		return this.data.collections$.getValue();
	}

	public get isCollectionsLoaded(): boolean {
		return this.loaders.collections$.getValue();
	}

	public get hasFilters(): boolean {
		return this.filters.name.length > 0 || this.filters.isFree || this.filters.isMintActive;
	}

	public get collectionsToLoad(): number {
		return this.pagination;
	}

	public data: Record<string, BehaviorSubject<ICollectionDetails[]>> = {
		collections$: new BehaviorSubject<ICollectionDetails[]>([]),
		collectionsByName$: new BehaviorSubject<ICollectionDetails[]>([]),
	};
	public loaders: Record<string, BehaviorSubject<boolean>> = {
		collections$: new BehaviorSubject<boolean>(false),
		collectionsByName$: new BehaviorSubject<boolean>(false),
	};
	public tasks: Record<string, Record<ChainSymbolEnum, BehaviorSubject<INetworkTaskState>>> = {
		collections$: {
			[ChainSymbolEnum.eth]: new BehaviorSubject<INetworkTaskState>(undefined),
			[ChainSymbolEnum.bsc]: new BehaviorSubject<INetworkTaskState>(undefined),
			[ChainSymbolEnum.avax]: new BehaviorSubject<INetworkTaskState>(undefined),
			[ChainSymbolEnum.polygon]: new BehaviorSubject<INetworkTaskState>(undefined),
			[ChainSymbolEnum.arb]: new BehaviorSubject<INetworkTaskState>(undefined),
			[ChainSymbolEnum.optimism]: new BehaviorSubject<INetworkTaskState>(undefined),
			[ChainSymbolEnum.ftm]: new BehaviorSubject<INetworkTaskState>(undefined),
		},
	};
	public filters: {
		name: string;
		isFree: boolean;
		isMintActive: boolean;
	};
	public sortBy: 'newest' | 'oldest';
	public pagination: number;
	public maxPagination: number;
	public itemsPerLoad: number;
	public page: number;
	public filteredCollections: ICollectionDetails[];
	public collectionsFilteredByName: ICollectionDetails[];
	public allCollections: ICollectionDetails[];
	private loadingId: number;
	private paginatedCollections: ICollectionDetails[];
	private isPaginating: boolean;
	private readonly defaultPagination: number = 12;

	public constructor(
		private readonly collectionsService: CollectionsService,
		private readonly firebaseStorageService: FirebaseStorageService,
	) {
		this.pagination = this.defaultPagination;
		this.itemsPerLoad = this.defaultPagination;
		this.maxPagination = this.defaultPagination * 12; // 96
		this.filteredCollections = [];
		this.collectionsFilteredByName = [];
		this.allCollections = [];
		this.sortBy = 'newest';
		this.page = 1;
		this.filters = {
			name: '',
			isFree: undefined,
			isMintActive: undefined,
		};
		this.loadingId = 0;
	}

	public resetFilters(): void {
		this.page = 1;
		this.isPaginating = false;
		this.pagination = this.defaultPagination;
		this.filters = {
			name: '',
			isFree: undefined,
			isMintActive: undefined,
		};
		this.loadCollections();
	}

	public onPaginationChange(): void {
		if (this.pagination >= this.maxPagination) {
			return;
		}
		const pagination: number = this.pagination + this.itemsPerLoad;

		this.isPaginating = true;
		this.pagination = pagination > this.maxPagination ? this.maxPagination : pagination;
		this.page++;
		this.loadCollections(true);
	}

	public async loadCollections(onPaginationChange?: boolean): Promise<void> {
		this.loaders.collections$.next(false);

		if (!onPaginationChange) {
			this.page = 1;
			this.pagination = this.defaultPagination;
			this.isPaginating = false;
			this.loadingId++;
			this.data.collections$.next([]);
			this.paginatedCollections = [];
		} else if (this.page > 1) {
			this.paginatedCollections = [...this.collections];
		}
		const loadingCollections: ICollectionDetails[] = [];

		for (const chainSymbol in this.tasks.collections$) {
			if (!this.tasks.collections$[chainSymbol]) {
				continue;
			}
			this.onLoadingStateChange(this.loadingId, chainSymbol as ChainSymbolEnum, undefined, onPaginationChange);
		}
		this.hasFilters
			? this.loadFilteredCollections(this.loadingId, loadingCollections, onPaginationChange)
			: this.loadPaginatedCollections(this.loadingId, loadingCollections, onPaginationChange);
	}

	public searchByName(name: string): void {
		this.loaders.collectionsByName$.next(false);

		if (!name) {
			this.filters.name = '';
			this.loaders.collectionsByName$.next(true);

			return;
		}

		if (!this.allCollections.length) {
			this.filters.name = name;
			this.loadAllCollectionsByName(name);

			return;
		}
		this.collectionsFilteredByName = [];
		this.data.collectionsByName$.next(this.collectionsFilteredByName);

		for (const collection of this.allCollections) {
			if (!collection.collectionName.toLowerCase().includes(name.toLowerCase())) {
				continue;
			}

			this.collectionsFilteredByName.push(collection);
			this.data.collectionsByName$.next(this.collectionsFilteredByName);
		}

		if (this.data.collectionsByName$.getValue().length !== this.collectionsFilteredByName.length) {
			this.data.collectionsByName$.next(this.collectionsFilteredByName);
		}
		this.loaders.collectionsByName$.next(true);
	}

	public async getCollectionDetails(collectionAddress: string, collectionChainSymbol: ChainSymbolEnum): Promise<ICollectionDetails> {
		return await this.collectionsService.getCollectionDetails(
			collectionAddress,
			collectionChainSymbol,
		);
	}

	private onCollectionsLoaded(loadingId: number, loadingCollections: ICollectionDetails[], onPaginationChanged?: boolean) {
		if (this.isOldSearch(loadingId, onPaginationChanged)) {
			return;
		}
		const offset: number = (this.page - 1) * this.itemsPerLoad;
		const collections: ICollectionDetails[] = loadingCollections
			.sort((a, b) => this.sortBy === 'newest' ? (b.createdAt - a.createdAt) : (a.createdAt - b.createdAt))
			.slice(offset, offset + this.itemsPerLoad);

		this.data.collections$.next(onPaginationChanged ? [...this.paginatedCollections, ...collections] : collections);

		if (loadingCollections.length) {
			this.loaders.collections$.next(true);
		}
	}

	private onLoadingStateChange(loadingId: number, chainSymbol: ChainSymbolEnum, state: INetworkTaskState, onPaginationChanged?: boolean) {
		if (this.isOldSearch(loadingId, onPaginationChanged)) {
			return;
		}

		this.tasks.collections$[chainSymbol].next(state);
	}

	private loadPaginatedCollections(loadingId: number, loadingCollections: ICollectionDetails[], onPaginationChanged?: boolean) {
		const supportedChainsSymbols: ChainSymbolEnum[] = environment.supportedChains;
		let chainIndex: number = 0;

		if (this.isOldSearch(loadingId, onPaginationChanged)) {
			return;
		}
		const preloadingInterval: NodeJS.Timeout = setInterval(() => {
			this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);

			if (chainIndex === supportedChainsSymbols.length) {
				clearInterval(preloadingInterval);
			}
			return;
		}, 7500);

		for (const chainSymbol of supportedChainsSymbols) {
			if (this.isOldSearch(loadingId, onPaginationChanged)) {
				return;
			}

			this.onLoadingStateChange(loadingId, chainSymbol, {
				status: 'pending',
				progress: 0,
			}, onPaginationChanged);
			let collectionIndex: number = 0;
			const chainStateInterval: NodeJS.Timeout = setInterval(() => {
				this.onLoadingStateChange(loadingId, chainSymbol, {
					status: 'pending',
					progress: collectionIndex ? ((collectionIndex / this.pagination) * 100) : 0,
				}, onPaginationChanged);

				if (chainIndex === supportedChainsSymbols.length) {
					clearInterval(chainStateInterval);
				}
				return;
			}, 7500);

			const chainLoadingTimeout: NodeJS.Timeout = setTimeout(() => {
				console.error(`${chainSymbol} loading timeout - ignoring this chain data`);
				// TODO: Enable retry etc.
				chainIndex++;
				clearInterval(chainStateInterval);
				this.onLoadingStateChange(loadingId, chainSymbol, {
					status: 'error',
					reason: 'timeout',
					// onRetry: () => {},
				}, onPaginationChanged);

				if (chainIndex === supportedChainsSymbols.length) {
					clearInterval(preloadingInterval);
					this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
				}

				return;
			}, 60000);

			if (this.isOldSearch(loadingId, onPaginationChanged)) {
				return;
			}
			this.collectionsService.getRepositoryContract(chainSymbol).then((repository) => {
				if (!repository) {
					console.error(`loadCollections() - No ${chainSymbol} repository`);
					clearTimeout(chainLoadingTimeout);
					clearInterval(chainStateInterval);
					chainIndex++;
					this.onLoadingStateChange(loadingId, chainSymbol, {
						status: 'error',
						reason: 'noRepository',
					}, onPaginationChanged);

					if (chainIndex === supportedChainsSymbols.length) {
						clearInterval(preloadingInterval);
						this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
					}

					return;
				}

				if (this.isOldSearch(loadingId, onPaginationChanged)) {
					return;
				}
				repository.getAll().then((collectionsAddresses: string[]) => {
					if (!collectionsAddresses?.length) {
						clearTimeout(chainLoadingTimeout);
						clearInterval(chainStateInterval);
						chainIndex++;
						this.onLoadingStateChange(loadingId, chainSymbol, {
							status: 'completed',
						}, onPaginationChanged);

						if (chainIndex === supportedChainsSymbols.length) {
							clearInterval(preloadingInterval);
							this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
						}

						return;
					}
					collectionsAddresses = [...collectionsAddresses].reverse().slice(0, this.pagination);

					for (const collectionAddress of collectionsAddresses) {
						if (this.isOldSearch(loadingId, onPaginationChanged)) {
							return;
						}
						this.collectionsService.getCollectionBasicDetails(collectionAddress, chainSymbol).then((collection) => {
							if (collection.hidden || this.isOldSearch(loadingId, onPaginationChanged)) {
								return;
							}
							this.firebaseStorageService.getIpfsFileURL(collection.fileURI).then((url) => {
								collection.imageURL = url;
							});

							loadingCollections.push(collection);
						}).finally(() => {
							collectionIndex++;
							const loadingTargetAmount: number = collectionsAddresses.length < this.pagination
								? collectionsAddresses.length
								: this.pagination;

							if (collectionIndex === loadingTargetAmount) {
								clearTimeout(chainLoadingTimeout);
								clearInterval(chainStateInterval);
								chainIndex++;
								this.onLoadingStateChange(loadingId, chainSymbol, {
									status: 'completed',
								}, onPaginationChanged);

								if (chainIndex === supportedChainsSymbols.length) {
									clearInterval(preloadingInterval);
									this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
								}
							}
						});
					}
				}).catch((e) => {
					clearTimeout(chainLoadingTimeout);
					clearInterval(chainStateInterval);
					console.error(e);
					chainIndex++;
					this.onLoadingStateChange(loadingId, chainSymbol, {
						status: 'error',
						reason: 'getAll',
					}, onPaginationChanged);

					if (chainIndex === supportedChainsSymbols.length) {
						clearInterval(preloadingInterval);
						this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
					}
				});
			}).catch((e) => {
				console.error(e);
				clearTimeout(chainLoadingTimeout);
				clearInterval(chainStateInterval);
				chainIndex++;
				this.onLoadingStateChange(loadingId, chainSymbol, {
					status: 'error',
					reason: 'getRepositoryContract',
				}, onPaginationChanged);

				if (chainIndex === supportedChainsSymbols.length) {
					clearInterval(preloadingInterval);
					this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
				}
			});
		}
	}

	private loadFilteredCollections(loadingId: number, loadingCollections: ICollectionDetails[], onPaginationChanged?: boolean) {
		const supportedChainsSymbols: ChainSymbolEnum[] = environment.supportedChains;
		let chainIndex: number = 0;

		if (this.isOldSearch(loadingId, onPaginationChanged)) {
			return;
		}
		const preloadingInterval: NodeJS.Timeout = setInterval(() => {
			this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);

			if (chainIndex === supportedChainsSymbols.length) {
				clearInterval(preloadingInterval);
			}
			return;
		}, 7500);

		for (const chainSymbol of supportedChainsSymbols) {
			this.onLoadingStateChange(loadingId, chainSymbol, {
				status: 'pending',
				progress: 0,
			}, onPaginationChanged);
			let collectionIndex: number = 0;
			const chainStateInterval: NodeJS.Timeout = setInterval(() => {
				this.onLoadingStateChange(loadingId, chainSymbol, {
					status: 'pending',
					progress: collectionIndex ? ((collectionIndex / this.pagination) * 100) : 0,
				}, onPaginationChanged);

				if (chainIndex === supportedChainsSymbols.length) {
					clearInterval(chainStateInterval);
				}
				return;
			}, 7500);
			const chainLoadingTimeout: NodeJS.Timeout = setTimeout(() => {
				console.error(`${chainSymbol} loading timeout - ignoring this chain data`);
				chainIndex++;
				clearInterval(chainStateInterval);
				this.onLoadingStateChange(loadingId, chainSymbol, {
					status: 'error',
					reason: 'timeout',
					// onRetry: () => {},
				}, onPaginationChanged);

				if (chainIndex === supportedChainsSymbols.length) {
					clearInterval(preloadingInterval);
					this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
				}
				return;
			}, 60000);

			if (this.isOldSearch(loadingId, onPaginationChanged)) {
				return;
			}
			this.collectionsService.getRepositoryContract(chainSymbol).then((repository) => {
				if (!repository) {
					console.error(`loadCollections() - No ${chainSymbol} repository`);
					clearTimeout(chainLoadingTimeout);
					clearInterval(chainStateInterval);
					chainIndex++;
					this.onLoadingStateChange(loadingId, chainSymbol, {
						status: 'error',
						reason: 'noRepository',
					}, onPaginationChanged);

					if (chainIndex === supportedChainsSymbols.length) {
						clearInterval(preloadingInterval);
						this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
					}
					return;
				}

				if (this.isOldSearch(loadingId, onPaginationChanged)) {
					return;
				}
				repository.getAll().then((collectionsAddresses: string[]) => {
					if (!collectionsAddresses?.length) {
						clearTimeout(chainLoadingTimeout);
						clearInterval(chainStateInterval);
						chainIndex++;
						this.onLoadingStateChange(loadingId, chainSymbol, {
							status: 'completed',
						}, onPaginationChanged);

						if (chainIndex === supportedChainsSymbols.length) {
							clearInterval(preloadingInterval);
							this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
						}

						return;
					}
					collectionsAddresses = this.sortBy === 'newest' ? [...collectionsAddresses].reverse() : collectionsAddresses;
					collectionsAddresses = collectionsAddresses.slice(0, this.pagination);

					for (const collectionAddress of collectionsAddresses) {
						if (this.isOldSearch(loadingId, onPaginationChanged)) {
							return;
						}

						this.collectionsService.getCollectionDetails(collectionAddress, chainSymbol).then((collection) => {
							if (collection.hidden || this.isOldSearch(loadingId, onPaginationChanged) || !this.isMatchingFilters(collection)) {
								return;
							}
							loadingCollections.push(collection);
							this.onLoadingStateChange(loadingId, chainSymbol, {
								status: 'pending',
								progress: ((collectionIndex / this.pagination) * 100),
							}, onPaginationChanged);
						}).finally(() => {
							collectionIndex++;
							const loadingTargetAmount: number = collectionsAddresses.length < this.pagination
								? collectionsAddresses.length
								: this.pagination;

							if (collectionIndex === loadingTargetAmount) {
								clearTimeout(chainLoadingTimeout);
								clearInterval(chainStateInterval);
								chainIndex++;
								this.onLoadingStateChange(loadingId, chainSymbol, {
									status: 'completed',
								}, onPaginationChanged);

								if (chainIndex === supportedChainsSymbols.length) {
									clearInterval(preloadingInterval);
									this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
								}
							}
						});
					}
				}).catch((e) => {
					clearTimeout(chainLoadingTimeout);
					clearInterval(chainStateInterval);
					console.error(e);
					chainIndex++;
					this.onLoadingStateChange(loadingId, chainSymbol, {
						status: 'error',
						reason: 'getAll',
					}, onPaginationChanged);

					if (chainIndex === supportedChainsSymbols.length) {
						clearInterval(preloadingInterval);
						this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
					}
				});
			}).catch((e) => {
				console.error(e);
				clearTimeout(chainLoadingTimeout);
				clearInterval(chainStateInterval);
				chainIndex++;
				this.onLoadingStateChange(loadingId, chainSymbol, {
					status: 'error',
					reason: 'getRepositoryContract',
				}, onPaginationChanged);

				if (chainIndex === supportedChainsSymbols.length) {
					clearInterval(preloadingInterval);
					this.onCollectionsLoaded(loadingId, loadingCollections, onPaginationChanged);
				}
			});
		}
	}

	private loadAllCollectionsByName(name: string) {
		const supportedChainsSymbols: ChainSymbolEnum[] = environment.supportedChains;
		let chainIndex: number = 0;

		for (const chainSymbol of supportedChainsSymbols) {
			const chainLoadingTimeout: NodeJS.Timeout = setTimeout(() => {
				console.error(`${chainSymbol} loading timeout - ignoring this chain data`);
				chainIndex++;
				if (chainIndex === supportedChainsSymbols.length) {
					this.data.collectionsByName$.next(this.collectionsFilteredByName);
					this.loaders.collectionsByName$.next(true);
				}
				return;
			}, 30000);

			this.collectionsService.getRepositoryContract(chainSymbol).then((repository) => {
				if (!repository) {
					console.error(`loadCollections() - No ${chainSymbol} repository`);
					clearTimeout(chainLoadingTimeout);
					chainIndex++;
					if (chainIndex === supportedChainsSymbols.length) {
						this.data.collectionsByName$.next(this.collectionsFilteredByName);
						this.loaders.collectionsByName$.next(true);
					}
					return;
				}

				repository.getAll().then((collectionsAddresses: string[]) => {
					if (!collectionsAddresses?.length) {
						clearTimeout(chainLoadingTimeout);
						chainIndex++;
						if (chainIndex === supportedChainsSymbols.length) {
							this.data.collectionsByName$.next(this.collectionsFilteredByName);
							this.loaders.collectionsByName$.next(true);
						}
					}

					let collectionIndex: number = 0;
					collectionsAddresses = [...collectionsAddresses].reverse().slice(0, this.pagination);

					for (const collectionAddress of collectionsAddresses) {
						this.collectionsService.getCollectionDetails(collectionAddress, chainSymbol).then((collection) => {
							if (collection.hidden) {
								return;
							}
							this.allCollections.push(collection);

							if (!collection.collectionName.toLowerCase().includes(name.toLowerCase())) {
								return;
							}

							this.collectionsFilteredByName.push(collection);
							this.data.collectionsByName$.next(this.collectionsFilteredByName);
						}).finally(() => {
							collectionIndex++;
							if (collectionIndex === collectionsAddresses.length) {
								clearTimeout(chainLoadingTimeout);
								chainIndex++;
								if (chainIndex === supportedChainsSymbols.length) {
									this.data.collectionsByName$.next(this.collectionsFilteredByName);
									this.loaders.collectionsByName$.next(true);
								}
							}
						});
					}
				}).catch((e) => {
					clearTimeout(chainLoadingTimeout);
					console.error(e);
					chainIndex++;
					if (chainIndex === supportedChainsSymbols.length) {
						this.data.collectionsByName$.next(this.collectionsFilteredByName);
						this.loaders.collectionsByName$.next(true);
					}
				});
			}).catch((e) => {
				console.error(e);
				clearTimeout(chainLoadingTimeout);
				chainIndex++;
				if (chainIndex === supportedChainsSymbols.length) {
					this.data.collectionsByName$.next(this.collectionsFilteredByName);
					this.loaders.collectionsByName$.next(true);
				}
			});
		}
	}


	private isMintOngoing(collection: ICollectionDetails): boolean {
		if (!collection.dropFrom) {
			return true;
		}

		return moment().isSameOrAfter(collection.dropFrom) && moment().isSameOrBefore(collection.dropTo);
	}

	private isMintActive(collection: ICollectionDetails): boolean {
		return (!collection.totalSupply || collection.tokenIds < collection.totalSupply) && this.isMintOngoing(collection);
	}

	private isMintFree(collection: ICollectionDetails): boolean {
		const isFree: boolean = !collection.mintPrice;

		return !((this.filters.isFree && !isFree) || !this.filters.isFree && isFree);
	}

	private isMatchingFilters(collection: ICollectionDetails): boolean {
		return !(this.filters.name && !collection.metadata?.name?.includes(this.filters.name)
			|| this.filters.isFree && !this.isMintFree(collection)
			|| this.filters.isMintActive && !this.isMintActive(collection));
	}

	private isOldSearch(loadingId: number, onPaginationChanged: boolean): boolean {
		return loadingId !== this.loadingId || (onPaginationChanged && !this.isPaginating);
	}
}
