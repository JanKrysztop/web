import { Component, OnDestroy, OnInit } from '@angular/core';
import Web3Service from '../../../web3/services/web3.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, Subscription } from 'rxjs';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import NotificationsService from '../../../shared/services/notifications.service';
import { environment } from '../../../../../environments/environment';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import { ChainNameToSymbolMap } from '../../../web3/types/chain-name-to-symbol-map.type';
import { ContractReceipt, ContractTransaction } from 'ethers';
import ONFTGatewayService from '../../services/nft-gateway.service';
import { INFTMetadata } from '../../../nfts/interfaces/nft-metadata.interface';

@Component({
	selector: 'app-onft-gateway',
	templateUrl: './onft-gateway.component.html',
	styleUrls: ['./onft-gateway.component.scss']
})
export class ONFTGatewayComponent implements OnInit, OnDestroy {
	public get isBridging(): boolean {
		return this.state?.status && (this.state.status === 'approving' || this.state.status === 'pending' || this.state.status === 'completed');
	}

	public get isDisabled(): boolean {
		return this.tokenURIError
			|| this.form.get('collectionAddress').value === undefined
			|| this.form.get('tokenId').value === undefined
			|| this.isBridging;
	}

	public isConnectedToWallet: boolean;
	public connectedChain: ChainSymbolEnum;
	public state: { status: 'pending' | 'approving' | 'approval_error' | 'error' | 'completed' };
	public form: FormGroup;
	public selectedChainName: SupportedChainNameEnum;
	public chainNameToSymbolMap: Partial<ChainNameToSymbolMap>;
	public tokenURI: string;
	public tokenURIError: boolean;
	public tokenImage: string;
	public isTokenImageLoaded: boolean;
	private subscriptions: Subscription;

	constructor(
		private readonly web3Service: Web3Service,
		private readonly formBuilder: FormBuilder,
		private readonly oNFTGatewayService: ONFTGatewayService,
		private readonly notificationService: NotificationsService,
	) {
	}

	public ngOnInit(): void {
		this.subscriptions = new Subscription();
		this.chainNameToSymbolMap = {};

		for (const chainName in environment.chainNameToSymbolMap) {
			if (
				!environment.chainNameToSymbolMap[chainName]
				|| !this.isSupportedNetwork(environment.chainNameToSymbolMap[chainName])
			) {
				continue;
			}
			this.chainNameToSymbolMap[chainName] = environment.chainNameToSymbolMap[chainName];
		}

		this.form = this.formBuilder.group({
			collectionAddress: [undefined, Validators.maxLength(150)],
			tokenId: [undefined, Validators.required],
			dstChainSymbol: [undefined, Validators.required],
		});

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();
					const userAddress: string = await this.web3Service.getUserAddress();

					if (!this.isConnectedToWallet || !userAddress) {
						return;
					}
					clearInterval(walletInterval);
					this.connectedChain = this.web3Service.getConnectedChainSymbol();
					this.chainNameToSymbolMap[environment.chainSymbolToNameMap[this.connectedChain]] = undefined;
					this.onTargetChainChange(
						this.connectedChain !== ChainSymbolEnum.eth
							? SupportedChainNameEnum.Ethereum
							: SupportedChainNameEnum.Polygon
					);
				}, 250);
			}),
		);

		this.subscriptions.add(
			this.form.valueChanges.pipe(
				debounceTime(500),
				distinctUntilChanged(),
			).subscribe(() => {
				if (!this.form.get('collectionAddress').value || this.form.get('tokenId').value === undefined) {
					return;
				}

				this.onTokenChange();
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public async onSubmit(): Promise<void> {
		if (this.tokenURIError) {
			return;
		}

		const collectionAddress: string = this.form.get('collectionAddress').value;
		const tokenId: number = this.form.get('tokenId').value;
		const dstChainSymbol: ChainSymbolEnum = this.form.get('dstChainSymbol').value;

		this.state = {
			status: null,
		};

		try {
			const tx: ContractTransaction = await this.oNFTGatewayService.sendTo(
				collectionAddress,
				tokenId,
				dstChainSymbol,
				this.state,
			);

			const txResult: ContractReceipt = await tx.wait();
			this.notificationService.success('Bridging NFT', 'Your NFT will be transferred to the destination chain in a few minutes');

			if (txResult && txResult.status !== 1) {
				this.notificationService.error('Unknown status', 'Couldn\'t confirm the tx status. Please, check it in the blockchain explorer.');
			}
		} catch (e) {
			console.error(e);
			this.notificationService.error('Error', 'Couldn\'t initialize bridging. Please, try again in a moment.');
		}
	}

	public connectWallet(): void {
		this.web3Service.connectWithModal();
	}

	public onTargetChainChange(chainName: SupportedChainNameEnum): void {
		const chainSymbol: ChainSymbolEnum = this.chainNameToSymbolMap[chainName];

		if (!chainSymbol) {
			this.notificationService.error('Invalid chain');
			return;
		}

		this.selectedChainName = chainName;
		this.form.get('dstChainSymbol').setValue(chainSymbol);
	}

	public mapChainNameToEnum(chainName: string): SupportedChainNameEnum {
		return chainName as SupportedChainNameEnum;
	}

	private isSupportedNetwork(chainSymbol: ChainSymbolEnum): boolean {
		return environment.supportedChains.includes(chainSymbol);
	}

	private async onTokenChange(): Promise<void> {
		this.tokenImage = undefined;
		this.isTokenImageLoaded = false;
		this.tokenURIError = false;
		this.tokenURI = undefined;
		const collectionAddress: string = this.form.get('collectionAddress').value;
		const tokenId: number = this.form.get('tokenId').value;

		if (!collectionAddress || tokenId === undefined || tokenId === null || tokenId.toString() === '') {
			return;
		}

		try {
			this.tokenURI = await this.oNFTGatewayService.getTokenURI(collectionAddress, tokenId);

			if (this.tokenURI) {
				fetch(this.tokenURI)
					.then((response) => response.json())
					.then((metadata?: INFTMetadata) => {
						if (!metadata || !metadata.image) {
							return;
						}

						this.tokenImage = metadata.image;
					});
			}
		} catch (e) {
			console.error(e);
			this.tokenURIError = true;
			this.notificationService.error('Token not found');
		}
	}
}
