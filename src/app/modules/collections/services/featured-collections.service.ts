import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ICollectionDetails } from '../../web3/interfaces/collection-details.interface';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';

@Injectable()
export default class FeaturedCollectionsService {
	public readonly collections$: BehaviorSubject<ICollectionDetails[]>;

	constructor() {
		const featuredCollections: ICollectionDetails[] = [
			{
				collectionAddress: '',
				imageURL: '',
				collectionName: '',
				totalSupply: 0,
				mintPrice: 0,
				chainSymbol: ChainSymbolEnum.eth,
				isImageLoaded: false,
				fileURI: '',
				createdAt: 0,
				tokenIds: 0,
			},
		];

		this.collections$.next(featuredCollections);
	}
}
