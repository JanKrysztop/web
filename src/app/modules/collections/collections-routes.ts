import { Routes } from '@angular/router';

export const collectionsRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./collections.module').then(m => m.CollectionsModule),
		data: { preload: true },
	},
];
