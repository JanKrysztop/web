import { NgModule } from '@angular/core';
import { CollectionsRoutingModule } from './collections-routing.module';
import { SharedModule } from '../shared/shared.module';
import { Web3Module } from '../web3/web3.module';
import { CollectionDetailsComponent } from './components/collection-details/collection-details.component';
import { UserCollectionsComponent } from './components/user-collections/user-collections.component';
import { AllCollectionsComponent } from './components/all-collections/all-collections.component';
import FeaturedCollectionsService from './services/featured-collections.service';
import { CollectionsRankingComponent } from './components/collections-ranking/collections-ranking.component';

@NgModule({
	providers: [
		FeaturedCollectionsService,
	],
	declarations: [
		CollectionDetailsComponent,
		UserCollectionsComponent,
		AllCollectionsComponent,
		CollectionsRankingComponent,
	],
	imports: [
		CollectionsRoutingModule,
		SharedModule,
		Web3Module,
	],
	exports: [
		CollectionDetailsComponent,
		UserCollectionsComponent,
		AllCollectionsComponent,
		CollectionsRankingComponent,
	],
})
export class CollectionsModule {
}
