import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import IpfsService from '../../../ipfs/services/ipfs.service';
import Web3Service from '../../../web3/services/web3.service';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import { Params, Router } from '@angular/router';
import NotificationsService from '../../../shared/services/notifications.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ethers } from 'ethers';
import { NgProgress, NgProgressRef } from 'ngx-progressbar';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { INetworkTaskState } from '../../../web3/interfaces/network-task-state.interface';
import { environment } from '../../../../../environments/environment';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import { inOutAnimation } from '../../../shared/data/animations/in-out-animation';
import CollectionsService from '../../../chain-data/services/collections.service';
import { ContentTypeEnum } from '../../enums/content-type.enum';
import { SupportedAssetEnum } from '../../../web3/enum/supported-asset.enum';

@Component({
	selector: 'app-all-collections',
	templateUrl: './all-collections.component.html',
	styleUrls: ['./all-collections.component.scss'],
	animations: inOutAnimation,
})
export class AllCollectionsComponent implements OnInit, OnDestroy {
	public get collections$(): BehaviorSubject<ICollectionDetails[]> {
		return this.collectionsDataService.publicCollections$;
	}

	public get isFilteringOnClient(): boolean {
		return this.collectionsDataService.filters.isMintActive
			|| this.collectionsDataService.filters.contentType === ContentTypeEnum.ART;
	}

	public get canPaginate(): boolean {
		return (this.collections.length >= this.collectionsDataService.pagination || this.isFilteringOnClient)
			&& this.collectionsDataService.pagination < this.collectionsDataService.maxPagination
			&& this.isLoaded;
	}

	public get sortBy(): 'newest' | 'oldest' {
		return this.collectionsDataService.sortBy;
	}

	public get hasFilters(): boolean {
		return this.collectionsDataService.hasFilters;
	}

	public get isActiveFilterOn(): boolean {
		return this.collectionsDataService.filters.isMintActive;
	}

	public get isFreeMintFilterOn(): boolean {
		return this.collectionsDataService.filters.isFree;
	}

	public get collectionsToLoad(): number {
		return this.collectionsDataService.pagination;
	}

	public progressRef: NgProgressRef;
	public isLoaded: boolean;
	public isLoadingError: boolean;
	public isEmpty: boolean;
	public subscriptions: Subscription;
	public isConnectedToWallet: boolean;
	public collections: ICollectionDetails[];
	public filteredCollections: ICollectionDetails[];
	public userAddress: string;
	public readonly placeholderItems: number[] = Array.from(Array(40).keys());
	public readonly chainsLoadingState: Partial<Record<ChainSymbolEnum, INetworkTaskState>>;
	public loadingStatus: 'pending' | 'completed' | 'error';
	public readonly supportedChains: ChainSymbolEnum[];
	public readonly chainSymbolToNameMap: Partial<Record<ChainSymbolEnum, SupportedChainNameEnum>>;
	public areNewPublicCollectionsAvailable: boolean;
	public contentTypes: ContentTypeEnum[];
	public selectedContentType?: ContentTypeEnum;

	constructor(
		private readonly ipfsService: IpfsService,
		private readonly web3Service: Web3Service,
		private readonly collectionsService: CollectionsService,
		private readonly collectionsDataService: CollectionsDataService,
		private readonly router: Router,
		private readonly notificationsService: NotificationsService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly progressBar: NgProgress,
	) {
		this.supportedChains = environment.supportedChains;
		this.chainSymbolToNameMap = environment.chainSymbolToNameMap;
		this.chainsLoadingState = {};
		for (const chainSymbol of this.supportedChains) {
			this.chainsLoadingState[chainSymbol] = {};
		}
	}

	public async ngOnInit(): Promise<void> {
		this.collections = this.collections$?.getValue() || [];
		this.progressRef = this.progressBar.ref('collectionsProgressBar');
		this.uiLoaderService.stopAll();
		this.subscriptions = new Subscription();
		this.contentTypes = [ContentTypeEnum.ART, ContentTypeEnum.MUSIC];

		this.subscriptions.add(this.collections$.subscribe((collections: ICollectionDetails[]) => {
			this.collections = [...collections];
			this.isLoaded = true;
			this.progressRef.complete();

			if (this.collections.length === 0) {
				setTimeout(() => {
					this.isEmpty = this.collections.length === 0;
				}, 2500);
			}
		}));

		this.subscriptions.add(
			this.collectionsDataService.areNewPublicCollectionsAvailable$.subscribe((areNewPublicCollectionsAvailable: boolean) => {
				this.areNewPublicCollectionsAvailable = areNewPublicCollectionsAvailable;

				this.areNewPublicCollectionsAvailable
					? this.uiLoaderService.stopBackgroundLoader('newPublicCollectionsLoader')
					: this.uiLoaderService.startBackgroundLoader('newPublicCollectionsLoader');
			}),
		);

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				setTimeout(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();
				}, 250);
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public resetFilters(): void {
		this.isLoaded = false;
		this.progressRef.start();
		this.collectionsDataService.resetPublicCollectionsFilters();
		this.selectedContentType = undefined;
	}

	public toggleIsActiveFilter(): void {
		this.isLoaded = false;
		this.progressRef.start();
		this.collectionsDataService.filters.isMintActive = !this.collectionsDataService.filters.isMintActive;
		this.collectionsDataService.onFilter();
	}

	public toggleIsFreeFilter(): void {
		this.isLoaded = false;
		this.progressRef.start();
		this.collectionsDataService.filters.isFree = !this.collectionsDataService.filters.isFree;
		this.collectionsDataService.onFilter();
	}

	public onSortingChange(value: 'newest' | 'oldest'): void {
		this.isLoaded = false;
		this.progressRef.start();
		this.collectionsDataService.sortBy = value;
		this.collectionsDataService.onSortChange();
	}

	public onContentTypeChange(contentType: ContentTypeEnum): void {
		this.selectedContentType = contentType;
		this.isLoaded = false;
		this.progressRef.start();
		this.collectionsDataService.filters.contentType = this.selectedContentType;
		this.collectionsDataService.onFilter();
	}

	public getFormattedPrice(mintPriceInUnit: number, assetName: string): string {
		return ethers.utils.formatUnits(mintPriceInUnit, environment.assetToDigits[SupportedAssetEnum[assetName]]);
	}

	public goToCollection(collection: ICollectionDetails): void {
		const queryParams: Params = {
			id: collection.id,
			chainSymbol: collection.chainSymbol,
			collectionAddress: collection.collectionAddress,
			metadataURI: collection.metadataURI,
			creatorAddress: collection.creator,
		};

		this.router.navigate(
			['/collections/details'],
			{queryParams},
		);
	}

	public goToCreate(): void {
		this.router.navigate(['/create']);
	}

	public onImageLoadError(collectionElement: HTMLElement): void {
		this.removeImage(collectionElement);
	}

	public loadMoreCollections(): void {
		if (!this.canPaginate) {
			return;
		}

		this.isLoaded = false;
		this.progressRef.start();
		this.collectionsDataService.onPaginationChange();
	}

	public loadNewPublicCollections(): void {
		if (!this.areNewPublicCollectionsAvailable) {
			return;
		}
		this.collectionsDataService.sortBy = 'newest';
		this.collectionsDataService.loadNewPublicCollections();
	}

	private removeImage(collectionElement: HTMLElement): void {
		collectionElement?.remove();
	}
}
