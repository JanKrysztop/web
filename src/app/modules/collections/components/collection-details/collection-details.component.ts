import { Component, OnDestroy, OnInit } from '@angular/core';
import { of, Subscription, switchMap } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import IpfsService from '../../../ipfs/services/ipfs.service';
import Web3Service from '../../../web3/services/web3.service';
import { BigNumber, BigNumberish, ContractReceipt, ContractTransaction, ethers } from 'ethers';
import { Web3Provider } from '@ethersproject/providers';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { environment } from '../../../../../environments/environment';
import { ChainNameToSymbolMap } from '../../../web3/types/chain-name-to-symbol-map.type';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import { CollectionIpfsMetadata } from '../../../create/dto/collection-metadata.dto';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import * as moment from 'moment';
import CollectionsService from '../../../chain-data/services/collections.service';
import { Contract } from '@ethersproject/contracts';
import { ActivatedRoute, Params, Router } from '@angular/router';
import NotificationsService from '../../../shared/services/notifications.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import FirebaseStorageService from '../../../firebase/services/firebase-storage.service';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import NftsService from '../../../nfts/services/nfts.service';
import { INFTDetails } from '../../../web3/interfaces/nft-details.interface';
import { CollectionRefundsListType } from '../../types/collection-refunds-list.type';
import { CollectionEarningListType } from '../../types/collection-earning-list.type';
import FirebaseAnalyticsService from '../../../firebase/services/firebase-analytics.service';
import { EventParams } from '@firebase/analytics';
import { IProfile } from '../../../user/interfaces/profile.interface';
import ProfileService from '../../../user/services/profile.service';
import { ContentTypeEnum } from '../../enums/content-type.enum';
import { IEstimatedFees } from '../../../omnichain/interfaces/estimated-fees.interface';
import OmnichainService from '../../../omnichain/services/omnichain.service';
import { OmnichainActionTypeEnum } from '../../../omnichain/enums/omnichain-action-type.enum';
import { SupportedAssetEnum } from '../../../web3/enum/supported-asset.enum';

@Component({
	selector: 'app-collection-details',
	templateUrl: './collection-details.component.html',
	styleUrls: ['./collection-details.component.scss']
})
export class CollectionDetailsComponent implements OnInit, OnDestroy {
	public get isNetworkSupportedIfConnected(): boolean {
		return !this.connectedChainSymbol || environment.supportedChains.includes(this.connectedChainSymbol);
	}

	public get isOngoing(): boolean {
		const isRemaining: boolean = !this.totalSupply || this.totalSupply > this.mintedCount;

		if (!isRemaining) {
			return false;
		}
		const now: number = moment().unix();

		if (!this.dateFromAsUnix) {
			return true;
		}

		if (this.dateToAsUnix > 0) {
			return now >= this.dateFromAsUnix && now <= this.dateToAsUnix;
		}

		return now >= this.dateFromAsUnix;
	}

	public get isBeforeStart(): boolean {
		return this.dateFromAsUnix > 0 && moment().unix() < this.dateFromAsUnix;
	}

	public get isFinished(): boolean {
		return this.dateToAsUnix > 0 && moment().unix() > this.dateToAsUnix;
	}

	public get isCreator(): boolean {
		return this.userAddress?.length > 0 && this.details && this.userAddress === this.details.creator;
	}

	public get actionButtonLabel(): string {
		if (!this.isConnectedToWallet) {
			return 'Connect';
		}

		if (!this.mintedCountLoaded || !this.isLoaded) {
			return 'Loading...';
		}

		if (!environment.production && !this.isAssetBalanceError) {
			if (this.assetBalance === undefined) {
				return 'Checking balance...';
			}

			if (!this.isBalanceEnoughToMint) {
				return 'Fund account';
			}
		}

		if (!this.isNetworkSupportedIfConnected) {
			return 'Unsupported Network';
		}

		if (this.isMinting) {
			return 'Minting...';
		}

		if (this.isApprovingAsset) {
			return 'Approving...';
		}

		if (this.isMintingError) {
			return 'Try again';
		}

		return 'Mint';
	}

	public get isActionButtonDisabled(): boolean {
		return this.isConnectedToWallet && (!this.canMint() || this.isMinting || this.isApprovingAsset || !this.isNetworkSupportedIfConnected);
	}

	public get isBalanceEnoughToMint(): boolean {
		if (!this.mintPriceInUnits) {
			return true;
		}

		if (!this.assetBalance) {
			return false;
		}

		return this.assetBalance.gte(this.mintPriceInUnits);
	}

	public isLoaded: boolean;
	public isLoadingError: boolean;
	public isMinting: boolean;
	public isMintingSuccess: boolean;
	public isMintingError: boolean;
	public isMintedOnDifferentChain: boolean;
	public isApprovingAsset: boolean;
	public isApproveAssetError: boolean;
	public subscriptions: Subscription;
	public isConnectedToWallet: boolean;
	public imageSrc: string | ArrayBuffer;
	public hdImageSrc: string | ArrayBuffer;
	public hdImageLoaded: boolean;
	public readonly chainSymbols: ChainSymbolEnum[];
	public readonly chainNameToSymbolMap: ChainNameToSymbolMap;
	public details: ICollectionDetails;
	public collectionAddress: string;
	public metadataURI: string;
	public collectionChainName: string;
	public srcChainName: string;
	public collectionChainSymbol: ChainSymbolEnum;
	public mintPriceInUnits: BigNumberish;
	public mintPriceFormatted: string;
	public totalSupply: number;
	public mintedCount: number;
	public mintedCountLoaded: boolean;
	public mintedOnCollectionChainCount: number;
	public remainingCount: number;
	public metadata: CollectionIpfsMetadata;
	public dateTo: string;
	public dateFrom: string;
	public hasEnded: boolean;
	public dateFromAsUnix: number;
	public dateToAsUnix: number;
	public assetBalance: BigNumber;
	public nfts: INFTDetails[];
	public selectedTab: 'details' | 'nfts' | 'description' | 'earnings' | 'refunds';
	public refundsList: Partial<CollectionRefundsListType>;
	public isRefundable: boolean;
	public refundsClaimedToChainMap: Partial<Record<ChainSymbolEnum, boolean>>;
	public earningsList: Partial<CollectionEarningListType>;
	public isEarning: boolean;
	public earningsClaimedToChainMap: Partial<Record<ChainSymbolEnum, boolean>>;
	public otherChains: Partial<Array<ChainSymbolEnum>>;
	public isRiskingSupplyExceeded: boolean;
	public profile?: IProfile;
	public contentType: ContentTypeEnum;
	private id: number;
	private isAssetBalanceError: boolean;
	private creatorAddress: string;
	private tokenFactoryContractAddress: string;
	private readonly mintParamsStruct: string = '(string dstChainName, address coll, uint256 mintPrice, string memory assetName, address creator, uint256 gas, uint256 redirectFee)';
	private readonly tokenFactoryContractAbi: string[] = [
		// tslint:disable-next-line:max-line-length
		`function mintToken(${this.mintParamsStruct} params) public payable`,
		'function estimateFees(string memory chSym, uint256 price, uint256 gas, uint256 nativeGas) external view returns (uint)',
	];
	private readonly erc20Abi: string[] = [
		'function allowance(address owner, address spender) public view returns (uint256)',
		'function approve(address spender, uint256 amount) public returns (bool)',
	];
	private userAddress: string;
	private connectedChainSymbol: ChainSymbolEnum;
	private walletCheckOnMint: NodeJS.Timer;
	private mintTx: ContractTransaction;

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly ipfsService: IpfsService,
		private readonly web3Service: Web3Service,
		private readonly profileService: ProfileService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly collectionsService: CollectionsService,
		private readonly collectionDataService: CollectionsDataService,
		private readonly nftsService: NftsService,
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		private readonly notificationsService: NotificationsService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly analyticsService: FirebaseAnalyticsService,
		private readonly omnichainService: OmnichainService,
	) {
		this.chainSymbols = [
			ChainSymbolEnum.eth,
			ChainSymbolEnum.bsc,
			ChainSymbolEnum.avax,
			ChainSymbolEnum.polygon,
			ChainSymbolEnum.ftm,
			ChainSymbolEnum.arb,
			ChainSymbolEnum.optimism,
		];
		this.chainNameToSymbolMap = {
			[SupportedChainNameEnum.Ethereum]: ChainSymbolEnum.eth,
			[SupportedChainNameEnum.BSC]: ChainSymbolEnum.bsc,
			[SupportedChainNameEnum.Avalanche]: ChainSymbolEnum.avax,
			[SupportedChainNameEnum.Fantom]: ChainSymbolEnum.ftm,
			[SupportedChainNameEnum.Polygon]: ChainSymbolEnum.polygon,
			[SupportedChainNameEnum.Arbitrum]: ChainSymbolEnum.arb,
			[SupportedChainNameEnum.Optimism]: ChainSymbolEnum.optimism,
			[SupportedChainNameEnum.Moonbeam]: ChainSymbolEnum.moonbeam,
		};
	}

	public async ngOnInit(): Promise<void> {
		this.uiLoaderService.stopAll();
		this.subscriptions = new Subscription();
		this.selectedTab = 'details';
		this.nfts = [];

		this.subscriptions.add(this.route.queryParams.pipe(
			switchMap((params: Params) => {
				if (!params.chainSymbol || !params.id) {
					return of(false);
				}

				if (!!this.collectionAddress && params.collectionAddress !== this.collectionAddress) {
					window.location.reload();
				}

				this.id = params.id;
				this.collectionAddress = params.collectionAddress;
				this.collectionChainSymbol = params.chainSymbol;
				this.creatorAddress = params.creatorAddress;
				this.metadataURI = params.metadataURI;
				this.collectionChainName = this.mapChainSymbolToName(this.collectionChainSymbol);
				this.otherChains = environment.supportedChains.filter((chain) => chain !== this.collectionChainSymbol);
				this.earningsClaimedToChainMap = {};
				this.refundsClaimedToChainMap = {};

				return of(true);
			})
		).subscribe(async (isValidURL: boolean) => {
			if (!isValidURL) {
				this.notificationsService.error('Error', 'Invalid URL params');

				return;
			}

			this.getCollectionDetails();
		}));

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();

					if (this.isConnectedToWallet && this.details) {
						clearInterval(walletInterval);
						this.userAddress = await this.web3Service.getUserAddress();
						this.connectedChainSymbol = this.web3Service.getConnectedChainSymbol();
						this.getRefundsList();
						this.getEarningList();

						if (!this.connectedChainSymbol) {
							setTimeout(async () => {
								await this.setConnectedChainData();
							}, 3000);
							return;
						}
						await this.setConnectedChainData();
					}
				}, 250);
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public connectWallet(): void {
		this.web3Service.connectWithModal();
	}

	public async onMint(): Promise<void> {
		if (!this.isConnectedToWallet) {
			this.connectWallet();

			return;
		}

		if (!environment.production && !this.isAssetBalanceError && !this.isBalanceEnoughToMint) {
			this.goToFaucet();

			return;
		}

		if (!this.canMint()) {
			return;
		}

		this.mint();
	}

	public async refreshMetadataAndUpdateInfo(): Promise<void> {
		try {
			if (this.details.metadataURI) {
				const readMetadata: AsyncIterable<Uint8Array> = this.ipfsService.read(this.details.metadataURI);

				try {
					for await (const chunk of readMetadata) {
						const metadataString: string = new TextDecoder().decode(chunk); // Not supported in IE

						if (!metadataString?.length) {
							return;
						}
						this.metadata = JSON.parse(metadataString);

						if (!this.metadata) {
							return;
						}

						if (this.metadata.media?.length === 46) {
							this.hdImageSrc = `https://omnisea.infura-ipfs.io/ipfs/${this.metadata.media}`;
						}
					}
				} catch (e) {
					console.error(e);
				}
			}

			this.getMintedCount();
		} catch (e) {
			this.notificationsService.error('Error', 'Missing data about this collection');
			this.isLoadingError = true;
		}
	}

	private async mint(): Promise<void> {
		this.isMintingError = false;
		this.isApproveAssetError = false;

		if (!this.collectionAddress) {
			this.notificationsService.error('Error', 'Collection\'s address unknown');
			return;
		}
		let estimatedFees: IEstimatedFees;

		try {
			let web3Provider: Web3Provider;
			try {
				if (!this.web3Service.isConnected()) {
					await this.web3Service.connectWithModal();
				}

				if (this.walletCheckOnMint) {
					clearInterval(this.walletCheckOnMint);
				}

				web3Provider = this.web3Service.provider$.getValue();
			} catch (e) {
				console.error(e);
				this.walletCheckOnMint = setInterval(() => {
					this.mint();
				}, 15000);

				return;
			}
			const signer: JsonRpcSigner = web3Provider.getSigner();

			if (!this.tokenFactoryContractAddress) {
				this.tokenFactoryContractAddress = environment.tokenFactoryAddressesMap[this.connectedChainSymbol];
			}
			const contract = new ethers.Contract(this.tokenFactoryContractAddress, this.tokenFactoryContractAbi, signer);

			if (this.mintPriceInUnits > 0) {
				const assetAddress: string = environment.chainSymbolToAsset[this.details.assetName][this.connectedChainSymbol];
				const assetContract: Contract = new ethers.Contract(assetAddress, this.erc20Abi, signer);
				const allowance: BigNumber = await this.getAllowance(assetContract, this.tokenFactoryContractAddress);

				if (allowance.lt(this.mintPriceInUnits)) {
					try {
						this.isApprovingAsset = true;
						const approvalTx: ContractTransaction = await this.approveSpent(
							assetContract,
							this.tokenFactoryContractAddress,
						);

						await approvalTx.wait();
						this.isApprovingAsset = false;
					} catch (e) {
						this.isApprovingAsset = false;
						this.isApproveAssetError = true;
						throw e;
					}
				}
			}

			if (!environment.chainSymbolToAsset[this.details.assetName][this.collectionChainSymbol]) {
				this.notificationsService.error('Error', 'Invalid payment asset');
				throw new Error('Wrong asset');
			}

			try {
				this.isMinting = true;
				this.isMintingSuccess = false;

				if (this.collectionChainSymbol === this.connectedChainSymbol) {
					this.mintTx = await contract.mintToken(
						[
							this.collectionChainSymbol,
							this.collectionAddress,
							this.mintPriceInUnits,
							this.details.assetName,
							this.details.creator,
							environment.chainSymbolToMintTokenGas[this.collectionChainSymbol],
							0,
						],
					);
				} else {
					const encodedFnData: string = this.web3Service.encodePayload(
						[
							this.collectionChainSymbol,
							this.collectionAddress,
							this.mintPriceInUnits,
							this.details.assetName,
							this.details.creator,
							environment.chainSymbolToMintTokenGas[this.collectionChainSymbol],
							0,
						],
						['string', 'address', 'uint', 'string', 'address', 'uint', 'uint'],
					);
					const encodedPayload: string = this.web3Service.encodePayload(
						[
							this.collectionChainSymbol,
							this.userAddress || '0xMock',
							encodedFnData,
							environment.chainSymbolToMintTokenGas[this.collectionChainSymbol],
							this.userAddress || '0xMock',
							this.collectionChainSymbol,
							this.userAddress || '0xMock',
							0,
						],
						['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
					);

					estimatedFees = await this.omnichainService.getEstimatedFees(
						this.collectionChainSymbol,
						encodedPayload,
						OmnichainActionTypeEnum.MINT_TOKEN,
					);
					this.mintTx = await contract.mintToken(
						[
							this.collectionChainSymbol,
							this.collectionAddress,
							this.mintPriceInUnits,
							this.details.assetName,
							this.details.creator,
							environment.chainSymbolToMintTokenGas[this.collectionChainSymbol],
							estimatedFees.redirectFee,
						],
						{value: estimatedFees.fee},
					);
				}
				const userAddress: string = this.userAddress || await signer.getAddress();
				this.collectionsService.saveMintTx(
					this.details,
					userAddress,
					this.mintTx,
				);

				try {
					this.collectionsService.incrementCollectionMintsCount(this.details, userAddress, Number(this.mintPriceFormatted));
				} catch (e) {
					console.error(e);
				}

				this.mintTx.wait().then(async (txResult: ContractReceipt) => {
					this.collectionsService.saveMintTx(
						this.details,
						this.userAddress || await signer.getAddress(),
						this.mintTx,
						txResult?.status,
					);
					this.isMintingSuccess = true;
					this.refreshDetailsAndMetadata();
					this.isMintedOnDifferentChain = this.connectedChainSymbol !== this.collectionChainSymbol;
					this.notificationsService.success(
						'Success',
						this.isMintedOnDifferentChain ? 'Your NFT will be minted in a moment. On the Testnet the process may be slower.' : 'Minted NFT is already on your blockchain address',
					);
				}).catch((e) => {
					this.notificationsService.error('Error', 'Minting process failed due to the transaction error');
					this.isMintingError = true;
					console.error(e);
					throw e;
				}).finally(() => {
					this.isMinting = false;
				});
			} catch (e) {
				console.error(e);
				this.isMintingError = true;
				throw e;
			}
		} catch (e) {
			this.isMinting = false;
			this.isApprovingAsset = false;
			const errorMessage: string = e?.message || e;
			const errorDataMessage: string = e?.data?.message;

			if (errorDataMessage) {
				if (errorDataMessage.includes('insufficient funds for gas') || errorDataMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[this.connectedChainSymbol];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}
			}

			if (errorMessage) {
				if (errorMessage.includes('MetaMask Tx Signature: User denied transaction signature')) {
					// Canceled on User's action
					return;
				}

				if (errorMessage.includes('insufficient funds for gas') || errorMessage.includes('insufficient balance for transfer')) {
					console.error(e);
					const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
					const nativeAsset: string = environment.chainSymbolToNativeAsset[this.connectedChainSymbol];
					const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

					this.notificationsService.error('Insufficient funds for transaction', tipMessage);
					return;
				}

				if (errorMessage.includes('insufficient balance for transfer')) {
					if (!environment.production) {
						this.notificationsService.error('Insufficient funds', 'You\'ll be redirected to the faucet to get some test tokens.');
						setTimeout(() => {
							this.goToFaucet();
						}, 5000);
						return;
					}
					this.notificationsService.error('Insufficient funds', 'Looks like you currently don\'t have required amount to mint');
					return;
				}

				if (errorMessage.includes('invalid contract address or ENS name')) {
					console.error(e);
					this.notificationsService.error('Error', 'Network error. Refresh and try again in a moment.');
					return;
				}

				if (errorMessage.includes('underlying network changed')) {
					this.notificationsService.error('Error', 'Refreshing due to the connected network change during the process');
					setTimeout(() => {
						window.location.reload();
					}, 5000);
					return;
				}
			}
			console.error(e);
			this.notificationsService.error('Error', 'Unknown error, refresh and try again');
		}
	}

	public goToCollectionOnExplorer(): void {
		this.web3Service.goToAddressOnExplorer(
			this.collectionAddress,
			this.collectionChainSymbol,
			this.collectionChainSymbol === ChainSymbolEnum.arb ? 'token' : 'address',
		);
	}

	public goToIpfsFileView(): void {
		if (!this?.details?.metadata?.media) {
			return;
		}

		window.open(`https://omnisea.infura-ipfs.io/ipfs/${this.details.metadata.media.substring(0, 46)}`, '_blank');
	}

	public goToIpfsMetadataView(): void {
		if (!this?.details?.metadataURI) {
			return;
		}

		window.open(`https://omnisea.infura-ipfs.io/ipfs/${this.details.metadataURI}`, '_blank');
	}

	public shareOnTwitter(): void {
		window.open(`https://twitter.com/intent/tweet?text=${this.details.collectionName} on Omnisea:&url=${encodeURIComponent(window.location.href)}`, '_blank');
	}

	public getCreatorAlias(): string {
		const creatorAddress: string = this.details.creator;

		if (this.profile?.name) {
			return this.profile.name;
		}

		return `${creatorAddress.substring(0, 5)}...${creatorAddress.slice(-3)}`;
	}

	public async goToCreator(): Promise<void> {
		this.router.navigate(['/collections/user'], {queryParams: {userAddress: this.details.creator}});
	}

	public mapChainSymbolToName(chainSymbol: ChainSymbolEnum): SupportedChainNameEnum {
		switch (chainSymbol) {
			case ChainSymbolEnum.ftm:
				return SupportedChainNameEnum.Fantom;
			case ChainSymbolEnum.avax:
				return SupportedChainNameEnum.Avalanche;
			case ChainSymbolEnum.arb:
				return SupportedChainNameEnum.Arbitrum;
			case ChainSymbolEnum.bsc:
				return SupportedChainNameEnum.BSC;
			case ChainSymbolEnum.optimism:
				return SupportedChainNameEnum.Optimism;
			case ChainSymbolEnum.polygon:
				return SupportedChainNameEnum.Polygon;
			case ChainSymbolEnum.moonbeam:
				return SupportedChainNameEnum.Moonbeam;
			case ChainSymbolEnum.eth:
			default:
				return SupportedChainNameEnum.Ethereum;
		}
	}

	public getMintedFromChainCount(earnedFromChain: number): number {
		const mintPrice: number = Number(this.mintPriceFormatted);

		return earnedFromChain ? Math.ceil(earnedFromChain / mintPrice) : 0;
	}

	public getMintedFromCollectionChain(): number {
		if (this.mintedCount === 0) {
			return 0;
		}
		let omniMintedCount: number = 0;

		for (const chain of this.otherChains) {
			omniMintedCount += this.getMintedFromChainCount(this.earningsList[chain] || 0);
		}

		return this.mintedCount - omniMintedCount;
	}

	public isConnectedToCollectionChain(): boolean {
		return this.collectionChainSymbol === this.connectedChainSymbol;
	}

	public canClaimRefunds(dstChainSymbol: ChainSymbolEnum): boolean {
		return this.isConnectedToCollectionChain() && this.refundsList[dstChainSymbol] > 0 && !this.refundsClaimedToChainMap[dstChainSymbol];
	}

	public canClaimEarned(dstChainSymbol: ChainSymbolEnum): boolean {
		return this.isConnectedToCollectionChain() && this.earningsList[dstChainSymbol] > 0 && !this.earningsClaimedToChainMap[dstChainSymbol];
	}

	public onClaimEarned(dstChainSymbol: ChainSymbolEnum) {
		if (!this.canClaimEarned(dstChainSymbol)) {
			return;
		}

		this.collectionsService.claimEarned(this.details, dstChainSymbol).then((result: boolean) => {
			if (result) {
				this.earningsClaimedToChainMap[dstChainSymbol] = true;
				this.notificationsService.success('Claimed', 'Omnichain Claim takes a few minutes to transfer your earnings.');

				return;
			}

			this.notificationsService.error('Error', 'Unable to claim your earnings. Network may be congested. Please, try again in a few minutes.');
		});
	}

	public onRefund(dstChainSymbol: ChainSymbolEnum) {
		if (!this.canClaimRefunds(dstChainSymbol)) {
			return;
		}

		this.collectionsService.refund(this.details, dstChainSymbol).then((result: boolean) => {
			if (result) {
				this.refundsClaimedToChainMap[dstChainSymbol] = true;
				this.notificationsService.success('Claimed', 'Omnichain Refund takes a few minutes to transfer your funds.');

				return;
			}

			this.notificationsService.error('Error', 'Unable to refund. Network may be congested. Please, try again in a few minutes.');
		});
	}

	public getEarnedAmountByMintCount(mintedOnCollectionChainCount: number) {
		if (!mintedOnCollectionChainCount) {
			return 0;
		}
		const mintPrice: number = Number(this.mintPriceFormatted);

		return mintedOnCollectionChainCount * mintPrice;
	}

	private async getAllowance(contract: Contract, spenderAddress: string): Promise<BigNumber> {
		return await contract.allowance(contract.signer.getAddress(), spenderAddress);
	}

	private async approveSpent(contract, spenderAddress: string): Promise<ContractTransaction> {
		return await contract.approve(spenderAddress, BigNumber.from(2).pow(256).sub(1));
	}

	private async getCollectionDetails(): Promise<void> {
		this.getMintedCount();

		try {
			this.collectionsService.getCollectionMintsCountFromDatabase(this.id, async (mintCount) => {
				if (this.totalSupply > 0 && mintCount >= this.totalSupply) {
					this.isRiskingSupplyExceeded = true;
				}
			});
		} catch (e) {
			console.error(e);
		}

		this.collectionDataService.getCollectionDetails(this.id, async (details: ICollectionDetails) => {
			this.details = details;
			this.contentType = this.details.contentType || ContentTypeEnum.ART;

			try {
				this.profileService.getProfile(this.creatorAddress).then(async (profile: IProfile) => {
					this.profile = profile;
				});
			} catch (e) {
				console.error(e);
			}

			if (this.details.srcChain) {
				this.srcChainName = this.mapChainSymbolToName(this.details.srcChain);
			}
			this.nftsService.getByCollectionId(
				this.details.id,
				async (nfts: Record<string, INFTDetails>) => {
					if (!nfts) {
						return;
					}

					this.nfts = Object.values(nfts);
					for (const nft of this.nfts) {
						this.firebaseStorageService.getIpfsFileURL(nft.smFileIpfsHash).then((url) => {
							nft.fileURL = url;
						});
					}

					if (this.nfts.length) {
						this.selectedTab = 'nfts';
					}
				},
				async () => {
					console.error(`Couldn\'t load NFTs of collection: ${this.collectionAddress}`);
				}
			);

			this.refreshDetailsAndMetadata();
			this.isLoaded = true;

			try {
				setTimeout(() => {
					if (!this.details.id || !this.details.collectionName) {
						return;
					}
					const analyticsEventParams: Pick<EventParams, 'items' | 'currency' | 'value'> = {
						currency: 'USD',
						value: (this.mintPriceInUnits > 0 && this.mintPriceFormatted) ? Number(this.mintPriceFormatted) : 0,
						items: [
							{
								item_id: this.id.toString(),
								item_name: this.details.collectionName,
							}
						]
					};

					this.analyticsService.logEvent('view_item', analyticsEventParams);
				});
			} catch (e) {
				console.error(e);
			}
		}, async () => {
			console.error(`Loading ${this.collectionAddress} collection from fallback`);
			this.details = await this.collectionsService.getCollectionDetails(this.collectionAddress, this.collectionChainSymbol);

			if (this.details.srcChain) {
				this.srcChainName = this.mapChainSymbolToName(this.details.srcChain);
			}
			await this.refreshDetailsAndMetadata();
		});
	}

	private async refreshDetails(): Promise<void> {
		if (!this.imageSrc) {
			this.imageSrc = await this.firebaseStorageService.getIpfsFileURL(this.details.fileURI);
		}
		this.mintPriceInUnits = this.details.mintPrice;
		this.mintPriceFormatted = this.mintPriceInUnits
			? ethers.utils.formatUnits(this.mintPriceInUnits, environment.assetToDigits[this.details.assetName])
			: '0';
		this.totalSupply = this.details.totalSupply;
		this.mintedCount = this.details.tokenIds;
		this.getRefundsList();
		this.getEarningList();

		if (!this.details.dropFrom) {
			return;
		}
		this.dateFromAsUnix = this.details.dropFrom;
		this.dateToAsUnix = this.details.dropTo;
		const secondsToStart: number = moment(this.details.dropFrom).diff(moment().unix());

		this.dateFrom = moment.duration(secondsToStart, 'seconds').humanize(true);
		if (this.details.dropTo) {
			// if (this.isOngoing && !this.isFinished) {
			const secondsToEnd: number = moment(this.details.dropTo).diff(moment().unix());

			this.dateTo = moment.duration(secondsToEnd, 'seconds').humanize(true);

			return;
			// }

			this.dateTo = moment.unix(this.details.dropTo).format('MMM Do');
		}
	}

	private async refreshDetailsAndMetadata(): Promise<void> {
		try {
			await this.refreshDetails();
			await this.refreshMetadataAndUpdateInfo();
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Unable to refresh the collection\'s data');
		}
	}

	private goToFaucet(): void {
		const queryParams: Params = {
			collectionAddress: this.collectionAddress,
			collectionChainSymbol: this.collectionChainSymbol,
		};
		this.router.navigate(['/testnet'], {queryParams});
	}

	private async setConnectedChainData(): Promise<void> {
		this.tokenFactoryContractAddress = environment.tokenFactoryAddressesMap[this.connectedChainSymbol];
		try {
			this.assetBalance = await this.web3Service.getBalance(environment.chainSymbolToAsset[this.details.assetName][this.connectedChainSymbol]);
		} catch (e) {
			console.error(e);
			this.isAssetBalanceError = true;
		}
	}

	private getMintedCount(): void {
		this.collectionsService.getCollectionMintedCount(this.collectionAddress, this.collectionChainSymbol).then((mintedCount: number) => {
			this.mintedCount = mintedCount;
			this.mintedCountLoaded = true;

			if (!this.totalSupply) {
				return;
			}
			this.remainingCount = this.totalSupply - this.mintedCount;
		});
	}

	private getEarningList(): void {
		if (this.earningsList || !this.mintPriceInUnits || BigNumber.from(this.mintPriceInUnits).eq(0)) {
			return;
		}

		this.collectionsService.getEarningsList(this.details, Number(this.mintPriceFormatted)).then((earningsList) => {
			this.earningsList = earningsList;
			this.mintedOnCollectionChainCount = this.getMintedFromCollectionChain();
			this.isEarning = this.getIsEarning();
		});
	}

	private getRefundsList(): void {
		if (this.refundsList || !this.userAddress || !this.mintPriceInUnits || BigNumber.from(this.mintPriceInUnits).eq(0)) {
			return;
		}

		this.collectionsService.getRefundsList(this.details, this.userAddress).then((refundsList) => {
			this.refundsList = refundsList;
			this.isRefundable = this.getIsRefundable();
		});
	}

	private getIsRefundable(): boolean {
		if (!this.refundsList) {
			return false;
		}

		for (const chainSymbol in this.refundsList) {
			if (!this.refundsList[chainSymbol]) {
				continue;
			}

			if (this.refundsList[chainSymbol] > 0) {
				return true;
			}
		}

		return false;
	}

	private getIsEarning(): boolean {
		if (!this.earningsList) {
			return false;
		}

		for (const chainSymbol in this.earningsList) {
			if (!this.earningsList[chainSymbol]) {
				continue;
			}

			if (this.earningsList[chainSymbol] > 0) {
				return true;
			}
		}

		return false;
	}

	private canMint(): boolean {
		return this.isLoaded && this.isOngoing && !this.isMintingSuccess && this.details.isPublic && this.mintedCountLoaded;
	}
}
