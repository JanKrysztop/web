import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgProgress, NgProgressRef } from 'ngx-progressbar';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import { inOutAnimation } from '../../../shared/data/animations/in-out-animation';
import CollectionsService from '../../../chain-data/services/collections.service';
import { ICollectionStatistics } from '../../interfaces/collection-statistics.interface';

@Component({
	selector: 'app-collections-ranking',
	templateUrl: './collections-ranking.component.html',
	styleUrls: ['./collections-ranking.component.scss'],
	animations: inOutAnimation,
})
export class CollectionsRankingComponent implements OnInit, OnDestroy {
	public get fromLabel(): string {
		if (this.days === 0.0416) {
			return 'Last Hour';
		}

		if (this.days === 1) {
			return 'Last Day';
		}

		if (this.days === 7) {
			return 'Last Week';
		}

		if (this.days === 30) {
			return 'Last Month';
		}
	}

	public progressRef: NgProgressRef;
	public isLoaded: boolean;
	public isLoadingError: boolean;
	public isEmpty: boolean;
	public collectionsStatistics: ICollectionStatistics[];
	public days: number;
	public readonly placeholderItems: number[] = [1, 2, 3, 4, 5, 6, 7, 8];
	public readonly fromFilterOptionsInDays: number[] = [0.0416, 1, 7, 30];
	private subscriptions: Subscription;

	constructor(
		private readonly collectionsService: CollectionsService,
		private readonly collectionsDataService: CollectionsDataService,
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly progressBar: NgProgress,
	) {
	}

	public async ngOnInit(): Promise<void> {
		this.collectionsStatistics = this.collectionsDataService.trendingCollectionsStats$?.getValue() || [];
		this.progressRef = this.progressBar.ref('rankingProgressBar');
		this.uiLoaderService.stopAll();
		this.subscriptions = new Subscription();
		this.days = 7;

		this.subscriptions.add(
			this.collectionsDataService.trendingCollectionsStats$.subscribe(async (collectionsStatistics: ICollectionStatistics[]) => {
				this.collectionsStatistics = [...collectionsStatistics];

				for (const collectionStatistic of this.collectionsStatistics) {
					try {
						const collection: ICollectionDetails = await this.collectionsService.getCollectionById(collectionStatistic.id);

						collectionStatistic.totalSupply = collection.totalSupply;
						collectionStatistic.chainSymbol = collection.chainSymbol;
						collectionStatistic.chainName = this.mapChainSymbolToName(collectionStatistic.chainSymbol);
						collectionStatistic.address = collection.collectionAddress;
						collectionStatistic.collectionId = collection.id;
						collectionStatistic.isOverMinted = collectionStatistic.totalSupply > 0
							&& collectionStatistic.minted > collectionStatistic.totalSupply;
						collectionStatistic.assetName = collection.assetName;
					} catch (e) {
						console.error(e);
					}
				}

				this.isLoaded = true;
				this.progressRef.complete();

				if (this.collectionsStatistics.length === 0) {
					setTimeout(() => {
						this.isEmpty = this.collectionsStatistics.length === 0;
					}, 2500);
				}
			}),
		);

		this.collectionsDataService.getTrendingCollections(this.days * 24 * 60);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public mapChainSymbolToName(chainSymbol: ChainSymbolEnum): SupportedChainNameEnum {
		switch (chainSymbol) {
			case ChainSymbolEnum.ftm:
				return SupportedChainNameEnum.Fantom;
			case ChainSymbolEnum.avax:
				return SupportedChainNameEnum.Avalanche;
			case ChainSymbolEnum.arb:
				return SupportedChainNameEnum.Arbitrum;
			case ChainSymbolEnum.bsc:
				return SupportedChainNameEnum.BSC;
			case ChainSymbolEnum.optimism:
				return SupportedChainNameEnum.Optimism;
			case ChainSymbolEnum.polygon:
				return SupportedChainNameEnum.Polygon;
			case ChainSymbolEnum.moonbeam:
				return SupportedChainNameEnum.Moonbeam;
			case ChainSymbolEnum.eth:
			default:
				return SupportedChainNameEnum.Ethereum;
		}
	}

	public goToCollection(collectionStatistic: ICollectionStatistics): void {
		const queryParams: Params = {
			id: collectionStatistic.collectionId,
			chainSymbol: collectionStatistic.chainSymbol,
			collectionAddress: collectionStatistic.address,
		};

		this.router.navigate(
			['/collections/details'],
			{queryParams},
		);
	}

	public changeDaysFilter(days: number): void {
		if (days === this.days) {
			return;
		}
		this.isLoaded = false;
		this.days = days;
		this.collectionsDataService.getTrendingCollections(this.days * 24 * 60);
	}

	getFromLabel(inDays: number): string {
		if (inDays === 0.0416) {
			return 'Last Hour';
		}

		if (inDays === 1) {
			return 'Last Day';
		}

		if (inDays === 7) {
			return 'Last Week';
		}

		if (inDays === 30) {
			return 'Last Month';
		}
	}
}
