import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { debounceTime, distinctUntilChanged, of, Subscription, switchMap } from 'rxjs';
import IpfsService from '../../../ipfs/services/ipfs.service';
import Web3Service from '../../../web3/services/web3.service';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import CollectionsService from '../../../chain-data/services/collections.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import NotificationsService from '../../../shared/services/notifications.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ethers } from 'ethers';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import { CollectionStatusEnum } from '../../../web3/enum/collection-status.enum';
import { INFTDetails } from '../../../web3/interfaces/nft-details.interface';
import NftsService from '../../../nfts/services/nfts.service';
import * as moment from 'moment';
import UserService from '../../../user/services/user.service';
import { IMintTx } from '../../interfaces/mint-tx.interface';
import { CollectionBasicChainDataType } from '../../../chain-data/types/collection-basic-chain-data.type';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICompressImageOptions } from '../../../shared/interfaces/compress-image-options.interface';
import imageCompression from 'browser-image-compression';
import FirebaseStorageService from '../../../firebase/services/firebase-storage.service';
import { IProfile } from '../../../user/interfaces/profile.interface';
import FirestoreService from '../../../firebase/services/firestore.service';
import ProfileService from '../../../user/services/profile.service';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { environment } from '../../../../../environments/environment';
import { SupportedAssetEnum } from '../../../web3/enum/supported-asset.enum';
import { IFollow } from '../../../user/interfaces/follow.interface';

@Component({
	selector: 'app-user-collections',
	templateUrl: './user-collections.component.html',
	styleUrls: ['./user-collections.component.scss']
})
export class UserCollectionsComponent implements OnInit, OnDestroy {
	public get isOwner(): boolean {
		return this.isConnectedToWallet && this.connectedUserAddress?.length > 0 && this.connectedUserAddress === this.userAddress;
	}

	public get creatorAlias(): string {
		if (!this.userAddress) {
			return;
		}

		if (this.profile?.name) {
			return this.profile.name;
		}

		return `${this.userAddress.substring(0, 5)}...${this.userAddress.slice(-3)}`;
	}

	public get isProfileFormValid(): boolean {
		return this.profileForm.valid && !this.profileAvatarFileTooBig && !this.profileCoverFileTooBig && this.isProfileNameUnique;
	}

	@ViewChild('profileModalCloseBtn') public profileModalCloseBtn: ElementRef<HTMLButtonElement>;

	public areCreatedCollectionsLoaded: boolean;
	public areCreatedCollectionsFromChainLoaded: boolean;
	public areUserCollectionsLoaded: boolean;
	public areNFTsLoaded: boolean;
	public isWaitingForCreation: boolean;
	public subscriptions: Subscription;
	public createdCollections: ICollectionDetails[];
	public createdCollectionsOnChain: CollectionBasicChainDataType[];
	public userCollections: ICollectionDetails[];
	public mintedNFTs: INFTDetails[];
	public userAddress: string;
	public connectedUserAddress: string;
	public selectedTab: 'created' | 'creating' | 'public' | 'minted' | 'following' | 'followers';
	public readonly placeholderItems: number[] = [1, 2, 3, 4, 5, 6, 7, 8];
	public profileCoverPreviewSrc: string | ArrayBuffer;
	public profileCoverFileTooBig: boolean;
	public profileCoverFile: File;
	public profileAvatarPreviewSrc: string | ArrayBuffer;
	public profileAvatarFileTooBig: boolean;
	public profileAvatarFile: File;
	public profileForm: FormGroup;
	public profile?: IProfile;
	public isProfileLoaded: boolean;
	public isProfileError: boolean;
	public profileCover?: string;
	public profileAvatar?: string;
	public isAvatarLoaded: boolean;
	public isProfileLinksVisible: boolean;
	public isProfileNameUnique: boolean;
	public isSubmittingProfile: boolean;
	public isFollower: boolean;
	public followers: IProfile[];
	public isFollowersLoaded: boolean;
	public following: IProfile[];
	public isFollowingLoaded: boolean;
	private mintedFromCollections: Array<Pick<ICollectionDetails, 'collectionAddress' | 'chainSymbol' | 'id'>>;
	private mintsCount: number;
	private profileCoverFileStoragePath: string;
	private profileAvatarFileStoragePath: string;
	private isLoadingCollections: boolean;
	private isConnectedToWallet: boolean;

	constructor(
		private readonly ipfsService: IpfsService,
		private readonly web3Service: Web3Service,
		private readonly userService: UserService,
		private readonly profileService: ProfileService,
		private readonly nftsService: NftsService,
		private readonly collectionsService: CollectionsService,
		private readonly collectionsDataService: CollectionsDataService,
		private readonly route: ActivatedRoute,
		private readonly router: Router,
		private readonly notificationsService: NotificationsService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly formBuilder: FormBuilder,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly firestoreService: FirestoreService,
	) {
	}

	public async ngOnInit(): Promise<void> {
		this.selectedTab = 'creating';
		this.createdCollections = [];
		this.createdCollectionsOnChain = [];
		this.userCollections = [];
		this.createdCollections = [];
		this.mintedNFTs = [];
		this.mintedFromCollections = [];
		this.uiLoaderService.stopAll();
		this.subscriptions = new Subscription();

		this.subscriptions.add(this.route.queryParams.pipe(
			switchMap((params: Params) => {
				if (this.userAddress && params.userAddress && params.userAddress !== this.userAddress) {
					window.location.reload();
				}

				this.userAddress = params.userAddress;

				return of(!!this.userAddress);
			})
		).subscribe(async (isValidURL: boolean) => {
			if (!isValidURL) {
				this.notificationsService.error('Error', 'No user address in URL');
				return;
			}

			this.profileService.getProfile(this.userAddress).then(async (profile: IProfile) => {
				this.profile = profile;
				this.setProfileData();
				this.profileForm = this.formBuilder.group({
					name: [this.profile?.name || undefined, [Validators.required, Validators.maxLength(50)]],
					twitter: [this.profile?.twitter || undefined, [Validators.maxLength(150)]],
					discord: [this.profile?.discord || undefined, [Validators.maxLength(150)]],
					instagram: [this.profile?.instagram || undefined, [Validators.maxLength(150)]],
					ens: [this.profile?.ens || undefined, [Validators.maxLength(250)]],
					externalLink: [this.profile?.externalLink || undefined, [Validators.maxLength(250)]],
				});
				this.isProfileNameUnique = true;

				if (!this.isOwner) {
					this.isFollower = await this.isFollowing();
				}

				try {
					this.getFollowing();
				} catch (e) {
					console.error(e);
				}

				try {
					this.getFollowers();
				} catch (e) {
					console.error(e);
				}

				this.subscriptions.add(
					this.profileForm.get('name').valueChanges
						.pipe(
							debounceTime(500),
							distinctUntilChanged(),
						)
						.subscribe(() => {
							const profileName: string = this.profileForm.get('name').value;

							if (!profileName) {
								return;
							}

							if (profileName.toLowerCase() === this.profile.lowercaseName) {
								return;
							}

							this.profileService.isProfileNameUnique(profileName).then((isUnique) => {
								this.isProfileNameUnique = isUnique;
							});
						}),
				);
			}).catch(() => {
				console.error('Cant load profile');
				this.isProfileError = true;
			});
		}));

		setTimeout(() => {
			this.subscriptions.add(
				this.web3Service.provider$.subscribe(async () => {
					const walletInterval: NodeJS.Timer = setInterval(async () => {
						if (this.connectedUserAddress) {
							clearInterval(walletInterval);

							return;
						}

						if (!this.web3Service.isConnected()) {
							if (!this.isLoadingCollections) {
								this.getSelectedUserData();
							}

							return;
						}
						const connectedUserAddress: string = await this.web3Service.getUserAddress();
						this.connectedUserAddress = connectedUserAddress;

						if (!connectedUserAddress && !this.isLoadingCollections) {
							this.getSelectedUserData();

							return;
						}

						if (this.isLoadingCollections) {
							return;
						}

						if (this.isOwner) {
							this.getConnectedUserData();

							return;
						}
						this.getSelectedUserData();
					}, 250);
				}),
			);
		}, 250);

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();
				});
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public getFormattedPrice(mintPriceInUnit: number, asset: string): string {
		return ethers.utils.formatUnits(mintPriceInUnit, environment.assetToDigits[SupportedAssetEnum[asset]]);
	}

	public goToCollection(collection: ICollectionDetails): void {
		if (!collection.collectionAddress || !collection.id || !collection.isPublic) {
			return;
		}
		const queryParams: Params = {
			id: collection.id,
			chainSymbol: collection.chainSymbol,
			collectionAddress: collection.collectionAddress,
			metadataURI: collection.metadataURI,
			creatorAddress: collection.creator,
		};

		this.router.navigate(
			['/collections/details'],
			{queryParams},
		);
	}

	public goToNFTCollection(nft: INFTDetails): void {
		if (!nft.collectionAddress) {
			return;
		}

		const queryParams: Params = {
			id: nft.collectionId,
			chainSymbol: nft.chainSymbol,
			collectionAddress: nft.collectionAddress,
			metadataURI: nft.collectionMetadataURI,
		};

		this.router.navigate(
			['/collections/details'],
			{queryParams},
		);
	}

	public goToCreate(): void {
		this.router.navigate(['/create']);
	}

	public goToExplore(): void {
		this.router.navigate(['/collections']);
	}

	public onImageLoad(collection: ICollectionDetails): void {
		collection.isImageLoaded = true;
	}

	public async setCollectionPublic(collection: ICollectionDetails): Promise<void> {
		if (!this.isOwner || !collection.collectionAddress) {
			return;
		}

		try {
			await this.collectionsDataService.setCollectionPublic(collection);
			this.notificationsService.success('Success', 'Collection is now public');
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Couldn\'t make collection public');
		}
	}

	public async setCollectionPrivate(collection: ICollectionDetails): Promise<void> {
		if (!this.isOwner) {
			return;
		}

		try {
			await this.collectionsDataService.setCollectionPrivate(collection);
			this.notificationsService.success('Success', 'Collection is now private');
		} catch (e) {
			console.error(e);
			this.notificationsService.error('Error', 'Couldn\'t make collection private');
		}
	}

	public async copyCreatorAddress(): Promise<void> {
		await navigator.clipboard.writeText(this.userAddress);
		this.notificationsService.success('Copied');
	}

	public isCollectionHidden(collection: ICollectionDetails): boolean {
		const isNotCreated: boolean = [
			CollectionStatusEnum.ERROR,
			CollectionStatusEnum.INITIATED,
			CollectionStatusEnum.CREATING,
		].includes(collection.status);

		if (!this.isOwner) {
			return isNotCreated;
		}

		return isNotCreated && moment(collection.createdAt).add(2, 'days').isBefore(moment());
	}

	public isProfileCoverFileImage(): boolean {
		if (!this.profileCoverFile) {
			return false;
		}

		return !!this.profileCoverFile.type.match(/image\/*/);
	}

	public isProfileAvatarFileImage(): boolean {
		if (!this.profileAvatarFile) {
			return false;
		}

		return !!this.profileAvatarFile.type.match(/image\/*/);
	}

	public onProfileCoverFileChange(event: Event): void {
		this.profileCoverPreviewSrc = undefined;
		this.profileCoverFile = undefined;
		this.profileCoverFileTooBig = false;
		const input: HTMLInputElement = event.target as HTMLInputElement;

		if (!input?.files?.[0]) {
			return;
		}
		this.profileCoverFile = input.files[0];
		const reader = new FileReader();

		reader.onload = async (e) => {
			const mimeType: string = this.profileCoverFile.type;

			if (!this.isProfileCoverFileValid()) {
				this.profileCoverFile = undefined;
				this.notificationsService.error('Invalid File', 'Max cover file size is 10 MB');
				return;
			}

			if (!mimeType.match(/image\/*/)) {
				return;
			}

			this.profileCoverPreviewSrc = reader.result;
		};
		reader.readAsDataURL(this.profileCoverFile);
	}

	public onProfileAvatarFileChange(event: Event): void {
		this.profileAvatarPreviewSrc = undefined;
		this.profileAvatarFile = undefined;
		this.profileAvatarFileTooBig = false;
		const input: HTMLInputElement = event.target as HTMLInputElement;

		if (!input?.files?.[0]) {
			return;
		}
		this.profileAvatarFile = input.files[0];
		const reader = new FileReader();

		reader.onload = async (e) => {
			const mimeType: string = this.profileAvatarFile.type;

			if (!this.isProfileAvatarFileValid()) {
				this.profileAvatarFile = undefined;
				this.notificationsService.error('Invalid File', 'Max avatar file size is 3 MB');
				return;
			}

			if (!mimeType.match(/image\/*/)) {
				return;
			}

			this.profileAvatarPreviewSrc = reader.result;
		};
		reader.readAsDataURL(this.profileAvatarFile);
	}

	public async onProfileEditSubmit(): Promise<void> {
		if (this.isSubmittingProfile) {
			return;
		}

		const name: string = this.profileForm.get('name').value;

		if (!this.connectedUserAddress || !this.isOwner) {
			this.notificationsService.error('Error', 'You are not the owner of this profile');

			return;
		}

		if (!name) {
			this.notificationsService.error('Error', 'Name is required');

			return;
		}

		if (!this.isProfileNameUnique) {
			this.notificationsService.error('Error', 'Name is already taken');

			return;
		}
		const externalLink: string = this.profileForm.get('externalLink').value;

		if (externalLink?.length) {
			const reg: RegExp = new RegExp('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?');

			if (!reg.test(externalLink)) {
				this.notificationsService.error('Invalid Website Link', 'Website link is not a valid HTTPS URL');

				return;
			}
		}
		const discordInviteLink: string = this.profileForm.get('discord').value;

		if (discordInviteLink?.length) {
			const reg: RegExp = new RegExp('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?');

			if (!reg.test(discordInviteLink)) {
				this.notificationsService.error('Invalid Discord Link', 'Discord Invite Link is not a valid HTTPS URL');

				return;
			}
		}
		this.isSubmittingProfile = true;

		try {
			if (this.profileCoverFile) {
				await this.compressProfileCoverAndUpload();
			}

			if (this.profileAvatarFile) {
				await this.compressProfileAvatarAndUpload();
			}
			const profile: IProfile = {
				name,
				lowercaseName: name.toLowerCase(),
				twitter: this.profileForm.get('twitter').value || null,
				instagram: this.profileForm.get('instagram').value || null,
				discord: this.profileForm.get('discord').value || null,
				ens: this.profileForm.get('ens').value || null,
				externalLink: this.profileForm.get('externalLink').value || null,
				userAddress: this.connectedUserAddress,
			};

			if (this.profileAvatarFileStoragePath) {
				profile.avatar = this.profileAvatarFileStoragePath;
			}

			if (this.profileCoverFileStoragePath) {
				profile.backgroundImage = this.profileCoverFileStoragePath;
			}

			try {
				await this.firestoreService.set(
					'profiles',
					this.connectedUserAddress,
					profile,
				);
				this.notificationsService.success('Success', 'Profile has been updated');
				this.profile = profile;
				this.setProfileData();
				this.isSubmittingProfile = false;
				this.closeProfileModal();
			} catch (e) {
				this.notificationsService.error('Error', 'Couldn\'t update Profile');
				this.isSubmittingProfile = false;
				console.error(e);
			}
		} catch (e) {
			console.error(e);
			this.isSubmittingProfile = false;
		}
	}

	public toggleProfileLinks(): void {
		this.isProfileLinksVisible = !this.isProfileLinksVisible;
	}

	public closeProfileModal(): void {
		this.profileModalCloseBtn?.nativeElement?.click();
	}

	public goToUserAddressOnExplorer(): void {
		this.web3Service.goToAddressOnExplorer(
			this.userAddress,
			ChainSymbolEnum.eth,
			'address',
		);
	}

	public goToUser(profile: IProfile): void {
		if (!profile?.userAddress) {
			return;
		}
		const queryParams: Params = {
			userAddress: profile.userAddress,
		};


		this.uiLoaderService.stopAll();
		this.router.navigate(
			['/collections/user'],
			{queryParams},
		);
	}

	public goToUserTwitter(): void {
		if (!this.profile?.twitter) {
			return;
		}

		window.open(`https://twitter.com/${this.profile.twitter}`, '_blank');
	}

	public goToUserInstagram(): void {
		if (!this.profile?.instagram) {
			return;
		}

		window.open(`https://instagram.com/${this.profile.instagram}`, '_blank');
	}

	public goToUserDiscord(): void {
		const discordLink: string = this.profile?.discord;
		if (!discordLink) {
			return;
		}

		if (discordLink.includes('http')) {
			window.open(discordLink, '_blank');

			return;
		}

		window.open(new URL(`//${discordLink}`, document.baseURI).href, '_blank');
	}

	public goToUserENS(): void {
		if (!this.profile?.ens) {
			return;
		}

		window.open(`https://etherscan.io/enslookup-search?search=${this.profile.ens}`, '_blank');
	}

	public goToUserWebsite(): void {
		const externalLink: string = this.profile?.externalLink;
		if (!externalLink) {
			return;
		}

		if (externalLink.includes('http')) {
			window.open(externalLink, '_blank');

			return;
		}

		window.open(new URL(`//${externalLink}`, document.baseURI).href, '_blank');
	}

	public async toggleFollow(): Promise<void> {
		if (this.isOwner || !this.isProfileLoaded || !this.profile) {
			return;
		}

		this.isFollower ?
			await this.profileService.unfollow(this.userAddress) :
			await this.profileService.follow(this.userAddress);
		this.isFollower = !this.isFollower;
	}

	public async follow(profile: IProfile): Promise<void> {
		if (!profile.userAddress || profile.userAddress === this.connectedUserAddress) {
			return;
		}

		await this.profileService.follow(profile.userAddress);
		profile.isFollowed = true;
	}

	public async unfollow(profile: IProfile): Promise<void> {
		if (!profile.userAddress || profile.userAddress === this.connectedUserAddress) {
			return;
		}

		await this.profileService.unfollow(profile.userAddress);
		profile.isFollowed = false;
	}

	public async getFollowing(): Promise<void> {
		this.isFollowingLoaded = false;

		try {
			const following: IFollow[] = await this.profileService.getFollowing(this.userAddress);

			this.following = await this.profileService.getProfiles(following.map((follow) => follow.userAddress));
		} catch (e) {
			console.error(e);
		} finally {
			this.isFollowingLoaded = true;
		}
	}

	public async getFollowers(): Promise<void> {
		this.isFollowersLoaded = false;

		try {
			const followers: IFollow[] = await this.profileService.getFollowers(this.userAddress);

			this.followers = await this.profileService.getProfiles(followers.map((follow) => follow.userAddress));
		} catch (e) {
			console.error(e);
		} finally {
			this.isFollowersLoaded = true;
		}
	}

	private compressProfileCoverAndUpload(): Promise<void> {
		return new Promise((resolve, reject) => {
			const options: ICompressImageOptions = {
				maxSizeMB: 1,
				maxWidthOrHeight: 2200,
				useWebWorker: true,
				alwaysKeepResolution: true,
			};

			imageCompression(this.profileCoverFile, options)
				.then((compressedFile) => {
					this.firebaseStorageService.uploadFile(`cover_${this.userAddress}`, compressedFile).then((res) => {
						this.profileCoverFileStoragePath = res.ref.fullPath;
						resolve();
					});
				})
				.catch((error) => {
					console.error(error.message);
					reject(error);
				});
		});
	}

	private compressProfileAvatarAndUpload(): Promise<void> {
		return new Promise((resolve, reject) => {
			const options: ICompressImageOptions = {
				maxSizeMB: 1,
				maxWidthOrHeight: 1600,
				useWebWorker: true,
				alwaysKeepResolution: true,
			};

			imageCompression(this.profileAvatarFile, options)
				.then((compressedFile) => {
					this.firebaseStorageService.uploadFile(`avatar_${this.userAddress}`, compressedFile).then((res) => {
						this.profileAvatarFileStoragePath = res.ref.fullPath;
						resolve();
					});
				})
				.catch((error) => {
					console.error(error.message);
					reject(error);
				});
		});
	}

	private isProfileCoverFileValid(): boolean {
		if (!this.profileCoverFile) {
			return false;
		}

		// File bigger than 10MB is too big
		if ((this.profileCoverFile.size / 1024 / 1024) > 10) {
			this.profileCoverFileTooBig = true;

			return false;
		}

		return true;
	}

	private isProfileAvatarFileValid(): boolean {
		if (!this.profileAvatarFile) {
			return false;
		}

		// File bigger than 10MB is too big
		if ((this.profileAvatarFile.size / 1024 / 1024) > 3) {
			this.profileAvatarFileTooBig = true;

			return false;
		}

		return true;
	}

	private setMintedNFTs(): void {
		this.areNFTsLoaded = false;
		this.mintedNFTs = [];
		const userMintTransactions: Record<string, Record<string, IMintTx>> = this.userService.userTransactions$.getValue()?.mint;

		if (!userMintTransactions) {
			this.areNFTsLoaded = true;

			return;
		}

		for (const collectionAddress in userMintTransactions) {
			if (!userMintTransactions[collectionAddress]) {
				continue;
			}
			const collectionMintTransactions: IMintTx[] = Object.values(userMintTransactions[collectionAddress]);

			this.mintedFromCollections.push({
				id: collectionMintTransactions[0].collectionId,
				collectionAddress,
				chainSymbol: collectionMintTransactions[0].dstChainSymbol,
			});
		}
		let mintedFromCollectionsLoadedCount: number = 0;

		if (!this.mintedFromCollections.length) {
			this.areNFTsLoaded = true;

			return;
		}

		for (const collectionMintedFromByUser of this.mintedFromCollections.reverse()) {
			try {
				if (!collectionMintedFromByUser || !collectionMintedFromByUser.collectionAddress || !collectionMintedFromByUser.chainSymbol) {
					mintedFromCollectionsLoadedCount++;
					continue;
				}

				try {
					this.nftsService.getMintedByUser(
						collectionMintedFromByUser.collectionAddress,
						collectionMintedFromByUser.id,
						collectionMintedFromByUser.chainSymbol,
						this.userAddress,
					).then((nfts) => {
						if (!nfts?.length) {
							return;
						}

						for (const nft of nfts) {
							this.mintedNFTs.push(nft);
						}
					}).finally(() => {
						mintedFromCollectionsLoadedCount++;

						if (mintedFromCollectionsLoadedCount === this.mintedFromCollections.length) {
							this.mintedNFTs.reverse();
							this.areNFTsLoaded = true;
						}
					});
				} catch (e) {
					mintedFromCollectionsLoadedCount++;
					console.error(e);
				}
			} catch (e) {
				console.error('Could not checkAndUpdateCreatingNFTsStatuses() for ', collectionMintedFromByUser.collectionAddress);
				console.error(e);
			}
		}
	}

	private listenToCollectionsStateChanges(): void {
		setTimeout(() => {
			for (const collection of this.userCollections) {
				if (![CollectionStatusEnum.INITIATED, CollectionStatusEnum.CREATING].includes(collection.status)) {
					continue;
				}

				this.uiLoaderService.startBackgroundLoader(`creatingCollectionLoader_${collection.id}`);
			}
		}, 0);

		this.subscriptions.add(
			this.collectionsDataService.createdCollectionsFromChain$.subscribe((createdCollections) => {
				if (createdCollections === undefined) {
					return;
				}

				if (createdCollections?.length && createdCollections[0].creator !== this.userAddress) {
					return;
				}

				this.createdCollectionsOnChain = createdCollections;
				this.areCreatedCollectionsFromChainLoaded = true;

				for (const createdCollection of createdCollections) {
					try {
						this.uiLoaderService.stopBackgroundLoader(`creatingCollectionLoader_${createdCollection.id}`);
					} catch (e) {
						console.error(e);
					}
				}
			}),
		);
	}

	private getConnectedUserData(): void {
		if (this.isLoadingCollections) {
			return;
		}
		this.isLoadingCollections = true;

		this.subscriptions.add(
			this.collectionsDataService.userCollections$.subscribe((userCollections: ICollectionDetails[]) => {
				if (userCollections === undefined) {
					return;
				}

				if (userCollections?.length && userCollections[0].creator !== this.userAddress) {
					return;
				}

				this.userCollections = userCollections;
				this.areUserCollectionsLoaded = true;

				this.createdCollections = userCollections.filter((collection) => collection.status === CollectionStatusEnum.CREATED);
				this.areCreatedCollectionsLoaded = true;

				this.listenToCollectionsStateChanges();
			}),
		);

		this.subscriptions.add(
			this.userService.userTransactions$.subscribe((userTransactions) => {
				if (userTransactions === undefined) {
					return;
				}

				const mintsCount: number = Object.keys(userTransactions?.mint || {}).length;

				if (mintsCount === this.mintsCount) {
					return;
				}
				this.mintsCount = mintsCount;
				this.setMintedNFTs();
			})
		);
	}

	private getSelectedUserData(): void {
		if (!this.userAddress || this.isLoadingCollections) {
			return;
		}
		this.isLoadingCollections = true;

		this.subscriptions.add(
			this.collectionsDataService.selectedUserCollections$.subscribe((userCollections: ICollectionDetails[]) => {
				if (userCollections === undefined) {
					return;
				}

				if (userCollections?.length && userCollections[0].creator !== this.userAddress) {
					return;
				}

				this.userCollections = userCollections;
				this.areUserCollectionsLoaded = true;

				this.createdCollections = userCollections.filter((collection) => collection.status === CollectionStatusEnum.CREATED);
				this.areCreatedCollectionsLoaded = true;
			}),
		);
		this.collectionsDataService.listenToSelectedUserCollections(this.userAddress);
	}

	private async setProfileData(): Promise<void> {
		this.isProfileLoaded = false;
		if (this.profile?.backgroundImage) {
			this.profileCover = await this.firebaseStorageService.getFileURL(this.profile.backgroundImage);
		}

		if (this.profile?.avatar) {
			this.profileAvatar = await this.firebaseStorageService.getFileURL(this.profile.avatar);
		}
		this.isProfileLoaded = true;
	}

	private async isFollowing(): Promise<boolean> {
		return await this.profileService.isFollower(this.userAddress);
	}
}
