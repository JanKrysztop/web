import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollectionDetailsComponent } from './components/collection-details/collection-details.component';
import { UserCollectionsComponent } from './components/user-collections/user-collections.component';
import { AllCollectionsComponent } from './components/all-collections/all-collections.component';
import { CollectionsRankingComponent } from './components/collections-ranking/collections-ranking.component';

const routes: Routes = [
	{
		path: '',
		component: AllCollectionsComponent,
	},
	{
		path: 'details',
		component: CollectionDetailsComponent,
	},
	{
		path: 'user',
		component: UserCollectionsComponent,
	},
	{
		path: 'rankings',
		component: CollectionsRankingComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CollectionsRoutingModule {
}
