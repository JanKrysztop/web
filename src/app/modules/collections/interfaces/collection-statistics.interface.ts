import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { SupportedAssetEnum } from '../../web3/enum/supported-asset.enum';

export interface ICollectionStatistics {
	id: string;
	collectionId?: number; // TODO (Must) Old numeric collection id - left for current routing param. Remove after full migration to Firestore
	name: string;
	fileURI?: string;
	minted: number;
	volume: number;
	totalSupply?: number;
	isOverMinted?: boolean;
	chainSymbol?: ChainSymbolEnum;
	chainName?: string;
	address?: string;
	assetName?: SupportedAssetEnum;
}
