export interface IMintData {
	collectionId: string;
	collectionName: string;
	collectionFileURI: string;
	userAddress: string;
	at: number;
	price: number;
}
