import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';

export interface IClaimEarnedTx {
	collectionId: number;
	dstChainSymbol: ChainSymbolEnum;
	srcChainSymbol: ChainSymbolEnum;
	at: number;
	status?: number;
}
