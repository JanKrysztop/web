import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { INetworkTaskState } from '../interfaces/network-task-state.interface';

export type NetworkTasksType = Record<ChainSymbolEnum, INetworkTaskState> | {};
