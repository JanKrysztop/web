import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Web3ModalModule } from '@mindsorg/web3modal-angular';
import NetworksService from './services/networks.service';

@NgModule({
	providers: [
		NetworksService,
	],
	imports: [
		CommonModule,
		Web3ModalModule,
	],
	exports: [
		Web3ModalModule,
	]
})
export class Web3Module {
}
