import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { CollectionIpfsMetadata } from '../../create/dto/collection-metadata.dto';
import { CollectionStatusEnum } from '../enum/collection-status.enum';
import { IProfile } from '../../user/interfaces/profile.interface';
import { ContentTypeEnum } from '../../collections/enums/content-type.enum';
import { ArtCategoryEnum } from '../../collections/enums/art-category.enum';
import { MusicCategoryEnum } from '../../collections/enums/music-category.enum';
import { SupportedAssetEnum } from '../enum/supported-asset.enum';
import { BigNumberish } from 'ethers';

export interface ICollectionDetails {
	id?: number;
	firestoreId?: string;
	createdAt: number;
	creator?: string;
	creatorProfile?: IProfile;
	collectionName: string;
	contentType?: ContentTypeEnum;
	category?: ArtCategoryEnum | MusicCategoryEnum;
	status?: CollectionStatusEnum;
	dropFrom?: number;
	dropTo?: number;
	mintPrice: BigNumberish;
	assetName?: SupportedAssetEnum;
	totalSupply: number;
	tokenIds: number;
	collectionAddress?: string;
	chainSymbol?: ChainSymbolEnum;
	metadataURI?: string;
	fileURI?: string;
	fileStoragePath?: string;
	nftsFilesStoragePaths?: string[];
	hidden?: boolean;
	metadata?: CollectionIpfsMetadata;
	tokensURIs?: string[];
	isImageLoaded?: boolean;
	isImageLoading?: boolean;
	imageURL?: string;
	isPublic?: boolean;
	srcChain?: ChainSymbolEnum;
	onChainLoaders?: {
		mintedCount?: boolean;
		isCheckingIfCreated?: boolean;
	};
	txHash?: string;
	txStatus?: number;
	reason?: string;
}
