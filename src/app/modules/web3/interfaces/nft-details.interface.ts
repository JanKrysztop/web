import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { ContentTypeEnum } from '../../collections/enums/content-type.enum';

export interface INFTDetails {
	tokenId?: number;
	name?: string;
	description?: string;
	file?: File;
	fileIpfsHash?: string;
	smFileIpfsHash?: string;
	fileStoragePath?: string;
	tokenURI?: string;
	attributes?: Record<string, string | boolean | number>;
	fileURL?: string;
	contentType?: ContentTypeEnum;
	isFileLoaded?: boolean;
	collectionId?: number;
	collectionName?: string;
	collectionAddress?: string;
	collectionMetadataURI?: string;
	chainSymbol?: ChainSymbolEnum;
}

