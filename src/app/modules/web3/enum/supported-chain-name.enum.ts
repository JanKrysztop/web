export enum SupportedChainNameEnum {
	Ethereum = 'Ethereum',
	Fantom = 'Fantom',
	BSC = 'BNB Chain',
	Avalanche = 'Avalanche',
	Polygon = 'Polygon',
	Arbitrum = 'Arbitrum',
	Optimism = 'Optimism',
	Moonbeam = 'Moonbeam',
}
