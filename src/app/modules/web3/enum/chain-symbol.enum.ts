export enum ChainSymbolEnum {
	eth = 'Ethereum',
	avax = 'Avalanche',
	ftm = 'Fantom',
	bsc = 'BSC',
	polygon = 'Polygon',
	arb = 'Arbitrum',
	optimism = 'Optimism',
	moonbeam = 'Moonbeam',
}
