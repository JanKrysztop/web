import { Injectable } from '@angular/core';
import { Web3ModalService } from '@mindsorg/web3modal-angular';
import { Web3Provider } from '@ethersproject/providers';
import { BehaviorSubject } from 'rxjs';
import { ChainSymbolEnum } from '../enum/chain-symbol.enum';
import { environment } from '../../../../environments/environment';
import { ChainIdToSymbolMap } from '../types/chain-id-to-symbol-map.type';
import NetworksService from './networks.service';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { BigNumber, ethers } from 'ethers';
import { Contract } from '@ethersproject/contracts';
import FirebaseAnalyticsService from '../../firebase/services/firebase-analytics.service';

@Injectable()
export default class Web3Service {
	public get chainIdToSymbolMap(): ChainIdToSymbolMap {
		return environment.chainIdToSymbolMap;
	}

	public networksProviders$: Record<ChainSymbolEnum, BehaviorSubject<BaseProvider>>;
	public MAINNETNetworksProviders$: Record<ChainSymbolEnum, BehaviorSubject<BaseProvider>>;
	public readonly provider$: BehaviorSubject<Web3Provider> = new BehaviorSubject<Web3Provider>(undefined);
	public readonly networkId$: BehaviorSubject<number> = new BehaviorSubject<number>(null);
	private readonly erc20Abi: string[] = [
		'function balanceOf(address account) view returns (uint256)',
	];

	constructor(
		private readonly web3ModalService: Web3ModalService,
		private readonly networksService: NetworksService,
		private readonly analyticsService: FirebaseAnalyticsService,
	) {
		this.provider$.subscribe((provider) => {
			if (!provider) {
				return;
			}

			provider.on('network', (newNetwork, oldNetwork) => {
				if (oldNetwork && !this.isProviderTorus()) {
					window.location.reload();
				}
			});

			setInterval(() => {
				if (this.provider$.getValue() && !this.isWalletUnlocked()) {
					this.provider$.next(undefined);
				}
			}, 500);
		});

		this.networksProviders$ = {
			[ChainSymbolEnum.eth]: undefined,
			[ChainSymbolEnum.bsc]: undefined,
			[ChainSymbolEnum.avax]: undefined,
			[ChainSymbolEnum.polygon]: undefined,
			[ChainSymbolEnum.arb]: undefined,
			[ChainSymbolEnum.optimism]: undefined,
			[ChainSymbolEnum.ftm]: undefined,
			[ChainSymbolEnum.moonbeam]: undefined,
		};
		this.MAINNETNetworksProviders$ = {
			[ChainSymbolEnum.eth]: undefined,
			[ChainSymbolEnum.bsc]: undefined,
			[ChainSymbolEnum.avax]: undefined,
			[ChainSymbolEnum.polygon]: undefined,
			[ChainSymbolEnum.arb]: undefined,
			[ChainSymbolEnum.optimism]: undefined,
			[ChainSymbolEnum.ftm]: undefined,
			[ChainSymbolEnum.moonbeam]: undefined,
		};
		const supportedChains: ChainSymbolEnum[] = environment.supportedChains;

		for (const chain of supportedChains) {
			this.networksProviders$[chain] = new BehaviorSubject<BaseProvider>(undefined);
			this.MAINNETNetworksProviders$[chain] = new BehaviorSubject<BaseProvider>(undefined);
		}
	}

	public async connectWithModal(): Promise<void> {
		try {
			const providerConnection = await this.web3ModalService.open();
			const provider: Web3Provider = new Web3Provider(providerConnection, 'any');

			this.provider$.next(provider);

			try {
				const chainSymbol: ChainSymbolEnum = this.getConnectedChainSymbol();

				this.analyticsService.logEvent('login', {
					method: `metamask-${chainSymbol}`,
				});
			} catch (e) {
				console.error(e);
			}
		} catch (e) {
			console.error(e);
			this.provider$.next(undefined);
			// TODO: Notification "Couldn't connect to wallet"
		}
	}

	public getProvider(): Web3Provider {
		return this.provider$.value;
	}

	public isConnected(): boolean {
		return !!this.getProvider() && this.isWalletUnlocked();
	}

	public isProvidedCached(): boolean {
		// @ts-ignore
		return this.web3ModalService?.web3WalletConnector?.cachedProvider === 'injected';
	}

	public async getUserAddress(): Promise<string | undefined> {
		return await this.getProvider()?.getSigner()?.getAddress() || undefined;
	}

	public isWalletUnlocked(): boolean {
		if (this.isProviderTorus()) {
			// @ts-ignore
			return this.getProvider()?.provider?.selectedAddress?.length > 0;
		}

		return window.ethereum?.selectedAddress?.length > 0;
	}

	public showTorus(): void {
		// @ts-ignore
		this.provider$.getValue().provider.torus.showTorusButton();
	}

	private isProviderTorus(): boolean {
		// @ts-ignore
		return this.provider$.getValue()?.provider?.isTorus;
	}

	public getConnectedChainSymbol(): ChainSymbolEnum | undefined {
		const chainId: number = this.getProvider()?.network?.chainId;

		return chainId > 0 ? this.chainIdToSymbolMap[chainId] : undefined;
	}

	public goToAddressOnExplorer(address: string, chainSymbol: ChainSymbolEnum, page?: string, params?: string): void {
		const chainSymbolToExplorerUrlMap: Record<ChainSymbolEnum, string> = environment.chainSymbolToExplorerUrlMap;

		window.open(`${chainSymbolToExplorerUrlMap[chainSymbol]}/${page || 'address'}/${address}/${params || ''}`, '_blank');
	}

	public getNetworkProvider(chainSymbol: ChainSymbolEnum, fallbackId?: number): BaseProvider {
		const provider: BaseProvider = this.networksProviders$[chainSymbol].getValue();

		if (!provider || fallbackId > 0) {
			this.networksProviders$[chainSymbol].next(
				ethers.getDefaultProvider(this.networksService.getNetworkByChainSymbol(chainSymbol, fallbackId || 0)),
			);
		}

		return this.networksProviders$[chainSymbol].getValue();
	}

	public getMAINNETNetworkProvider(chainSymbol: ChainSymbolEnum, fallbackId?: number): BaseProvider {
		const provider: BaseProvider = this.MAINNETNetworksProviders$[chainSymbol].getValue();

		if (!provider || fallbackId > 0) {
			this.MAINNETNetworksProviders$[chainSymbol].next(
				ethers.getDefaultProvider(this.networksService.getMAINNETNetworkByChainSymbol(chainSymbol, fallbackId || 0)),
			);
		}

		return this.MAINNETNetworksProviders$[chainSymbol].getValue();
	}


	public async getBalance(tokenAddress: string): Promise<BigNumber> {
		const provider: Web3Provider = this.getProvider();
		const userAddress: string = await provider?.getSigner()?.getAddress();

		if (!userAddress) {
			return BigNumber.from(0);
		}
		const tokenContract: Contract = new ethers.Contract(tokenAddress, this.erc20Abi, provider.getSigner());

		return await tokenContract.balanceOf(userAddress);
	}

	public async getNativeAssetBalance(address: string): Promise<BigNumber> {
		const provider: Web3Provider = this.getProvider();
		const userAddress: string = address || await provider?.getSigner()?.getAddress();

		if (!userAddress) {
			return BigNumber.from(0);
		}

		return await provider.getBalance(userAddress);
	}

	public async getFormattedNativeAssetBalance(address: string): Promise<number> {
		return Number(ethers.utils.formatEther(await this.getNativeAssetBalance(address)));
	}

	public encodePayload(params: unknown[], paramsTypes: string[]): string {
		return ethers.utils.AbiCoder.prototype.encode(
			paramsTypes,
			params,
		);
	}
}
