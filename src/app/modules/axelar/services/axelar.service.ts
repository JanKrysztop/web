import { Injectable } from '@angular/core';
import { AxelarQueryAPI, Environment, EvmChain, GasToken } from '@axelar-network/axelarjs-sdk';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { BehaviorSubject } from 'rxjs';
import { BigNumberish } from 'ethers';
import { environment } from '../../../../environments/environment';

@Injectable({
	providedIn: 'root',
})
export default class AxelarService {
	public readonly sdk$: BehaviorSubject<AxelarQueryAPI> = new BehaviorSubject<AxelarQueryAPI>(undefined);

	constructor() {
		this.initializeSdk();
	}

	public async getEstimatedFee(
		srcChainSymbol: ChainSymbolEnum,
		dstChainSymbol: ChainSymbolEnum,
		gasLimit?: number,
	): Promise<BigNumberish> {
		return await this.sdk$.getValue().estimateGasFee(
			this.mapChainName(srcChainSymbol),
			this.mapChainName(dstChainSymbol),
			environment.chainSymbolToGasToken[srcChainSymbol],
			gasLimit || undefined,
		);
	}

	private initializeSdk(): void {
		this.sdk$.next(new AxelarQueryAPI({
			environment: Environment.TESTNET,
		}));
	}

	private mapChainName(chainSymbol: ChainSymbolEnum): EvmChain {
		return EvmChain[chainSymbol.toUpperCase()];
	}
}
