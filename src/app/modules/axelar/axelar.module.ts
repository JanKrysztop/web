import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import AxelarService from './services/axelar.service';

@NgModule({
	providers: [
		AxelarService,
	],
	imports: [
		CommonModule,
	],
})
export class AxelarModule {
}
