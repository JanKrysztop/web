import { BigNumberish } from 'ethers';

export interface IEstimatedFees {
	fee: BigNumberish;
	redirectFee: BigNumberish;
}
