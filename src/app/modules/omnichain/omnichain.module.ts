import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import OmnichainService from './services/omnichain.service';

@NgModule({
	providers: [
		OmnichainService,
	],
	imports: [
		CommonModule,
	],
})
export class OmnichainModule {
}
