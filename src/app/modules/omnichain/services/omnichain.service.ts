import { Injectable } from '@angular/core';
import { ChainSymbolEnum } from '../../web3/enum/chain-symbol.enum';
import { Contract } from '@ethersproject/contracts';
import { BaseProvider } from '@ethersproject/providers/src.ts/base-provider';
import { environment } from '../../../../environments/environment';
import { BigNumber, BigNumberish, ethers } from 'ethers';
import Web3Service from '../../web3/services/web3.service';
import AxelarService from '../../axelar/services/axelar.service';
import { IEstimatedFees } from '../interfaces/estimated-fees.interface';
import { OmnichainActionTypeEnum } from '../enums/omnichain-action-type.enum';

@Injectable({
	providedIn: 'root',
})
export default class OmnichainService {
	public readonly omnichainRouterContractAbi: string[] = [
		'function isDirectRoute(string memory dstChainName) public view returns (bool)',
		'function estimateFees(uint16 chainId, bytes memory _payload, uint gas) external view returns (uint)'
	];

	constructor(
		private readonly web3Service: Web3Service,
		private readonly axelarService: AxelarService,
	) {
	}

	public async isDirectRoute(dstChainSymbol: ChainSymbolEnum): Promise<boolean> {
		const connectedChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!connectedChain) {
			return;
		}
		const omnichainRouter: Contract = await this.getOmnichainRouterContract(connectedChain);

		return await omnichainRouter.isDirectRoute(dstChainSymbol);
	}

	public async getOmnichainRouterContract(chainSymbol: ChainSymbolEnum, fallbackId?: number): Promise<Contract> {
		fallbackId = fallbackId || 0;

		try {
			const provider: BaseProvider = this.web3Service.getNetworkProvider(chainSymbol, fallbackId);
			const omnichainRouterAddress: string = environment.omnichainRouterAddressesMap[chainSymbol];

			if (!omnichainRouterAddress) {
				return;
			}

			return new ethers.Contract(omnichainRouterAddress, this.omnichainRouterContractAbi, provider);
		} catch (e) {
			if (e?.message?.includes('could not detect network (event="noNetwork')) {
				fallbackId++;
				if (fallbackId === 1) { // TODO: Enable more fallbacks by checking if present
					return await this.getOmnichainRouterContract(chainSymbol, fallbackId);
				}
			}

			console.error(e);
			return;
		}
	}

	public async getEstimatedFees(
		dstChain: ChainSymbolEnum,
		encodedPayload: string,
		actionType: OmnichainActionTypeEnum,
	): Promise<IEstimatedFees> {
		const srcChain: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		if (!srcChain) {
			return;
		}
		const isDirectRoute: boolean = await this.isDirectRoute(dstChain);
		const gasLimit: number = this.getGasLimitByActionType(actionType, dstChain);
		const omnichainRouter: Contract = await this.getOmnichainRouterContract(srcChain);

		if (isDirectRoute) {
			// Right now, Moonbeam involved = Axelar being used
			const isAxelarRoute: boolean = srcChain === ChainSymbolEnum.moonbeam || dstChain === ChainSymbolEnum.moonbeam;

			if (isAxelarRoute) {
				return {
					fee: await this.axelarService.getEstimatedFee(srcChain, dstChain, gasLimit),
					redirectFee: 0,
				};
			}

			return {
				fee: await omnichainRouter.estimateFees(environment.chainSymbolToLzChainId[dstChain], encodedPayload, gasLimit),
				redirectFee: 0,
			};
		}

		let fee: BigNumberish;
		let redirectFee: BigNumberish;
		// First route (redirect Moonbeam -> Avalanche) has to be via Axelar + default gasLimit
		if (srcChain === ChainSymbolEnum.moonbeam) {
			fee = await this.axelarService.getEstimatedFee(srcChain, ChainSymbolEnum.avax);
		} else {
			// First route (redirect to Avalanche) via LayerZero + default gasLimit
			fee = await omnichainRouter.estimateFees(environment.chainSymbolToLzChainId[ChainSymbolEnum.avax], encodedPayload, 0);
		}


		// Redirected (2nd) route (redirect Avalanche -> Moonbeam) has to be via Axelar + custom gas per action type
		if (dstChain === ChainSymbolEnum.moonbeam) {
			redirectFee = await this.axelarService.getEstimatedFee(ChainSymbolEnum.avax, dstChain, gasLimit);

			return {
				fee: BigNumber.from(fee).add(redirectFee),
				redirectFee,
			};
		}
		const redirectRouter: Contract = await this.getOmnichainRouterContract(ChainSymbolEnum.avax);

		redirectFee = await redirectRouter.estimateFees(
			environment.chainSymbolToLzChainId[dstChain],
			encodedPayload,
			gasLimit,
		);

		// Redirected (2nd) route (redirect from Avalanche) via LayerZero + custom gas per action type
		return {
			fee: BigNumber.from(fee).add(redirectFee),
			redirectFee,
			// LayerZero will charge UA on redirected route (Avalanche) and estimate redirectFee on-chain when this param is > 0.
		};
	}

	public getGasLimitByActionType(actionType: OmnichainActionTypeEnum, dstChain: ChainSymbolEnum): number {
		switch (actionType) {
			case OmnichainActionTypeEnum.MINT_TOKEN:
				return environment.chainSymbolToMintTokenGas[dstChain];
			case OmnichainActionTypeEnum.NFT_GATEWAY_SEND:
				return environment.chainSymbolToBridgeNFTGas[dstChain];
			case OmnichainActionTypeEnum.CREATE_COLLECTION:
			default:
				return environment.chainSymbolToCreateCollectionGas[dstChain];
		}
	}
}
