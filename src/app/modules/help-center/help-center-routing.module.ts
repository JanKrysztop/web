import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelpCenterComponent } from './components/help-center/help-center.component';
import { HelpCenterCategoryComponent } from './components/help-center-category/help-center-category.component';
import { HelpCenterTopicComponent } from './components/help-center-topic/help-center-topic.component';

const routes: Routes = [
	{
		path: '',
		component: HelpCenterComponent,
	},
	{
		path: 'category',
		component: HelpCenterCategoryComponent,
	},
	{
		path: 'topic',
		component: HelpCenterTopicComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class HelpCenterRoutingModule {
}
