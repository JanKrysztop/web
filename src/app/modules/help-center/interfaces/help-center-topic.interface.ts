import { Moment } from 'moment';

export interface IHelpCenterTopic {
	name: string;
	description: string;
	imageName: string;
	imagePath?: string;
	content?: string;
	redirectTo?: string;
	updatedDate?: Date | Moment;
}
