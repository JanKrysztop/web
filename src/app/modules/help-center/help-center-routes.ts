import { Routes } from '@angular/router';

export const helpCenterRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./help-center.module').then(m => m.HelpCenterModule),
	},
];
