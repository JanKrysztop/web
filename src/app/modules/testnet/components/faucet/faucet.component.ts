import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup } from '@angular/forms';
import Web3Service from '../../../web3/services/web3.service';
import { environment } from '../../../../../environments/environment';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import NotificationsService from '../../../shared/services/notifications.service';
import { ethers, Signer } from 'ethers';
import { ContractTransaction } from '@ethersproject/contracts/src.ts/index';
import { Router } from '@angular/router';
import { SupportedAssetEnum } from '../../../web3/enum/supported-asset.enum';

@Component({
	selector: 'app-faucet',
	templateUrl: './faucet.component.html',
	styleUrls: ['./faucet.component.scss']
})
export class FaucetComponent implements OnInit, OnDestroy {
	public subscriptions: Subscription;
	public createForm: FormGroup;
	public isConnectedToWallet: boolean;
	public userAddress: string;
	public assetName: SupportedAssetEnum;
	private connectedChainSymbol: ChainSymbolEnum;
	private testTokenContractAddress: string;
	private readonly testTokenContractAbi: string[] = [
		'function useFaucet() public',
	];

	constructor(
		private readonly web3Service: Web3Service,
		private readonly notificationsService: NotificationsService,
		private readonly router: Router,
	) {
	}

	public async ngOnInit(): Promise<void> {
		this.subscriptions = new Subscription();
		this.assetName = SupportedAssetEnum.USDC;

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				setTimeout(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();

					if (this.isConnectedToWallet) {
						this.userAddress = await this.web3Service.getUserAddress();
						this.connectedChainSymbol = this.web3Service.getConnectedChainSymbol();

						if (!this.connectedChainSymbol) {
							await this.connectWallet();
						}
						this.testTokenContractAddress = environment.chainSymbolToAsset[this.assetName][this.connectedChainSymbol];
					}
				}, 250);
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public onAssetChange(assetName: string): void {
		this.assetName = SupportedAssetEnum[assetName];
		this.testTokenContractAddress = environment.chainSymbolToAsset[this.assetName][this.connectedChainSymbol];
	}

	public async useFaucet(): Promise<void> {
		const signer: Signer = this.web3Service?.getProvider()?.getSigner();

		if (!signer) {
			this.notificationsService.error('Connect your wallet');
			return;
		}
		const contract = new ethers.Contract(
			this.testTokenContractAddress,
			this.testTokenContractAbi,
			signer,
		);

		try {
			const tx: ContractTransaction = await contract.useFaucet();

			tx.wait().then(() => {
				this.notificationsService.success('Success', `You\'ve received 1000 ${this.assetName}`);
				setTimeout(() => {
					this.goToExplore();
				}, 500);
			}).catch((e) => {
				this.notificationsService.error('Transaction Error', 'Limit is 1000 tokens per 30 minutes');
				console.error(e);
			});
		} catch (e) {
			console.error(e);
			if (e?.message?.includes('underlying network changed')) {
				this.notificationsService.error('Error', 'You\'ve changed network during the process');
				setTimeout(() => {
					window.location.reload();
				}, 2500);

				return;
			}

			this.notificationsService.error('Transaction Error', 'Limit is 1000 tokens per 30 minutes');
		}
	}

	public connectWallet(): void {
		this.web3Service.connectWithModal();
	}

	public goToExplore(): void {
		this.router.navigate(['/collections']);
	}
}
