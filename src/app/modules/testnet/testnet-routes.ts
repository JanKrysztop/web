import { Routes } from '@angular/router';

export const testnetRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./testnet.module').then(m => m.TestnetModule),
	},
];
