import { NgModule } from '@angular/core';
import { TestnetRoutingModule } from './testnet-routing.module';
import { SharedModule } from '../shared/shared.module';
import { Web3Module } from '../web3/web3.module';
import { FaucetComponent } from './components/faucet/faucet.component';

@NgModule({
	declarations: [
		FaucetComponent,
	],
	imports: [
		TestnetRoutingModule,
		SharedModule,
		Web3Module,
	],
	exports: [
		FaucetComponent,
	]
})
export class TestnetModule {
}
