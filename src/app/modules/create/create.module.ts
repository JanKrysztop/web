import { NgModule } from '@angular/core';
import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './components/create/create.component';
import { SharedModule } from '../shared/shared.module';
import { Web3Module } from '../web3/web3.module';

@NgModule({
	declarations: [
		CreateComponent,
	],
	imports: [
		CreateRoutingModule,
		SharedModule,
		Web3Module,
	],
	exports: [
		CreateComponent,
	]
})
export class CreateModule {
}
