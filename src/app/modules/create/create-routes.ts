import { Routes } from '@angular/router';

export const createRoutes: Routes = [
	{
		path: '',
		loadChildren: () => import('./create.module').then(m => m.CreateModule),
		data: { preload: true },
	},
];
