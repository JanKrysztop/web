export interface CollectionMetadataDto {
	name: string;
	description?: string;
	image?: string;
	media?: string;
	file?: string;
	external_link?: string;
	attributes?: Array<{
		type: string;
		name: string;
		value: string | number;
	}>;
}

export type CollectionIpfsMetadata = Readonly<CollectionMetadataDto>;
