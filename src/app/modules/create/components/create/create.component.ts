import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import IpfsService from '../../../ipfs/services/ipfs.service';
import Web3Service from '../../../web3/services/web3.service';
import { BigNumber, ethers } from 'ethers';
import { Web3Provider } from '@ethersproject/providers';
import { JsonRpcSigner } from '@ethersproject/providers/src.ts/json-rpc-provider';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { StepperConfigType } from '../../../shared/types/stepper-config.type';
import { environment } from '../../../../../environments/environment';
import { AddResult } from 'ipfs-core-types/types/src/root';
import { IStepperStepConfig } from '../../../shared/interfaces/stepper-step-config.interface';
import { StepperStatusEnum } from '../../../shared/enums/stepper-status.enum';
import { CollectionMetadataDto } from '../../dto/collection-metadata.dto';
import { ContractReceipt, ContractTransaction } from '@ethersproject/contracts/src.ts/index';
import { ChainNameToSymbolMap } from '../../../web3/types/chain-name-to-symbol-map.type';
import { ChainSymbolEnum } from '../../../web3/enum/chain-symbol.enum';
import { SupportedChainNameEnum } from '../../../web3/enum/supported-chain-name.enum';
import * as moment from 'moment-timezone';
import { Params, Router } from '@angular/router';
import NotificationsService from '../../../shared/services/notifications.service';
import imageCompression from 'browser-image-compression';
import { ICompressImageOptions } from '../../../shared/interfaces/compress-image-options.interface';
import FirebaseStorageService from '../../../firebase/services/firebase-storage.service';
import FirebaseDatabaseService from '../../../firebase/services/firebase-database.service';
import CollectionsDataService from '../../../chain-data/services/collections-data.service';
import { INFTDetails } from '../../../web3/interfaces/nft-details.interface';
import { CollectionStatusEnum } from '../../../web3/enum/collection-status.enum';
import CollectionsService from '../../../chain-data/services/collections.service';
import { ICollectionDetails } from '../../../web3/interfaces/collection-details.interface';
import FirestoreService from '../../../firebase/services/firestore.service';
import { ICreateCollectionTx } from '../../../collections/interfaces/create-collection-tx.interface';
import { UploadResult } from '@firebase/storage';
import { ContentTypeEnum } from '../../../collections/enums/content-type.enum';
import { ArtCategoryEnum } from '../../../collections/enums/art-category.enum';
import { MusicCategoryEnum } from '../../../collections/enums/music-category.enum';
import OmnichainService from '../../../omnichain/services/omnichain.service';
import { IEstimatedFees } from '../../../omnichain/interfaces/estimated-fees.interface';
import { OmnichainActionTypeEnum } from '../../../omnichain/enums/omnichain-action-type.enum';
import { SupportedAssetEnum } from '../../../web3/enum/supported-asset.enum';
import { INFTMetadata } from '../../../nfts/interfaces/nft-metadata.interface';

@Component({
	selector: 'app-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit, OnDestroy {
	public get supportedNetworksAsText(): string {
		let supportedNetworksText: string = '';

		for (const chainName in this.chainNameToSymbolMap) {
			if (!this.chainNameToSymbolMap[chainName]) {
				continue;
			}

			supportedNetworksText += `${chainName} | `;
		}

		return supportedNetworksText;
	}

	public get supportedAssets(): string[] {
		return environment.supportedAssets;
	}

	private get currentStep(): IStepperStepConfig {
		return this.stepper[this.activeStep];
	}

	public get nftFile(): File | undefined {
		return this.nftForm.get('file').value;
	}

	public get availableCategories(): ArtCategoryEnum[] | MusicCategoryEnum[] {
		switch (this.selectedContentType) {
			case ContentTypeEnum.MUSIC:
				return [
					MusicCategoryEnum.ROCK,
					MusicCategoryEnum.BLUES,
					MusicCategoryEnum.HIPHOP,
					MusicCategoryEnum.PUNK,
					MusicCategoryEnum.POP,
					MusicCategoryEnum.KPOP,
					MusicCategoryEnum.INDIE,
					MusicCategoryEnum.ELECTRONIC_DANCE,
					MusicCategoryEnum.RNB_SOUL,
					MusicCategoryEnum.REGGAE,
					MusicCategoryEnum.FUNK,
					MusicCategoryEnum.COUNTRY,
					MusicCategoryEnum.CLASSICAL,
					MusicCategoryEnum.GENERATIVE,
					MusicCategoryEnum.OTHERS,
				];
			case ContentTypeEnum.ART:
				return [
					ArtCategoryEnum.ANY,
					ArtCategoryEnum.AVATAR,
					ArtCategoryEnum.ABSTRACT,
					ArtCategoryEnum.GENERATIVE,
					ArtCategoryEnum.PHOTOGRAPHY,
					ArtCategoryEnum.GAMING,
					ArtCategoryEnum.PAINTING,
					ArtCategoryEnum.TEXTURE,
				];
			default:
				this.onArtCategoryChange(ArtCategoryEnum.ANY);
		}
	}

	@ViewChild('createStepperCloseBtn') public createStepperCloseBtn: ElementRef<HTMLButtonElement>;
	@ViewChild('nftModalCloseBtn') public nftModalCloseBtn: ElementRef<HTMLButtonElement>;
	public subscriptions: Subscription;
	public createForm: FormGroup;
	public nftForm: FormGroup;
	public isConnectedToWallet: boolean;
	public userBalance: BigNumber;
	public selectedChainName: SupportedChainNameEnum;
	public selectedAssetName: SupportedAssetEnum;
	public readonly chainSymbols: ChainSymbolEnum[];
	public readonly chainNameToSymbolMap: Partial<ChainNameToSymbolMap>;
	public imagePreviewSrc: string | ArrayBuffer;
	public nftImagePreviewSrc: string | ArrayBuffer;
	public isFileLoading: boolean;
	public isFileLoadingError: boolean;
	public isFileInvalid: boolean;
	public isFileTooBig: boolean;
	public isNFTFileTooBig: boolean;
	public stepper: StepperConfigType;
	public activeStep: number;
	public isDrop: boolean;
	public maxDateTo: any;
	public today: any;
	public dropDurationInDays: number;
	public isAdvancedOptionsVisible: boolean;
	public file: File;
	public compressedFile: File;
	public nfts: INFTDetails[];
	public contentTypes: ContentTypeEnum[];
	public selectedContentType: ContentTypeEnum;
	public selectedCategory: ArtCategoryEnum | MusicCategoryEnum;
	private readonly isCreatingCollectionCancelled$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
	private id: number;
	private firestoreCollectionId: string;
	private tokensURIs: string[];
	private isMetadata: boolean;
	private metadataIpfsHash: string;
	private fileIpfsHash: string;
	private smImageIpfsHash: string;
	private collectionFactoryContractAddress: string;
	private readonly createParamsStruct: string = '(string memory dstChainName, string memory name, string memory uri, string memory fileURI, uint256 price, string memory assetName, uint256 from, uint256 to, string[] memory metadataURIs, uint gas, uint256 redirectFee)';
	private readonly collectionFactoryContractAbi: string[] = [
		`function create(${this.createParamsStruct} params) public payable`,
		'function repository() public view returns (address)',
		'event Created(address collectionAddress)',
	];
	private createdCollectionAddress: string;
	private userAddress: string;
	private connectedChainSymbol: ChainSymbolEnum;
	private walletCheckOnMint: NodeJS.Timer;
	private createTx?: ContractTransaction;
	private createTxResult: ContractReceipt | null;
	private isSubmitted: boolean;
	private collectionFileStoragePath: string;
	private nftsFilesStoragePaths: string[];

	constructor(
		private readonly formBuilder: FormBuilder,
		private readonly ipfsService: IpfsService,
		private readonly web3Service: Web3Service,
		private readonly firestoreService: FirestoreService,
		private readonly firebaseStorageService: FirebaseStorageService,
		private readonly firebaseDatabaseService: FirebaseDatabaseService,
		private readonly collectionsDataService: CollectionsDataService,
		private readonly collectionsService: CollectionsService,
		private readonly uiLoaderService: NgxUiLoaderService,
		private readonly router: Router,
		private readonly notificationsService: NotificationsService,
		private readonly omnichainService: OmnichainService,
	) {
		this.chainSymbols = environment.supportedChains;
		this.chainNameToSymbolMap = {};

		for (const chainName in environment.chainNameToSymbolMap) {
			if (
				!environment.chainNameToSymbolMap[chainName]
				|| !this.isSupportedNetwork(environment.chainNameToSymbolMap[chainName])
			) {
				continue;
			}
			this.chainNameToSymbolMap[chainName] = environment.chainNameToSymbolMap[chainName];
		}
	}

	public async ngOnInit(): Promise<void> {
		// Initial state:
		this.id = Number(`9${moment().unix()}${Math.floor(Math.random() * 90 + 10)}`);
		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');
		this.stepper = [];
		this.subscriptions = new Subscription();
		this.selectedChainName = SupportedChainNameEnum.Ethereum;
		this.selectedAssetName = SupportedAssetEnum.USDC;
		this.maxDateTo = moment().add(91, 'days').toDate();
		this.today = moment().startOf('day').toDate();
		this.nfts = [];
		this.nftsFilesStoragePaths = [];
		this.selectedContentType = ContentTypeEnum.ART;
		this.selectedCategory = ArtCategoryEnum.ANY;
		this.contentTypes = [ContentTypeEnum.ART, ContentTypeEnum.MUSIC];

		this.createForm = this.formBuilder.group({
			name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(250)]],
			description: ['', [Validators.maxLength(2000)]],
			contentType: [ContentTypeEnum.ART, Validators.required],
			category: [ArtCategoryEnum.ANY, Validators.required],
			externalLink: ['', [Validators.maxLength(250)]],
			mintPrice: [0, [Validators.min(0)]],
			assetName: [SupportedAssetEnum.USDC, Validators.required],
			dateFrom: [undefined],
			durationInDays: [undefined],
			chainSymbol: [ChainSymbolEnum.eth, [Validators.required, Validators.maxLength(50)]],
			isPublic: [true, Validators.required],
		});
		this.nftForm = this.formBuilder.group({
			file: [undefined, Validators.required], // TODO: Consider custom Validators for size and type
			attributes: this.formBuilder.array([], Validators.maxLength(25)),
		});

		this.subscriptions.add(
			this.createForm.get('mintPrice').valueChanges.subscribe((value: number) => {
				this.isDrop = Number(value) > 0;
			}),
		);

		this.subscriptions.add(
			this.web3Service.provider$.subscribe(async () => {
				const walletInterval: NodeJS.Timer = setInterval(async () => {
					this.isConnectedToWallet = this.web3Service.isConnected();

					if (this.isConnectedToWallet) {
						clearInterval(walletInterval);
						this.userAddress = await this.web3Service.getUserAddress();
						const connectedChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

						this.connectedChainSymbol = this.isSupportedNetwork(connectedChainSymbol) ? connectedChainSymbol : ChainSymbolEnum.eth;

						if (!this.connectedChainSymbol) {
							await this.connectWallet();
						}
						this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[this.connectedChainSymbol];
						const chainSymbolControl: AbstractControl = this.createForm.get('chainSymbol');

						if (
							this.connectedChainSymbol?.length > 0
							&& chainSymbolControl.value === ChainSymbolEnum.eth
							&& !this.isSubmitted
						) {
							chainSymbolControl.setValue(this.connectedChainSymbol);
							this.selectedChainName = this.mapChainSymbolToName(this.connectedChainSymbol);
						}

						this.web3Service.getNativeAssetBalance(this.userAddress).then((userBalance: BigNumber) => {
							this.userBalance = userBalance;
						});
					}
				}, 250);
			}),
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.unsubscribe();
	}

	public onFileChange(event: Event): void {
		this.isFileInvalid = false;
		this.isFileLoadingError = false;
		this.isFileTooBig = false;
		this.imagePreviewSrc = undefined;
		this.file = undefined;
		const input: HTMLInputElement = event.target as HTMLInputElement;

		if (input?.files?.[0]) {
			this.file = input.files[0];
			const mimeType: string = this.file.type;

			if (!mimeType.match(/image\/*/)) {
				this.notificationsService.error('Not Image', 'Collection\'s cover has to be an image.');

				return;
			}

			if (this.isFileValid()) {
				this.compressCollectionImageAndSaveInIpfs();
			}
			const reader = new FileReader();

			reader.onloadstart = () => {
				this.isFileLoading = true;
			};
			reader.onload = async (e) => {
				this.isFileLoading = false;

				if (!this.isFileValid()) {
					this.isFileInvalid = true;
					this.file = undefined;
					return;
				}

				if (!mimeType.match(/image\/*/)) {
					return;
				}

				this.imagePreviewSrc = reader.result;
			};
			reader.onerror = (err) => {
				this.isFileLoading = false;
				this.isFileLoadingError = true;
				console.error(err);
			};
			reader.readAsDataURL(this.file);
		}
	}

	public onNFTFileChange(event: Event): void {
		this.nftImagePreviewSrc = undefined;
		this.nftForm.get('file').setValue(undefined);
		const input: HTMLInputElement = event.target as HTMLInputElement;

		if (!input?.files?.[0]) {
			return;
		}
		const mimeType: string = input.files[0].type;

		if (this.selectedContentType === 'MUSIC' && !mimeType.match(/audio\/*/)) {
			this.notificationsService.error('Add Audio', 'Music type collection\'s NFTs have to be audio files');

			return;
		}

		if (this.selectedContentType === 'ART' && !mimeType.match(/image\/*/)) {
			this.notificationsService.error('Add Image', 'Art type collection\'s NFTs have to be image files');

			return;
		}

		this.nftForm.get('file').setValue(input.files[0]);
		const reader = new FileReader();

		reader.onload = async (e) => {
			if (!this.isNFTFileValid()) {
				this.nftForm.get('file').setValue(undefined);
				return;
			}

			if (!mimeType.match(/image\/*/)) {
				return;
			}

			this.nftImagePreviewSrc = reader.result;
		};

		if (!mimeType.match(/image\/*/)) {
			return;
		}

		reader.readAsDataURL(this.nftFile);
	}

	public async onSubmit(): Promise<void> {
		this.isSubmitted = true;
		this.isCreatingCollectionCancelled$.next(false);

		if (!this.isConnectedToWallet || !this.connectedChainSymbol) {
			await this.connectWallet();
		}

		if (this.createForm.get('name').value.length < 3) {
			this.notificationsService.error('Invalid Name', 'Name has to contain minimum 3 characters.');

			return;
		}

		if (this.createForm.invalid) {
			this.notificationsService.error('Invalid Form', 'Check the form. Provide at least name and file.');

			return;
		}

		if (!this.isFileSupported()) {
			this.notificationsService.error('Upload File', 'Make sure to upload image or audio file.');
			return;
		}

		if (this.selectedContentType === 'MUSIC' && this.nfts?.length === 0) {
			this.notificationsService.error('Add NFTs', 'Music collection requires at least 1 audio NFT');
			return;
		}
		const externalLink: string = this.createForm.get('externalLink').value;

		if (externalLink?.length) {
			const reg: RegExp = new RegExp('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?');

			if (!reg.test(externalLink)) {
				this.notificationsService.error('Invalid External Link', 'Provided link is not a valid URL');
				return;
			}
		}
		this.isMetadata = this.isMetadataProvided();
		this.buildSteps();

		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}

		this.subscriptions.add(this.createCollection().subscribe());
	}

	public isWalletConnected(): boolean {
		return this.web3Service.isConnected();
	}

	public connectWallet(): void {
		this.web3Service.connectWithModal();
	}

	public onTargetChainChange(chainName: SupportedChainNameEnum): void {
		this.selectedChainName = chainName;
		this.createForm.get('chainSymbol').setValue(this.chainNameToSymbolMap[chainName]);
	}

	public onAssetChange(assetName: string): void {
		this.selectedAssetName = SupportedAssetEnum[assetName];
		this.createForm.get('assetName').setValue(this.selectedAssetName);
	}

	public onContentTypeChange(contentType: ContentTypeEnum): void {
		this.selectedContentType = this.mapContentTypeToEnum(contentType);
		this.createForm.get('contentType').setValue(this.selectedContentType);
		this.onCategoryChange();
	}

	public onCategoryChange(category?: ArtCategoryEnum | MusicCategoryEnum): void {
		switch (this.selectedContentType) {
			case ContentTypeEnum.MUSIC:
				this.onMusicCategoryChange(category as MusicCategoryEnum || MusicCategoryEnum.ROCK);
				break;
			case ContentTypeEnum.ART:
			default:
				this.onArtCategoryChange(category as ArtCategoryEnum || ArtCategoryEnum.ANY);
		}
		this.createForm.get('category').setValue(this.selectedCategory);
	}

	public onArtCategoryChange(category: ArtCategoryEnum): void {
		this.selectedCategory = category;
	}

	public onMusicCategoryChange(category: MusicCategoryEnum): void {
		this.selectedCategory = category;
	}

	public mapChainNameToEnum(chainName: string): SupportedChainNameEnum {
		return chainName as SupportedChainNameEnum;
	}

	public mapContentTypeToEnum(contentType: string): ContentTypeEnum {
		return ContentTypeEnum[contentType.toUpperCase()];
	}

	public onDateFromChange(): void {
		const dateFrom: Date = this.createForm.get('dateFrom').value;

		if (!dateFrom) {
			this.createForm.get('durationInDays').reset();

			return;
		}
	}

	public toggleAdvancedOptions(): void {
		this.isAdvancedOptionsVisible = !this.isAdvancedOptionsVisible;
	}

	public isNFTFileSupported(): boolean {
		if (!this.nftFile) {
			return false;
		}

		return !!this.nftFile.type.match(/image\/*/)
			|| !!this.nftFile.type.match(/audio\/*/);
	}

	public isFileSupported(): boolean {
		if (!this.file) {
			return false;
		}

		return !!this.file.type.match(/image\/*/)
			|| !!this.file.type.match(/audio\/*/);
	}

	public isFileImage(): boolean {
		if (!this.file) {
			return false;
		}

		return !!this.file.type.match(/image\/*/);
	}

	public stopCreatingCollection(): void {
		this.isCreatingCollectionCancelled$.next(true);
	}

	public isSupportedNetworkOrDisconnected(): boolean {
		const connectedChainSymbol: ChainSymbolEnum = this.web3Service.getConnectedChainSymbol();

		return !connectedChainSymbol || this.isSupportedNetwork(connectedChainSymbol);
	}

	public isCreateButtonDisabled(): boolean {
		return (this.isConnectedToWallet && this.createForm.invalid)
			|| !this.isSupportedNetworkOrDisconnected()
			|| (this.selectedContentType === 'MUSIC' && this.nfts?.length === 0);
	}

	public onAddNewNFT(): void {
		this.nftForm.reset();
		this.nftImagePreviewSrc = undefined;
		(document.getElementById('nftFilePicker') as HTMLInputElement).value = '';
	}

	public onConfirmNFT(): void {
		if (!this.nftFile) {
			this.notificationsService.error('Add file', `NFT has to be linked with a file - add ${this.selectedContentType === 'MUSIC' ? 'audio' : 'image'}`);
			return;
		}

		this.nfts.push({
			file: this.nftFile,
			attributes: this.nftForm.get('attributes').value,
		});
		this.closeNFTModal();
	}

	public closeNFTModal(): void {
		this.nftForm.reset();
		this.nftForm.get('file').setValue(undefined);
		this.nftImagePreviewSrc = undefined;
		(document.getElementById('nftFilePicker') as HTMLInputElement).value = '';
		this.nftModalCloseBtn?.nativeElement?.click();
	}

	public getAttributesCount(nft: INFTDetails): number {
		if (!nft.attributes) {
			return 0;
		}

		return Object.keys(nft.attributes).length;
	}

	public onRemoveNFT(index: number): void {
		this.nfts.splice(index, 1);
	}

	public getCategoryName(category: MusicCategoryEnum | ArtCategoryEnum): string {
		switch (category) {
			case MusicCategoryEnum.ELECTRONIC_DANCE:
				return 'Electronic & Dance';
			case MusicCategoryEnum.KPOP:
				return 'K-pop';
			case MusicCategoryEnum.RNB_SOUL:
				return 'RNB & Soul';
			case MusicCategoryEnum.HIPHOP:
				return 'Hip-hop';
			default:
				return category.charAt(0) + category.slice(1).toLowerCase();
		}
	}

	private async saveNFTs(): Promise<void> {
		let tokenId: number = 0;

		for (const nft of this.nfts) {
			if (!nft) {
				continue;
			}

			tokenId++;
			nft.tokenId = tokenId;
			await this.saveNFT(nft);
		}
	}

	private async saveNFT(nft: INFTDetails): Promise<void> {
		this.currentStep.subtitle = `Saving ${nft.tokenId}/${this.nfts.length} NFT`;

		if (!nft) {
			throw new Error('No NFT data');
		}

		if (!nft.file) {
			throw new Error('No NFT file');
		}

		if (!nft.file.type.match(/image\/*/)
			&& !nft.file.type.match(/audio\/*/)) {
			throw new Error('Unsupported NFT file type');
		}

		if ((nft.file.size / 1024 / 1024) > 10) {
			throw new Error('NFT file > 10MB');
		}
		await this.compressNFTFileAndSaveInIpfs(nft);
		await this.saveNFTInDatabase(nft);
	}

	private createCollection(): Observable<void> {
		return new Observable<void>((subscriber) => {
			try {
				if (!!this.file) {
					if (this.isCreatingCollectionCancelled$.getValue()) {
						subscriber.complete();
						return;
					}
					if (!this.isFileValid()) {
						this.stepper[0] = {
							title: 'File is invalid',
							iconText: 'File upload',
							subtitle: 'Please, verify selected file correctness and try again',
							status: StepperStatusEnum.ERROR,
							isActive: true,
						};
						this.activeStep = 0;

						this.notificationsService.error('File Error', 'Looks like the file is invalid');
						this.closeStepper();
						subscriber.complete();
						return;
					}

					this.activeStep = 0;
					this.sendFileToIpfs();
				} else if (this.isMetadata) {
					if (this.isCreatingCollectionCancelled$.getValue()) {
						subscriber.complete();
						return;
					}

					this.stepper[0] = {
						title: `Saving ${this.nfts.length ? 'NFTs ' : ''}metadata in IPFS`,
						iconText: `Save ${this.nfts.length ? 'NFTs ' : ''}metadata`,
						status: StepperStatusEnum.PENDING,
						isActive: true,
					};
					this.activeStep = 0;
					this.sendMetadataToIpfs();
				} else {
					if (this.isCreatingCollectionCancelled$.getValue()) {
						subscriber.complete();
						return;
					}
					this.stepper[0] = {
						title: 'Almost ready',
						iconText: 'Confirm',
						status: StepperStatusEnum.PENDING,
						isActive: true,
					};
					this.activeStep = 0;

					this.mint().then(() => {
						subscriber.complete();
					}, (e) => {
						subscriber.error(e);
					});
				}
			} catch (e) {
				console.error(e);
				subscriber.error(e);
			}
		});
	}

	private getDateFromTimestamp(): number {
		const dateFrom: Date = this.createForm.get('dateFrom').value;

		if (!dateFrom) {
			return 0;
		}

		return moment(dateFrom).unix();
	}

	private getDateToTimestamp(): number {
		const dateFrom: Date = this.createForm.get('dateFrom').value;
		const durationInDays: number = Number(this.createForm.get('durationInDays').value);

		if (!dateFrom || !durationInDays) {
			return 0;
		}

		if (durationInDays < 1) {
			return moment(dateFrom).add(1, 'days').unix();
		}

		if (durationInDays > 7) {
			return moment(dateFrom).add(7, 'days').unix();
		}

		return moment(dateFrom).add(durationInDays, 'days').unix();
	}

	private isFileValid(): boolean {
		if (!this.file) {
			return false;
		}

		// File bigger than 10MB is too big
		if ((this.file.size / 1024 / 1024) > 10) {
			this.isFileTooBig = true;

			return false;
		}

		return true;
	}

	private isNFTFileValid(): boolean {
		if (!this.nftFile) {
			return false;
		}

		// File bigger than 10MB is too big
		if ((this.nftFile.size / 1024 / 1024) > 10) {
			this.isNFTFileTooBig = true;

			return false;
		}

		return true;
	}

	private isMetadataProvided(): boolean {
		return !!this.file
			|| this.createForm.get('name').value?.length > 0
			|| this.createForm.get('description').value?.length > 0
			|| this.createForm.get('externalLink').value?.length > 0;
	}

	private async sendFileToIpfs(): Promise<void> {
		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');

		this.ipfsService.sendFile(this.file).then((result: AddResult) => {
			this.fileIpfsHash = result.path;
			this.currentStep.title = 'File successfully uploaded to IPFS 🥳';
			this.currentStep.status = StepperStatusEnum.COMPLETE;

			setTimeout(() => {
				this.currentStep.isActive = false;
				this.stepper[1] = {
					title: this.nfts.length ? 'Saving NFTs metadata in IPFS' : 'Saving metadata in IPFS',
					iconText: this.nfts.length ? 'Save NFTs metadata' : 'Save metadata',
					status: StepperStatusEnum.PENDING,
					isActive: true,
				};
				this.activeStep = 1;
				this.sendMetadataToIpfs();
			}, 3000);
		}).catch((e) => {
			console.error(e);
			this.currentStep.title = 'Couldn\'t upload file to IPFS';
			this.currentStep.subtitle = 'Verify your file correctness, try again, or contact us.';
			this.currentStep.status = StepperStatusEnum.ERROR;
			this.closeStepper(3000);
		}).finally(() => {
			this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
		});
	}

	private async sendMetadataToIpfs(): Promise<void> {
		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}

		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');
		const metadata: CollectionMetadataDto = this.buildMetadata();
		const metadataResult: AddResult = await this.ipfsService.saveMetadata<CollectionMetadataDto>(metadata);

		this.metadataIpfsHash = metadataResult.path;
		await this.saveNFTs();
		this.currentStep.subtitle = this.nfts.length ? 'Saved all NFTs 🥳' : 'Saved Metadata 🥳';
		this.tokensURIs = this.nfts.map((nft) => nft.tokenURI);
		this.onMetadataSaved();
	}

	private onMetadataSaved(): void {
		setTimeout(() => {
			this.currentStep.isActive = false;
			const activeStep: number = 2;

			this.stepper[activeStep] = {
				title: `${this.nfts.length > 0 ? 'NFTs and ' : ''}Metadata successfully uploaded to IPFS 🥳`,
				iconText: 'Confirm',
				status: StepperStatusEnum.PENDING,
				isActive: true,
			};
			this.activeStep = activeStep;
			this.mint();
		}, 3000);
	}

	private async saveCollectionInDatabase(status: CollectionStatusEnum): Promise<void> {
		if (status === CollectionStatusEnum.CREATING && this.createTxResult?.status !== 1) {
			status = CollectionStatusEnum.ERROR;
			this.notificationsService.error('Transaction Error', 'We saved your collection for you to complete it later, but for now the tx has failed.');

			if (this.createTxResult) {
				this.notificationsService.error('Error Details', 'If you\'re an advanced user, check browser console - we logged tx details for you.');
				console.log(`---------------- ${this.createTxResult.transactionHash} Transaction Status: ----------------`);
				console.log(this.createTxResult);
			}
		}

		const mintPrice: number = this.createForm.get('mintPrice').value;
		const payload: ICollectionDetails = {
			id: this.id,
			firestoreId: this.firestoreCollectionId || null,
			status,
			collectionName: this.createForm.get('name').value,
			mintPrice: mintPrice > 0
				? ethers.utils.parseUnits(mintPrice.toString(), environment.assetToDigits[this.selectedAssetName]).toString()
				: 0,
			assetName: this.selectedAssetName,
			totalSupply: this.nfts.length || 0,
			fileURI: ((this.isFileImage() && this.smImageIpfsHash?.length)
				? `${this.smImageIpfsHash}_${this.id}`
				: `${this.fileIpfsHash}_${this.id}`) || '',
			fileStoragePath: this.collectionFileStoragePath,
			nftsFilesStoragePaths: this.nftsFilesStoragePaths || null,
			metadataURI: this.metadataIpfsHash,
			tokensURIs: this.tokensURIs,
			tokenIds: 0,
			createdAt: moment().unix(),
			srcChain: this.connectedChainSymbol,
			chainSymbol: this.createForm.get('chainSymbol').value,
			creator: this.userAddress,
			dropFrom: this.getDateFromTimestamp(),
			dropTo: this.getDateToTimestamp(),
			metadata: {
				name: this.createForm.get('name').value,
				description: this.createForm.get('description').value || '',
				external_link: this.createForm.get('externalLink').value || '',
				media: this.isFileSupported() && this.fileIpfsHash?.length ? `${this.fileIpfsHash}_${this.id}` : '',
			},
			isPublic: this.createForm.get('isPublic').value,
			contentType: this.selectedContentType,
			category: this.selectedCategory,
		};

		if ([CollectionStatusEnum.CREATING, CollectionStatusEnum.ERROR].includes(status)) {
			payload.txHash = this.createTxResult?.transactionHash || null;
			payload.txStatus = this.createTxResult?.status || null;
		}

		try {
			if (status === CollectionStatusEnum.INITIATED) {
				const collectionFirestoreAddResult = await this.firestoreService.add(
					'collections',
					[],
					payload,
				);
				this.firestoreCollectionId = collectionFirestoreAddResult.id;
			} else {
				await this.firestoreService.set(
					'collections',
					this.firestoreCollectionId,
					payload,
				);
			}
		} catch (e) {
			console.error(e);
		}

		await this.collectionsService.saveCollectionInDatabase(
			this.userAddress,
			payload,
		);
	}

	private async saveNFTInDatabase(nft: INFTDetails): Promise<void> {
		try {
			this.firestoreService.add(
				'tokens',
				[],
				{
					collectionId: this.id,
					tokenId: nft.tokenId,
					tokenURI: nft.tokenURI,
					fileIpfsHash: `${nft.fileIpfsHash}_${this.id}`,
					smFileIpfsHash: `${nft.smFileIpfsHash}_${this.id}`,
					fileStoragePath: nft.fileStoragePath,
				},
				undefined,
				async () => {
					// TODO: Uncomment and remove firebaseDatabase.save when ready to migrate
					// this.stopCreatingCollection();
					// this.notificationsService.error('Error', 'Cannot save your NFTs!');
					// throw new Error(`Cannot store NFT of user: ${this.userAddress} at: ${moment().unix()}`);
				}
			);
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.save<INFTDetails>(
			`/tokens/${this.id}/${nft.tokenId}`,
			{
				tokenId: nft.tokenId,
				tokenURI: nft.tokenURI,
				fileIpfsHash: `${nft.fileIpfsHash}_${this.id}`,
				smFileIpfsHash: `${nft.smFileIpfsHash}_${this.id}`,
			},
			undefined,
			async () => {
				this.stopCreatingCollection();
				this.notificationsService.error('Error', 'Cannot save your NFTs!');
				throw new Error(`Cannot store NFT of user: ${this.userAddress} at: ${moment().unix()}`);
			}
		);
	}

	private async saveCreateTxInDatabase(): Promise<void> {
		const hash: string = this.createTx?.hash;

		if (!hash) {
			return;
		}

		try {
			// TODO: Uncomment and remove firebaseDatabase.save when ready to migrate
			const createCollectionTx: ICreateCollectionTx = {
				collectionId: this.id,
				hash,
			};
			const res = await this.firestoreService.add(
				'transactions',
				[this.userAddress, 'createCollection'],
				createCollectionTx,
			);
		} catch (e) {
			console.error(e);
		}

		await this.firebaseDatabaseService.save<Pick<ContractTransaction, 'hash'>>(
			`/transactions/${this.userAddress}/createCollection/${this.id}`,
			{hash},
		);
	}

	private buildMetadata(): CollectionMetadataDto {
		return  {
			name: this.createForm.get('name').value || '',
			description: this.createForm.get('description').value || '',
			external_link: this.createForm.get('externalLink').value || '',
			image: this.isFileImage() && this.fileIpfsHash?.length ? `https://omnisea.infura-ipfs.io/ipfs/${this.fileIpfsHash}` : '',
			file: this.file && this.fileIpfsHash?.length ? this.fileIpfsHash : '',
			media: this.isFileSupported() && this.fileIpfsHash?.length ? this.fileIpfsHash : '',
		};
	}

	private async mint(): Promise<void> {
		if (this.isCreatingCollectionCancelled$.getValue()) {
			return;
		}

		this.uiLoaderService.startBackgroundLoader('confirmStepperLoader');

		let web3Provider: Web3Provider;
		try {
			if (!this.web3Service.isConnected()) {
				await this.web3Service.connectWithModal();
			}

			if (this.walletCheckOnMint) {
				clearInterval(this.walletCheckOnMint);
			}

			web3Provider = this.web3Service.provider$.getValue();
		} catch (e) {
			this.notificationsService.error('Canceled', 'Please, connect your Web3 wallet');
			console.error(e);
			this.currentStep.title = 'Couldn\'t connect your wallet';
			this.currentStep.subtitle = 'Don\'t close this window! Connect to your wallet now. We\'ll check in 15 seconds';
			this.currentStep.status = StepperStatusEnum.ERROR;

			this.walletCheckOnMint = setInterval(() => {
				this.mint();
			}, 15000);

			return;
		}
		const signer: JsonRpcSigner = web3Provider.getSigner();
		const chainControl: AbstractControl = this.createForm.get('chainSymbol');
		const chainSymbol: ChainSymbolEnum = chainControl.value;

		if (!chainSymbol) {
			chainControl.setValue(this.connectedChainSymbol || ChainSymbolEnum.eth);
		}

		if (!chainSymbol) {
			this.createForm.get('chainSymbol').setValue(this.connectedChainSymbol);
		}

		if (!this.collectionFactoryContractAddress) {
			this.collectionFactoryContractAddress = environment.collectionFactoryAddressesMap[this.connectedChainSymbol];
		}

		const contract = new ethers.Contract(
			this.collectionFactoryContractAddress,
			this.collectionFactoryContractAbi,
			signer,
		);

		try {
			const mintPrice: number = Number(this.createForm.get('mintPrice').value);
			let estimatedFees: IEstimatedFees;

			if (chainSymbol !== this.connectedChainSymbol) {
				try {
					this.currentStep.subtitle = 'You\'ll be able to confirm transaction in a moment';
					const encodedFnData: string = this.web3Service.encodePayload(
						[
							chainSymbol,
							this.createForm.get('name').value,
							this.metadataIpfsHash || '',
							this.fileIpfsHash || '',
							mintPrice > 0
								? ethers.utils.parseUnits(mintPrice.toString(), environment.assetToDigits[this.selectedAssetName]).toString()
								: 0,
							this.selectedAssetName,
							this.getDateFromTimestamp(),
							this.getDateToTimestamp(),
							this.tokensURIs,
							environment.chainSymbolToCreateCollectionGas[chainSymbol],
							0,
						],
						['string', 'string', 'string', 'string', 'uint', 'string', 'uint', 'uint', 'string[]', 'uint', 'uint'],
					);
					const encodedPayload: string = this.web3Service.encodePayload(
						[
							chainSymbol,
							this.userAddress || '0xMock',
							encodedFnData,
							environment.chainSymbolToCreateCollectionGas[chainSymbol],
							this.userAddress || '0xMock',
							chainSymbol,
							this.userAddress || '0xMock',
							0,
						],
						['string', 'string', 'string', 'uint', 'string', 'string', 'string', 'uint'],
					);

					estimatedFees = await this.omnichainService.getEstimatedFees(
						chainSymbol,
						encodedPayload,
						OmnichainActionTypeEnum.CREATE_COLLECTION,
					);

					if (this.userBalance !== undefined && this.userBalance.lt(estimatedFees.fee)) {
						const estimatedFeeFormatted: number = Math.ceil(Number(ethers.utils.formatEther(estimatedFees.fee)) * 1000) / 1000;
						const nativeAsset: string = environment.chainSymbolToNativeAsset[this.connectedChainSymbol];
						const tipMessage: string = `Fund your wallet with at least ${estimatedFeeFormatted} ${nativeAsset} or try later`;

						this.notificationsService.error('Insufficient funds for transaction', tipMessage);
						this.currentStep.title = 'Insufficient funds for transaction';
						this.currentStep.subtitle = `${tipMessage}. Connected network can be congested now.`;
						this.currentStep.status = StepperStatusEnum.ERROR;

						this.closeStepper(10000);
						return;
					}
				} catch (e) {
					console.error(e);
					if (e?.message?.includes('aborted (timeout')) {
						// Ignore such error
					} else {
						this.notificationsService.error('Error', 'Cannot estimate transaction fee');
						this.closeStepper(3000);
					}
				}
			}
			this.currentStep.title = 'Confirm';
			this.currentStep.subtitle = 'Please, confirm the transaction with your wallet';

			if (this.isCreatingCollectionCancelled$.getValue()) {
				return;
			}

			if (chainSymbol === this.connectedChainSymbol) {
				this.createTx = await contract.create([
					chainSymbol,
					this.createForm.get('name').value,
					this.metadataIpfsHash || '',
					((this.isFileImage() && this.smImageIpfsHash?.length) ? this.smImageIpfsHash : this.fileIpfsHash) || '',
					mintPrice > 0 ? ethers.utils.parseUnits(mintPrice.toString(), environment.assetToDigits[this.selectedAssetName]).toString() : 0,
					this.selectedAssetName,
					this.getDateFromTimestamp(),
					this.getDateToTimestamp(),
					this.tokensURIs,
					environment.chainSymbolToCreateCollectionGas[chainSymbol],
					0,
				]);
			} else {
				this.createTx = await contract.create(
					[
						chainSymbol,
						this.createForm.get('name').value,
						this.metadataIpfsHash || '',
						((this.isFileImage() && this.smImageIpfsHash?.length) ? this.smImageIpfsHash : this.fileIpfsHash) || '',
						mintPrice > 0
							? ethers.utils.parseUnits(mintPrice.toString(), environment.assetToDigits[this.selectedAssetName]).toString()
							: 0,
						this.selectedAssetName,
						this.getDateFromTimestamp(),
						this.getDateToTimestamp(),
						this.tokensURIs,
						environment.chainSymbolToCreateCollectionGas[chainSymbol],
						estimatedFees.redirectFee,
					],
					{value: estimatedFees.fee},
				);
			}
			this.saveCreateTxInDatabase();
			await this.saveCollectionInDatabase(CollectionStatusEnum.INITIATED);
			this.currentStep.title = 'Waiting for transaction confirmation';
			this.currentStep.subtitle = this.connectedChainSymbol === chainSymbol ? 'Creating your collection...' : 'Your collection will be created on the selected chain and visible in a few minutes';

			this.createTx.wait().then((txResult: ContractReceipt) => {
				this.createTxResult = txResult;
				this.saveCollectionInDatabase(CollectionStatusEnum.CREATING);
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.currentStep.title = 'Success! 🥳';
				this.currentStep.subtitle = 'Collection will be visible in a few minutes.';
				this.currentStep.status = StepperStatusEnum.COMPLETE;
				this.notificationsService.success('Created', 'Collection will be visible in a few minutes');
				setTimeout(() => {
					this.closeStepper();
					setTimeout(() => {
						this.goToUserCollections(chainSymbol);
					}, 500);
				}, 500);
			}).catch((e) => {
				this.notificationsService.error('Transaction Error', 'Transaction has failed');
				console.error(e);
				this.currentStep.title = 'Transaction failed';
				this.currentStep.subtitle = 'Please, try again or contact us.';
				this.currentStep.status = StepperStatusEnum.ERROR;
				this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
				this.closeStepper(3000);
				this.saveCollectionInDatabase(CollectionStatusEnum.ERROR);
			});
		} catch (e) {
			if (e?.message?.includes('underlying network changed')) {
				console.error(e);
				this.notificationsService.error('Error', 'You\'ve changed network during the process');
				setTimeout(() => {
					this.closeStepper();
					window.location.reload();
				}, 2500);

				return;
			}

			if (e?.data?.message?.includes('insufficient funds for gas * price + value')) {
				console.error(e);
				this.notificationsService.error('Insufficient funds for transaction fees', 'Fund your wallet with more native asset for the tx fees');
				setTimeout(() => {
					this.closeStepper();
				}, 2500);

				return;
			}

			this.closeStepper();
			console.error(e);

			this.notificationsService.error('Transaction Error', 'Unable to create. Connected network can be congested');

			return;
		}
	}

	private getStepsCount(): number {
		return !!this.file ? 3 : (this.isMetadata ? 2 : 1);
	}

	private buildSteps(): void {
		const stepsCount: number = this.getStepsCount();

		if (stepsCount === 1) {
			this.stepper[0] = this.getFinalStepInitialConfig();
			this.stepper[0].isActive = true;
			return;
		}

		if (stepsCount === 2) {
			this.stepper[0] = this.getMetadataStepInitialConfig();
			this.stepper[0].isActive = true;
			this.stepper[1] = this.getFinalStepInitialConfig();
			return;
		}

		if (stepsCount === 3) {
			this.stepper[0] = this.getFileStepInitialConfig();
			this.stepper[1] = this.getMetadataStepInitialConfig();
			this.stepper[2] = this.getFinalStepInitialConfig();
			return;
		}
	}

	private getFileStepInitialConfig(): IStepperStepConfig {
		return {
			title: 'Uploading file to IPFS',
			status: StepperStatusEnum.PENDING,
			iconText: 'File upload',
			isActive: true,
		};
	}

	private getMetadataStepInitialConfig(): IStepperStepConfig {
		return {
			title: `Saving ${this.nfts.length ? 'NFTs and ' : ''}metadata in IPFS`,
			status: StepperStatusEnum.PENDING,
			iconText: `Save ${this.nfts.length ? 'NFTs ' : ''}metadata`,
			isActive: false,
		};
	}

	private getFinalStepInitialConfig(): IStepperStepConfig {
		return {
			title: 'Almost ready',
			status: StepperStatusEnum.PENDING,
			iconText: 'Confirm',
			isActive: false,
		};
	}

	private mapChainSymbolToName(chainSymbol: ChainSymbolEnum): SupportedChainNameEnum {
		switch (chainSymbol) {
			case ChainSymbolEnum.ftm:
				return SupportedChainNameEnum.Fantom;
			case ChainSymbolEnum.avax:
				return SupportedChainNameEnum.Avalanche;
			case ChainSymbolEnum.arb:
				return SupportedChainNameEnum.Arbitrum;
			case ChainSymbolEnum.bsc:
				return SupportedChainNameEnum.BSC;
			case ChainSymbolEnum.optimism:
				return SupportedChainNameEnum.Optimism;
			case ChainSymbolEnum.polygon:
				return SupportedChainNameEnum.Polygon;
			case ChainSymbolEnum.moonbeam:
				return SupportedChainNameEnum.Moonbeam;
			case ChainSymbolEnum.eth:
			default:
				return SupportedChainNameEnum.Ethereum;
		}
	}

	private closeStepper(timeout?: number): void {
		setTimeout(() => {
			const closeBtn: HTMLButtonElement = this.createStepperCloseBtn.nativeElement;

			if (closeBtn) {
				closeBtn.click();
			}
			document.querySelector('#confirmCreateStepper').classList.add('hidden');
		}, timeout || 0);
		this.uiLoaderService.stopBackgroundLoader('confirmStepperLoader');
	}

	private async goToUserCollections(chainSymbol: ChainSymbolEnum): Promise<void> {
		const queryParams: Params = {
			chainSymbol,
			metadataURI: this.metadataIpfsHash,
			userAddress: this.userAddress || await this.web3Service.getUserAddress(),
		};

		if (this.createdCollectionAddress) {
			queryParams.collectionAddress = this.createdCollectionAddress;
		}

		this.uiLoaderService.stopAll();
		this.closeStepper(0);
		this.router.navigate(
			['/collections/user'],
			{queryParams}
		);
	}

	private isSupportedNetwork(chainSymbol: ChainSymbolEnum): boolean {
		return environment.supportedChains.includes(chainSymbol);
	}

	private compressCollectionImageAndSaveInIpfs(): void {
		const options: ICompressImageOptions = {
			maxSizeMB: 1,
			maxWidthOrHeight: 2200,
			useWebWorker: true,
			alwaysKeepResolution: true,
		};

		imageCompression(this.file, options)
			.then((compressedFile) => {
				this.ipfsService.sendFile(compressedFile).then((result: AddResult) => {
					this.smImageIpfsHash = result.path;
					this.firebaseStorageService.uploadFile(`${this.smImageIpfsHash}_${this.id}`, compressedFile).then((res) => {
						this.collectionFileStoragePath = res.ref.fullPath;
					});
				});
			})
			.catch((error) => {
				console.error(error.message);
			});
	}

	private async compressNFTFileAndSaveInIpfs(nft: INFTDetails): Promise<void> {
		const options: ICompressImageOptions = {
			maxSizeMB: 1,
			maxWidthOrHeight: 1600,
			useWebWorker: true,
			alwaysKeepResolution: true,
		};
		const fileIpfsResult: AddResult = await this.ipfsService.sendFile(nft.file);

		nft.fileIpfsHash = fileIpfsResult.path;

		try {
			const nftFile: File = this.selectedContentType === 'ART' ? await imageCompression(nft.file, options) : nft.file;

			try {
				const compressedFileIpfsResult: AddResult = await this.ipfsService.sendFile(nftFile);

				try {
					nft.smFileIpfsHash = compressedFileIpfsResult.path;
					const res: UploadResult = await this.firebaseStorageService.uploadFile(`${nft.smFileIpfsHash}_${this.id}`, nftFile);
					const collectionName: string = this.createForm.get('name').value;
					const externalLink: string = this.createForm.get('externalLink').value;

					nft.fileStoragePath = res.ref.fullPath;
					this.nftsFilesStoragePaths.push(nft.fileStoragePath);

					try {
						const metadataIpfsResult: AddResult = await this.ipfsService.saveMetadata<INFTMetadata>({
							name: nft.name || (collectionName?.length >= 3 ? `${collectionName} #${nft.tokenId}` : `#${nft.tokenId}`),
							image: nftFile.type.match(/image\/*/) ? `https://omnisea.infura-ipfs.io/ipfs/${nft.fileIpfsHash}` : '',
							fileURI: `https://omnisea.infura-ipfs.io/ipfs/${nft.fileIpfsHash}`,
							external_link: externalLink || '',
							attributes: nft.attributes || {},
						});

						nft.tokenURI = metadataIpfsResult.path;
					} catch (e) {
						console.error(`nft ${JSON.stringify(nft)} this.ipfsService.saveMetadata() error`);
						console.error(e);
					}
				} catch (e) {
					console.error(`nft ${JSON.stringify(nft)} firebaseStorageService.uploadIpfsFile() error`);
					console.error(e);
				}
			} catch (e) {
				console.error(`nft ${JSON.stringify(nft)} ipfsService.sendFile() error`);
				console.error(e);
			}
		} catch (e) {
			console.error(`nft ${JSON.stringify(nft)} imageCompression() error`);
			console.error(e);
		}
	}
}
